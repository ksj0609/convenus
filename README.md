## Overview ##
This repository is for our project *ConVenus*, a network verification tool for Software-Defined Networking (SDN)

## Contents ##
* final version directory contains the latest version of our code
* previous versions directory contains the previous versions of our code
* WSC_paper directory contains our paper submitted in the "Winter Simulation 2016" conference
* GCASR_poster directory contains our poster presented in the Greater Chicago Area System Research (GCASR) workshop

## Environment ##
* ConVenus is implemented in C++ language, compiled by GCC compiler

## Compile and Run ##
1. cd final_version
2. make clean all
3. ./Main

## Contacts ##
* This repository is owned by Xin Liu <xliu125@hawk.iit.edu>, if you have any questions or comments, please contact him.