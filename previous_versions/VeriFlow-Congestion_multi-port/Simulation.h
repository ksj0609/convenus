#ifndef SIMULATION_H_
#define SIMULATION_H_

#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <map>
#include <unordered_map>
#include <set>
#include <list>
#include "Network.h"
#include "PortVolumnState.h"

using namespace std;

struct DependenceGraphNode{

	PortIdentifier myNode;
	list< PortIdentifier > directedNodes;	// this is the set of edges, each edge will have one or more critical segments


	DependenceGraphNode(PortIdentifier pid){
		myNode = pid;
	}
};

class Simulation
{
private:
	map< PortIdentifier, PortVolumnState* > portMap;
	unordered_set< DependenceGraphNode *> dependenceGraph;
	set< FlowSegment *> criticalSegments;

	map< FlowSegment *, FlowSegment *> causalRelation;	// This data structure record the causal relation between segments

public:
	void setup(Network& network);
	bool ruleBasedUpdate(Network& network);		// return true if all ports resolved; else return false
	void generateDependenceGraph(Network &network);
	void fixedPointIteration(Network &network);

	bool valuesFixed(map< FlowSegment *, double> previousIteration);

	void printAllPort() const;
	void printDependenceGraph();

};

#endif /* SIMULATION_H_ */