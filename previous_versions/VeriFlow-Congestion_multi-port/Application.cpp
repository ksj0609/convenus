#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <map>
#include <unordered_map>
#include <set>
#include <list>
#include <stack>
#include <queue>
#include <random>
#include <stdio.h>      
#include <stdlib.h>     
#include <time.h> 
#include "Network.h"
#include "PortVolumnState.h"
#include "Flow.h"
#include "PathGenerator.h"
#include "Application.h"

using namespace std;

Application::Application(Network& network){
	this->myNetwork = network;
	this->flowIDCounter = 1;

	unordered_map<string, ForwardingDevice> map = myNetwork.getDeviceMap();
	for(unordered_map<string, ForwardingDevice>::const_iterator iter = map.begin(); iter != map.end(); iter++){
		ForwardingDevice fd = iter->second;
		if(fd.endDevice){
			endDevices.push_back(iter->first);
		}
	}

	//srand(time(NULL));

}

Flow Application::issueFlow(string source, string dest){

	Flow flow;

	stack<PortIdentifier> *path = new stack<PortIdentifier>();
	bool foundPath = generateDFSPath(source, dest, path);
	
	flow.flowID = flowIDCounter;
	flowIDCounter++;


	double inRate = 100.0;	// in order to have congestion
	

	flow.ingressRate = inRate;
	flow.egressRate = 0.0;

	unsigned int segmentID = 0;
	FlowSegment *fs = new FlowSegment();
	PortIdentifier previousPID = path->top();
	path->pop();
	PortIdentifier currentPID = path->top();
	path->pop();

	fs->myFlowID = flow.flowID;
	fs->segmentID = segmentID;
	fs->type = settled;
	fs->sourcePort = previousPID;
	fs->destPort = currentPID;
	fs->in_rate = inRate;
	flow.flowPath.push_back(fs);

	segmentID++;
	previousPID = currentPID;

	while(!path->empty()){
		FlowSegment *fs = new FlowSegment();
		PortIdentifier currentPID = path->top();
		path->pop();
		fs->myFlowID = flow.flowID;
		fs->segmentID = segmentID;
		fs->type = unsettled;
		fs->sourcePort = previousPID;
		fs->destPort = currentPID;
		fs->in_rate = -1.0;
		flow.flowPath.push_back(fs);
		segmentID++;
		previousPID = currentPID;
	}

	return flow;
}


Flow Application::issueFlow(){
	
	Flow flow;

	// first genenrate a path
	int sourceIndex = rand() % endDevices.size();
	int destIndex = rand() % endDevices.size();
	while(sourceIndex == destIndex){
		destIndex = rand() % endDevices.size();
	}

	string source = endDevices[sourceIndex];
	string dest = endDevices[destIndex];
	stack<PortIdentifier> *path = new stack<PortIdentifier>();
	bool foundPath = generateDFSPath(source, dest, path);

	while(!foundPath){
		sourceIndex = rand() % endDevices.size();
		destIndex = rand() % endDevices.size();
		while(sourceIndex == destIndex){
			destIndex = rand() % endDevices.size();
		}
		string source = endDevices[sourceIndex];
		string dest = endDevices[destIndex];
		foundPath = generateBFSPath(source, dest, path);
	}

	// then generate flow ID and ingress rate
	flow.flowID = flowIDCounter;
	flowIDCounter++;

	//double inRate = (double) (rand() % 10 + 1);
	double inRate = 10;
	flow.ingressRate = inRate;
	flow.egressRate = 0.0;

	unsigned int segmentID = 0;
	FlowSegment *fs = new FlowSegment();
	PortIdentifier previousPID = path->top();
	path->pop();
	PortIdentifier currentPID = path->top();
	path->pop();

	fs->myFlowID = flow.flowID;
	fs->segmentID = segmentID;
	fs->type = settled;
	fs->sourcePort = previousPID;
	fs->destPort = currentPID;
	fs->in_rate = inRate;
	flow.flowPath.push_back(fs);

	segmentID++;
	previousPID = currentPID;

	while(!path->empty()){
		FlowSegment *fs = new FlowSegment();
		PortIdentifier currentPID = path->top();
		path->pop();
		fs->myFlowID = flow.flowID;
		fs->segmentID = segmentID;
		fs->type = unsettled;
		fs->sourcePort = previousPID;
		fs->destPort = currentPID;
		fs->in_rate = -1.0;
		flow.flowPath.push_back(fs);
		segmentID++;
		previousPID = currentPID;
	}

	return flow;
}

Flow Application::issueFlowDeletion(){

	Flow fakeFlow;
	
	// pick a random element from the set of flowID's, and delete this ID later
	std::unordered_set<uint64_t>::iterator random_iter = this->flowIDSet.begin();
	int advance = rand() % this->flowIDSet.size();
	std::advance(random_iter, advance);

	uint64_t flowID = *(random_iter);
	fakeFlow.flowID = flowID;

	return fakeFlow;
}

bool Application::generateBFSPath(string source, string dest, stack<PortIdentifier> *path){

	bool foundPath = false;

	unordered_map<string, bool> visitedStates;
	unordered_map<string, PortIdentifier> predecessor;
	unordered_map<string, ForwardingDevice> deviceMap = this->myNetwork.getDeviceMap();

	unordered_map<string, ForwardingDevice>::iterator iter;
	for(iter = deviceMap.begin(); iter != deviceMap.end(); iter++){
		string deviceAddress = iter->first;
		visitedStates[deviceAddress] = false;
	}

	queue<ForwardingDevice> nodeQueue;
	nodeQueue.push(deviceMap[source]);
	visitedStates[source] = true;

	while(!nodeQueue.empty() && !foundPath){

		ForwardingDevice fd = nodeQueue.front();
		//fprintf(stdout, "now examing device %s\n", fd.ipAddress.c_str());

		unordered_map< unsigned int, string >::iterator iter;
		for(iter = fd.portToNextHopIpAddressMap.begin(); iter != fd.portToNextHopIpAddressMap.end(); iter++){

			PortIdentifier currentPID;
			currentPID.deviceIP = fd.ipAddress;
			currentPID.portID = iter->first;

			string nextHopIp = iter->second;
			ForwardingDevice nextHop = deviceMap[nextHopIp];

			if(nextHop.endDevice){
				if(nextHopIp == dest){
					//fprintf(stdout, "found the destination %s\n", nextHopIp.c_str());
					predecessor[nextHopIp] = currentPID;
					foundPath = true;
				}
			}else{

				if(visitedStates[nextHopIp] == false){
				//fprintf(stdout, "%s hasn't been touched yet\n", nextHopIp.c_str());
				visitedStates[nextHopIp] = true;
				predecessor[nextHopIp] = currentPID;
				nodeQueue.push(nextHop);
				}
			}
		}
		
		nodeQueue.pop();
	}

	if(foundPath){

		PortIdentifier destPID;
		destPID.deviceIP = dest;
		destPID.portID = 0;

		PortIdentifier sourcePID;
		sourcePID.deviceIP = source;
		sourcePID.portID = 0;

		PortIdentifier next = destPID;

		while(next != sourcePID){
			path->push(next);
			next = predecessor[next.deviceIP];
		}
		path->push(sourcePID);

		return true;

	}else{

		return false;
	}
}

bool Application::generateDFSPath(string source, string dest, stack<PortIdentifier> *path){

	unordered_map<string, bool> *visitedStates = new unordered_map<string, bool>();
	unordered_map<string, ForwardingDevice> deviceMap = this->myNetwork.getDeviceMap();
	
	unordered_map<string, ForwardingDevice>::iterator iter;
	for(iter = deviceMap.begin(); iter != deviceMap.end(); iter++){
		string deviceAddress = iter->first;
		(*visitedStates)[deviceAddress] = false;
	}
	
	bool found = runDFS(source, dest, visitedStates, path);
	if(found){
		return true;
	}else{
		return false;
	}
}

bool Application::runDFS(string source, string dest, unordered_map<string, bool> *states, stack<PortIdentifier> *path){

	bool found = false;

	if(states->find(source) == states->end()){
		fprintf(stdout, "ERROR there is no %s\n", source.c_str());
		return false;
	}

	(*states)[source] = true;

	ForwardingDevice fd = (this->myNetwork.getDeviceMap())[source];
	
	unordered_map< unsigned int, string >::iterator iter;
	for(iter = fd.portToNextHopIpAddressMap.begin(); iter != fd.portToNextHopIpAddressMap.end(); iter++){
		
		unsigned int port = iter->first;
		string nextHopIP = iter->second;
		ForwardingDevice nextFD = (this->myNetwork.getDeviceMap())[nextHopIP];
		
		if(nextFD.endDevice){	
			if(nextFD.ipAddress == dest){

				PortIdentifier nextPID;
				nextPID.deviceIP = nextFD.ipAddress;
				nextPID.portID = 0;
				path->push(nextPID);

				PortIdentifier pid;
				pid.deviceIP = source;
				pid.portID = port;
				path->push(pid);
				return true;
			}
			continue;
		}

		if((*states)[nextHopIP] == false){
			found = runDFS(nextHopIP, dest, states, path);
		}

		if(found){
			PortIdentifier pid;
			pid.deviceIP = source;
			pid.portID = port;
			path->push(pid);
			return true;
		}
	}
		
	return false;
}
