#ifndef PORTVOLUMNSTATE_H_
#define PORTVOLUMNSTATE_H_

#include <sys/types.h>
#include <unistd.h>
#include <stdint.h>
#include <string>
#include <unordered_set>
#include <vector>
#include <map>
#include "Flow.h"

using namespace std;

enum PortState {resolved, transparent, unresolved};

struct PortVolumnState{
public:
	PortState portState;
	double bandwidth;				
	unordered_set< FlowSegment *> in_segment;	// The flows come into of this port	
};

#endif /* PORTVOLUMNSTATE_H_ */