#include <sys/types.h>
#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <string>
#include <signal.h>
#include <stdint.h>
#include <list>
#include <vector>
#include <sys/time.h>
#include <unordered_map>
#include <unordered_set>
#include "MyTest.h"

using namespace std;

int main(int argc, char** argv)
{
	if(argc == 1)
	{
        MyTest::myTest();
		return EXIT_SUCCESS;
	}
	else{
		fprintf(stdout, "No arguments needed\n");
		return EXIT_SUCCESS;
	}
}