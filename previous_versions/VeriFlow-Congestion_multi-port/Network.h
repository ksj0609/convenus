/*
 * Network.h
 *
 *  Created on: Mar 15, 2012
 *      Author: khurshid
 *
 * This file is protected by the VeriFlow Research License Agreement
 * available at http://www.cs.illinois.edu/~khurshi1/projects/veriflow/veriflow-research-license-agreement.txt.
 * A copy of this agreement is also included in this package.
 *
 * Copyright (c) 2012-2013 by
 * The Board of Trustees of the University of Illinois.
 * All rights reserved.
 */

#ifndef NETWORK_H_
#define NETWORK_H_

#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <unordered_map>
#include "ForwardingDevice.h"
#include "Flow.h"

using namespace std;

class Network
{
friend class Simulation;
private:
	unordered_map< string, ForwardingDevice > deviceMap;
	unordered_map< uint64_t, string > idToIpAddressMap;
	unordered_map< uint64_t, Flow *> flowMap;		// This flowID corresponding to Application? EquivalenceClass? 

public:
	unordered_map< string, ForwardingDevice > getDeviceMap() { return this->deviceMap; }
	unordered_map< uint64_t, Flow *> getFlowMap() { return this->flowMap; }
	void clearFlowMap() { this->flowMap.clear(); }

	bool addDevice(ForwardingDevice device);

	bool addDevice(uint64_t id, const string& ipAddress, bool endDevice);
	bool addPort(const string& ipAddress, unsigned int port, const string& nextHopIpAddress, double bandwidth);		// re-write addPort() to integrate port bandwidth
	string getNextHopIpAddress(const string& ipAddress, unsigned int port);
	bool isEndDevice(const string& ipAddress);
	bool isDevicePresent(const string& ipAddress) const;
	bool isPortPresent(const string& ipAddress, unsigned int port);
	string getDeviceIpAddress(uint64_t id);
	void print() const;

	// new ADDED functions
	bool addFlow(Flow flow);
	bool addFlow(uint64_t id, double ingress, double egress);
	bool addFlowSegment(uint64_t id, uint64_t sid, string sDevice, unsigned int sPort, string dDevice, unsigned int dPort, double v, FlowType t);
	bool addFlowSegment(uint64_t id, uint64_t sid, PortIdentifier sourcePID, PortIdentifier destPID, double v, FlowType t);
	bool deleteFlow(uint64_t flowID);
	bool flowExists(uint64_t flowID);
	Flow getFlow(uint64_t flowID);

	void printFlows() const;

	int calculateEgress();
};

#endif /* NETWORK_H_ */
