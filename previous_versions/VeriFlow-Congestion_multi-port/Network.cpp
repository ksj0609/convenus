/*
 * Network.cpp
 *
 *  Created on: Mar 15, 2012
 *      Author: khurshid
 *
 * This file is protected by the VeriFlow Research License Agreement
 * available at http://www.cs.illinois.edu/~khurshi1/projects/veriflow/veriflow-research-license-agreement.txt.
 * A copy of this agreement is also included in this package.
 *
 * Copyright (c) 2012-2013 by
 * The Board of Trustees of the University of Illinois.
 * All rights reserved.
 */

#include <sys/types.h>
#include <unistd.h>
#include <cstdio>
#include <string>
#include <unordered_map>
#include <map>
#include "Network.h"
#include "ForwardingDevice.h"

using namespace std;

bool Network::addDevice(uint64_t id, const string& ipAddress, bool endDevice)
{
	if(this->isDevicePresent(ipAddress) == false)
	{
		ForwardingDevice device;
		device.id = id;
		device.ipAddress = ipAddress;
		device.endDevice = endDevice;
		
		this->deviceMap[ipAddress] = device;
		this->idToIpAddressMap[id] = ipAddress;

		return true;
	}
	else
	{
		return false;
	}
}

bool Network::addPort(const string& ipAddress, unsigned int port, const string& nextHopIpAddress, double bandwidth)
{
	if(this->isDevicePresent(ipAddress) == false)
	{
		return false;
	}
	else
	{
		this->deviceMap[ipAddress].portToNextHopIpAddressMap[port] = nextHopIpAddress;
		this->deviceMap[ipAddress].portToBandwidthMap[port] = bandwidth;
		return true;
	}
}

string Network::getNextHopIpAddress(const string& ipAddress, unsigned int port)
{
	if(this->isDevicePresent(ipAddress) == false)
	{
		return "NULL";
	}
	else
	{
		if(this->isPortPresent(ipAddress, port) ==  false)
		{
			return "NULL";
		}
		else
		{
			return this->deviceMap[ipAddress].portToNextHopIpAddressMap[port];
		}
	}
}

bool Network::isEndDevice(const string& ipAddress)
{
	if(this->isDevicePresent(ipAddress) == false)
	{
		return false;
	}
	else
	{
		return (this->deviceMap[ipAddress].endDevice == true);
	}
}

bool Network::isDevicePresent(const string& ipAddress) const
{
	if(this->deviceMap.find(ipAddress) == this->deviceMap.end())
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool Network::isPortPresent(const string& ipAddress, unsigned int port)
{
	if(this->isDevicePresent(ipAddress) == false)
	{
		return false;
	}
	else
	{
		if(this->deviceMap[ipAddress].portToNextHopIpAddressMap.find(port) == this->deviceMap[ipAddress].portToNextHopIpAddressMap.end())
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}

string Network::getDeviceIpAddress(uint64_t id)
{
	if(this->idToIpAddressMap.find(id) == this->idToIpAddressMap.end())
	{
		return "NULL";
	}
	else
	{
		return this->idToIpAddressMap[id];
	}
}

void Network::print() const
{
	unordered_map< string, ForwardingDevice >::const_iterator itr1;
	for(itr1 = this->deviceMap.begin(); itr1 != this->deviceMap.end(); itr1++)
	{
		
		const ForwardingDevice& device = itr1->second;
		fprintf(stdout, "id %lu ipAddress %s endDevice %d\n", device.id, device.ipAddress.c_str(), device.endDevice);

		
		unordered_map< unsigned int, string >::const_iterator itr2;
		unordered_map< unsigned int, double>::const_iterator itr3;
		
		for(itr2 = device.portToNextHopIpAddressMap.begin(); itr2 != device.portToNextHopIpAddressMap.end(); itr2++)
		{
			fprintf(stdout, " port %u nextHop %s", itr2->first, itr2->second.c_str());
		}

		fprintf(stdout, "\n");

		for(itr3 = device.portToBandwidthMap.begin(); itr3 != device.portToBandwidthMap.end(); itr3++)
		{
			fprintf(stdout, " port %u bandwidth %lf", itr3->first, itr3->second);
		}

		fprintf(stdout, "\n\n");		
	}
}


//////////////////////////////////////// new functions ///////////////////////////////////////
bool Network::addDevice(ForwardingDevice device){
	string IP = device.ipAddress;
	if(this->deviceMap.find(IP) != this->deviceMap.end()){
		// device already exists
		return false;
	}else{
		this->deviceMap[IP] = device;
		this->idToIpAddressMap[device.id] = IP;
		return true;
	}
}

bool Network::addFlow(uint64_t id, double ingress, double egress){

	Flow *flow = new Flow();
	flow->flowID = id;
	flow->ingressRate = ingress;
	this->flowMap[id] = flow;

	return true;
}

bool Network::addFlow(Flow flow){

	if(this->flowMap.find(flow.flowID) != this->flowMap.end()){

		fprintf(stdout, "ERROR: existing flow id\n");
		return false;
	}else{

		Flow *newFlow = new Flow();
		newFlow->flowID = flow.flowID;
		newFlow->ingressRate = flow.ingressRate;
		newFlow->egressRate = flow.egressRate;
		newFlow->flowPath = flow.flowPath;
		this->flowMap[newFlow->flowID] = newFlow;
		
		return true;
	}
}

bool Network::addFlowSegment(uint64_t id, uint64_t sid, string sDevice, unsigned int sPort, string dDevice, unsigned int dPort, double v, FlowType t){

	if(this->flowMap.find(id) == this->flowMap.end()){	// which means the map doesn't contain such id
		fprintf(stdout, "ERROR: the map doesn't have such flow id\n");
		return false;
	}else{
		FlowSegment *fs = new FlowSegment();
		fs->myFlowID = id;
		fs->segmentID = sid;

		//
		PortIdentifier sourcePID;
		sourcePID.deviceIP = sDevice;
		sourcePID.portID = sPort;
		fs->sourcePort = sourcePID;

		PortIdentifier destPID;
		destPID.deviceIP = dDevice;
		destPID.portID = dPort;
		fs->destPort = destPID;
		
		fs->in_rate = v;
		fs->type = t;
		this->flowMap[id]->flowPath.push_back(fs);
		return true;
	}
}

bool Network::addFlowSegment(uint64_t id, uint64_t sid, PortIdentifier sourcePID, PortIdentifier destPID, double v, FlowType t){

	if(this->flowMap.find(id) == this->flowMap.end()){	// which means the map doesn't contain such id
		fprintf(stdout, "ERROR: the map doesn't have such flow id\n");
		return false;
	}else{
		FlowSegment *fs = new FlowSegment();
		fs->myFlowID = id;
		fs->segmentID = sid;
		fs->sourcePort = sourcePID;
		fs->destPort = destPID;
		fs->in_rate = v;
		fs->type = t;
		this->flowMap[id]->flowPath.push_back(fs);
		return true;
	}
}

bool Network::deleteFlow(uint64_t flowID){

	unordered_map<uint64_t, Flow *>::iterator iter = this->flowMap.find(flowID);
	if(iter == this->flowMap.end()){
		return false;
	}else{
		delete iter->second;
		this->flowMap.erase(iter);
		return true;
	}
}

bool Network::flowExists(uint64_t flowID){

	if(this->flowMap.find(flowID) == this->flowMap.end()){
		return false;	// does not exist
	}else{
		return true;
	}
}

Flow Network::getFlow(uint64_t flowID){

	unordered_map<uint64_t, Flow *>::iterator iter = this->flowMap.find(flowID);

	if(iter == this->flowMap.end()){
		fprintf(stdout, "Didn't find this flow with id = %ld\n", flowID);
	}
	Flow flow = *(iter->second);
	return flow;
}


int Network::calculateEgress(){

	int num_congestion = 0;

	unordered_map< uint64_t, Flow *>::iterator itr;
	for(itr = flowMap.begin(); itr != flowMap.end(); itr++){

		uint64_t flowID = itr->first;
		Flow *f = flowMap[flowID];

		FlowSegment *lastSegment = f->flowPath.back();

		f->egressRate = lastSegment->in_rate;

		if(f->egressRate < f->ingressRate){
			num_congestion++;
		}
	}
	return num_congestion;
}

void Network::printFlows() const
{
	unordered_map< uint64_t, Flow *>::const_iterator itr1;
	
	for(itr1 = this->flowMap.begin(); itr1 != this->flowMap.end(); itr1++)
	{
		const Flow *flow = itr1->second;
		fprintf(stdout, "Flow_id: %lu ingressRate %f egressRate %f \n", flow->flowID, flow->ingressRate, flow->egressRate);

		vector< FlowSegment *>::const_iterator itr2;
	
		for(itr2 = flow->flowPath.begin(); itr2 != flow->flowPath.end(); itr2++){
			const FlowSegment *fs = *itr2;
			fprintf(stdout, "segment %ld of flow %ld: <%s, %d> to <%s, %d>, rate = %f, type is %d\n", fs->segmentID, fs->myFlowID, fs->sourcePort.deviceIP.c_str(), 
															fs->sourcePort.portID, fs->destPort.deviceIP.c_str(), fs->destPort.portID, fs->in_rate, fs->type);	
		}

		fprintf(stdout, "\n");
	}
}

