#ifndef FLOW_H_
#define FLOW_H_

#include <sys/types.h>
#include <unistd.h>
#include <stdint.h>
#include <string>
#include <unordered_map>
#include <vector>
#include "ForwardingDevice.h"

using namespace std;

struct PortIdentifier{

	//uint64_t deviceID;
	string deviceIP;
	unsigned int portID;

	bool operator==(const PortIdentifier &o) const {
		return deviceIP == o.deviceIP && portID == o.portID;
	}

	bool operator<(const PortIdentifier &o) const {
		return deviceIP < o.deviceIP || (deviceIP == o.deviceIP && portID < o.portID);
	}

	bool operator!=(const PortIdentifier &o) const{
		return deviceIP != o.deviceIP || portID != o.portID;
	}
};

enum FlowType {settled, unsettled, bounded};

struct ForwardingDevice;

struct FlowSegment{
public:
	uint64_t myFlowID;	// which flow this segment belongs to
	uint64_t segmentID;	// this segment's ID

	FlowType type;
	PortIdentifier sourcePort;
	PortIdentifier destPort;

	double in_rate;
};

struct Flow
{
public:
	uint64_t flowID;
	double ingressRate;
	double egressRate;
	vector<FlowSegment *> flowPath;

	void setEgress(double e){
		this->egressRate = e;
	}


	bool operator==(const Flow &o) const {
		return flowID == o.flowID;
	}

	bool operator<(const Flow &o) const {
		return flowID < o.flowID;
	}

	bool operator!=(const Flow &o) const{
		return flowID != o.flowID;
	}

};

namespace std{

  template <>
  struct hash<PortIdentifier>
  {
    std::size_t operator()(const PortIdentifier& p) const
    {
      using std::size_t;
      using std::hash;
      using std::string;

      // Compute individual hash values for first,
      // second and third and combine them using XOR
      // and bit shifting:

      return ( (hash<string>()(p.deviceIP) ^ (hash<unsigned int>()(p.portID) << 1) ) >> 1);
    }
  };

  template <>
  struct hash<Flow>
  {
    std::size_t operator()(const Flow& f) const
    {
      using std::size_t;
      using std::hash;
      using std::string;

      // Compute individual hash values for first,
      // second and third and combine them using XOR
      // and bit shifting:

      return ( (hash<uint64_t>()(f.flowID) ^ (hash<double>()(f.ingressRate) << 1) ) >> 1);
    }
  };
}

#endif /* FLOW_H_ */
