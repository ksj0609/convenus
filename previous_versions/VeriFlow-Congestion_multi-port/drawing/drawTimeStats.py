import matplotlib.pyplot as plt
import csv
import numpy as np

# use csv

x = []
y = []

with open('../timeStats.txt','r') as csvfile:
	plots = csv.reader(csvfile, delimiter=' ')
	for row in plots:
		x.append(int(row[0]))
		y.append(float(row[1]))

plt.plot(x, y)

plt.xlabel('Verification #')
plt.ylabel('Verification Time')
plt.title('Time stats')

plt.show()
