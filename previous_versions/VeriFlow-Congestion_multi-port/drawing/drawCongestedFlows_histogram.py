import matplotlib.pyplot as plt
import csv
import numpy as np

# use csv

x = []

with open('../congestedFlows.txt','r') as csvfile:
	plots = csv.reader(csvfile, delimiter=' ')
	for row in plots:
		x.append(int(row[1]))	# x are the values of each # congested flows

maximum = max(x)
bins = list(xrange(maximum+1))


plt.hist(x, bins, histtype='bar', rwidth=0.8)

plt.xlabel('number of congested flows')
plt.ylabel('Population')
plt.title('a graph')

plt.show()
