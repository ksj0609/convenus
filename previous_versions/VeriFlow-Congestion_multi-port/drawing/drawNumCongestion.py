import matplotlib.pyplot as plt
import csv
import numpy as np

# use csv

x = []
y = []

with open('../numCongestion.txt','r') as csvfile:
	plots = csv.reader(csvfile, delimiter=' ')
	for row in plots:
		x.append(int(row[0]))
		y.append(float(row[1]))

f1 = plt.figure(1)

plt.plot(x, y)

plt.xlabel('verification #')
plt.ylabel('# congestions')
plt.title('Number of Congestions')

plt.show()
