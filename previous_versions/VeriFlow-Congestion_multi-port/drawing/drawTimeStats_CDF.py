import matplotlib.pyplot as plt
import csv
import numpy as np

# use csv
def frange(x, y, jump):
  while x < y:
    yield x
    x += jump

x = []

with open('../timeStats.txt','r') as csvfile:
	plots = csv.reader(csvfile, delimiter=' ')
	for row in plots:
		x.append(float(row[1]))	# x are the values of each # congested flows

maximum = max(x)
minimum = min(x)

# set each step to be 0.0001

bins = list(frange(minimum, maximum, 0.0001))

sorted_data = np.sort(x)

yvals = np.arange(len(sorted_data))/float(len(sorted_data))

plt.plot(sorted_data, yvals)

plt.show()