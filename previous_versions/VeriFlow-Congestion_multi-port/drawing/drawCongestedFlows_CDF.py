import matplotlib.pyplot as plt
import csv
import numpy as np

# use csv

x = []

with open('../congestedFlows.txt','r') as csvfile:
	plots = csv.reader(csvfile, delimiter=' ')
	for row in plots:
		x.append(int(row[1]))	# x are the values of each # congested flows

maximum = max(x)
bins = list(xrange(maximum+1))

sorted_data = np.sort(x)

yvals = np.arange(len(sorted_data))/float(len(sorted_data))

plt.plot(sorted_data, yvals)

plt.show()