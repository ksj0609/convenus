/*
 * Test.cpp
 *
 *  Created on: Jul 15, 2012
 *      Author: khurshid
 *
 * This file is protected by the VeriFlow Research License Agreement
 * available at http://www.cs.illinois.edu/~khurshi1/projects/veriflow/veriflow-research-license-agreement.txt.
 * A copy of this agreement is also included in this package.
 *
 * Copyright (c) 2012-2013 by
 * The Board of Trustees of the University of Illinois.
 * All rights reserved.
 */

#include <sys/types.h>
#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sys/time.h>
#include <dirent.h>
#include <time.h>
#include <random>
#include <stdio.h>      
#include <stdlib.h>     
#include "Network.h"
#include "MyTest.h"
#include "StringTokenizer.h"
#include "Simulation.h"
#include "Application.h"
#include "VeriCongestion.h"

using namespace std;

void parseTopologyFile(const string& fileName, Network& network)
{
	char buffer[1024];
	ifstream fin(fileName.c_str());
	while(fin.eof() == false)
	{
		fin.getline(buffer, 1023);
		if(strlen(buffer) == 0)	// This line is empty
		{
			continue;
		}

		if(strstr(buffer, "#") == buffer)	// This line is comment
		{
			continue;
		}

		// # Format: id ipAddress endDevice port1 nextHopIpAddress1 bandwidth1 port2 nextHopIpAddress2 bandwidth2 ...

		StringTokenizer st(buffer, " ");
		if(st.countTokens() < 3)	// This line is wrong
		{
			continue;
		}

		unsigned int id = atoi(st.nextToken().c_str());		// First token, ID
		string ipAddress = st.nextToken();					// Second token, IP
		bool endDevice = atoi(st.nextToken().c_str());		// Third token, isEndDevice

		network.addDevice(id, ipAddress, endDevice);
		while(st.hasMoreTokens() == true)
		{
			string port = st.nextToken();
			if(port.compare("#") == 0)		// If this port == '#', it's an in-line comment
			{
				break;
			}

			string nextHopIpAddress = st.nextToken();
			
			double bandwidth = -1.0;	// if it's an end host, the bandwidth doesn't matter
			if(endDevice == 0){
				bandwidth = atof(st.nextToken().c_str());
			}

			network.addPort(ipAddress, atof(port.c_str()), nextHopIpAddress,bandwidth);
		}
	}

	fin.close();
}

void generateCampusNetwork(Network& network, int m, int n, int w){	

	string address;
	string firstByte = "10.";
	string secondByte;
	string thirdByte;
	string forthByte;


	for(int i = 1; i < m+1; i++){

		// add exchange switch
		secondByte = to_string(i)+".";
		thirdByte = "0.";
		forthByte = "0";
		address = firstByte+secondByte+thirdByte+forthByte;

		//fprintf(stdout, "exchange switch: %s\n", address.c_str());
		network.addDevice(1, address, false);

		// port 1 to left neighbor, port 2 to right neighbor, port 3 to gateway 
		int left;
		int right;
		string leftExchangeAddress;
		string rightExchangeAddress;

		if(i == 1){
			left = m;
			right = i+1;
		}else if(i == m){
			left = i-1;
			right = 1;
		}else{
			left = i-1;
			right = i+1;
		}

		leftExchangeAddress = firstByte+to_string(left)+".0.0";
		rightExchangeAddress = firstByte+to_string(right)+".0.0";

		network.addPort(address, 1, leftExchangeAddress, 1000);
		network.addPort(address, 2, rightExchangeAddress, 1000);


		// add corresponding gateway switch
		thirdByte = to_string(n+1)+".";
		address = firstByte+secondByte+thirdByte+forthByte;
		//fprintf(stdout, "gateway: %s\n", address.c_str());
		network.addDevice(1, address, false);

		// port 1 to left access switch, port 2 to right access switch, port 3 to exchange switch
		string leftNeighborAddress = firstByte+secondByte+"1.0";
		string rightNeighborAddress = firstByte+secondByte+to_string(n)+".0";
		string exchangeAddress = firstByte+secondByte+"0.0";

		network.addPort(address, 1, leftNeighborAddress, 100);
		network.addPort(address, 2, rightNeighborAddress, 100);
		network.addPort(address, 3, exchangeAddress, 1000);
		network.addPort(exchangeAddress, 3, address, 1000);		// from exchange switch to gateway

		for(int j = 1; j < n+1; j++){

			// add access switches of this gateway
			thirdByte = to_string(j)+".";
			forthByte = "0";
			address = firstByte+secondByte+thirdByte+forthByte;
			//fprintf(stdout, "access switch: %s\n", address.c_str());
			network.addDevice(1, address, false);

			// w+1's port connect to "left" access switch, w+2's port connect to "right" access switch
			int left;
			int right;
			if(j == 1){
				left = n+1;
				right = j+1;
			}else{
				left = j-1;
				right = j+1;
			}

			string leftNeighborAddress = firstByte+secondByte+to_string(left)+".0";
			string rightNeighborAddress = firstByte+secondByte+to_string(right)+".0";

			network.addPort(address, w+1, leftNeighborAddress, 100);
			network.addPort(address, w+2, rightNeighborAddress, 100);

			for(int k = 1; k < w+1; k++){

				// add all hosts of this access switch
				string forthByte = to_string(k);
				string address = firstByte+secondByte+thirdByte+forthByte;
				//fprintf(stdout, "host: %s\n", address.c_str());
				network.addDevice(1, address, true);

				string myAccessSwitch = firstByte+secondByte+thirdByte+"0";
				network.addPort(address, 0, myAccessSwitch, 10);	// end host to access switch:	should be 10
				network.addPort(myAccessSwitch, k, address, 10);	// access switch to end host:	should be 10	
			}
		}
	}
}

void MyTest::myTest()
{	
	Network network;
	clock_t t1,t2;

	// send to both application and VeriFlow
	//parseTopologyFile("../demoTopo.txt", network);

	generateCampusNetwork(network, 3, 3, 3);		// m, n must be greater than 2

	Application app(network);
	VeriCongestion veriCon(network);
	
	Flow flow;

	//int congestionCounter = 0;



	// demonstrate a case when dependency graphs are generated
/*
	flow = app.issueFlow("10.1.1.1", "10.1.2.1");

	fprintf(stdout, "print out this flow\n");
	for(unsigned int i = 0; i < flow.flowPath.size(); i++){
		FlowSegment *fs = flow.flowPath[i];
		PortIdentifier sPid = fs->sourcePort;
		PortIdentifier dPid = fs->destPort;
		fprintf(stdout, "<%s, %d> -> <%s, %d>\n", sPid.deviceIP.c_str(), sPid.portID, dPid.deviceIP.c_str(), dPid.portID);
	}

	veriCon.addNewFlow(flow);

	flow = app.issueFlow("10.1.2.1", "10.1.3.1");

	fprintf(stdout, "print out this flow\n");
	for(unsigned int i = 0; i < flow.flowPath.size(); i++){
		FlowSegment *fs = flow.flowPath[i];
		PortIdentifier sPid = fs->sourcePort;
		PortIdentifier dPid = fs->destPort;
		fprintf(stdout, "<%s, %d> -> <%s, %d>\n", sPid.deviceIP.c_str(), sPid.portID, dPid.deviceIP.c_str(), dPid.portID);
	}

	veriCon.addNewFlow(flow);

	flow = app.issueFlow("10.1.3.1", "10.1.1.1");

	fprintf(stdout, "print out this flow\n");
	for(unsigned int i = 0; i < flow.flowPath.size(); i++){
		FlowSegment *fs = flow.flowPath[i];
		PortIdentifier sPid = fs->sourcePort;
		PortIdentifier dPid = fs->destPort;
		fprintf(stdout, "<%s, %d> -> <%s, %d>\n", sPid.deviceIP.c_str(), sPid.portID, dPid.deviceIP.c_str(), dPid.portID);
	}

	veriCon.verifyAFlow(flow, false);

*/

///////////////////////////////////////////exp 1///////////////////////////////////////////

	t1=clock();

	for(int i = 0; i < 400; i++){
		flow = app.issueFlow();
		if(!veriCon.verifyAFlow(flow, true)){
			//return false means there is congestion, dont add it to flowIDSet
			//congestionCounter++;
		}else{
			app.addFlowID(flow.flowID);
		}
	}

	t2=clock();
    double diff = ((float)t2-(float)t1) / CLOCKS_PER_SEC;

    fprintf(stdout, "total running time is %lf\n", diff);

//////////////////////////////////////////exp 2/////////////////////////////////////////
/*
	t1=clock();

	for(int i = 0; i < 200; i++){
		flow = app.issueFlow();
		if(!veriCon.verifyAFlow(flow, true)){
			//return false means there is congestion, dont add it to flowIDSet
			//congestionCounter++;
		}else{
			app.addFlowID(flow.flowID);
		}
	}



	// delete every flow one by one
	while(app.getIDSetSize() > 0){
		flow = app.issueFlowDeletion();
		if(!veriCon.verifyAFlow(flow, true)){
			// return false means there is congestion, dont delete from flowIDSet
			// congestionCounter++;
		}else{
			app.deleteFlowID(flow.flowID);
		}
	}

	t2=clock();
    double diff = ((float)t2-(float)t1) / CLOCKS_PER_SEC;

    fprintf(stdout, "total running time is %lf\n", diff);
*/
///////////////////////////////////////exp 3///////////////////////////////////////////
/*
    // first add 100 flows
    while(app.getIDSetSize() < 100){
    	flow = app.issueFlow();
		if(!veriCon.verifyAFlow(flow, false)){
			// return false means there is congestion, dont delete from flowIDSet
			// congestionCounter++;
		}else{
			app.addFlowID(flow.flowID);
		}
    }

    srand(time(NULL));

    int randomInt;
    int num_add = 0;
    int num_delete = 0;

	t1=clock();

    for(int i = 0; i < 400; i++){
    
    	randomInt = rand() % 2;
    	if(randomInt == 0){
    		num_add++;
    		flow = app.issueFlow();
    		if(veriCon.verifyAFlow(flow, true)){
    			app.addFlowID(flow.flowID);
    		}
    	}else{
    		num_delete++;
    		flow = app.issueFlowDeletion();
    		if(veriCon.verifyAFlow(flow, true)){
    			app.deleteFlowID(flow.flowID);
    		}
    	}
    }

    t2=clock();
    double diff = ((float)t2-(float)t1) / CLOCKS_PER_SEC;

    fprintf(stdout, "number of add = %d\n", num_add);
    fprintf(stdout, "number of deletion = %d\n", num_delete);
    fprintf(stdout, "total running time is %lf\n", diff);
*/
}
