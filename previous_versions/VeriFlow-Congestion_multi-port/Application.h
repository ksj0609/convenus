#ifndef APPLICATION_H_
#define APPLICATION_H_

#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <map>
#include <unordered_map>
#include <set>
#include <list>
#include <stack>
#include "Network.h"
#include "PortVolumnState.h"
#include "PathGenerator.h"
#include "Flow.h"

using namespace std;

class Application
{
private:
	Network myNetwork;
	uint64_t flowIDCounter;
	unordered_set<uint64_t> flowIDSet;
	vector<string> endDevices;
public:
	Application(Network& network);
	Flow issueFlow(string soruce, string dest);
	Flow issueFlow();
	Flow issueFlowDeletion();

	void addFlowID(uint64_t flowID){ this->flowIDSet.insert(flowID); }
	void deleteFlowID(uint64_t flowID){ this->flowIDSet.erase(flowID); }
	size_t getIDSetSize(){ return this->flowIDSet.size(); }

	bool generateBFSPath(string source, string dest, stack<PortIdentifier> *path);
	bool generateDFSPath(string source, string dest, stack<PortIdentifier> *path);
	bool runDFS(string source, string dest, unordered_map<string, bool> *states, stack<PortIdentifier> *path);
};

#endif /* APPLICATION_H_ */