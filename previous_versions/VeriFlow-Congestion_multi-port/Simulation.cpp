#include <sys/types.h>
#include <unistd.h>
#include <cstdio>
#include <string>
#include <unordered_map>
#include <map>
#include <set>
#include <list>
#include <algorithm>
#include "Simulation.h"
#include "Network.h"
#include "ForwardingDevice.h"
#include "PortVolumnState.h"

using namespace std;

void Simulation::setup(Network& network) {

	// iterate all ports on all switches, insert into PortMap

	unordered_map< string, ForwardingDevice >::const_iterator itr1;

	for(itr1 = network.deviceMap.begin(); itr1 != network.deviceMap.end(); itr1++)
	{
		const ForwardingDevice& device = itr1->second;

		if(device.endDevice){
			continue;
		}

		unordered_map< unsigned int, double>::const_iterator itr2;
		
		for(itr2 = device.portToBandwidthMap.begin(); itr2 != device.portToBandwidthMap.end(); itr2++){

			//fprintf(stdout, "port %d, bandwidth %lf\n", itr2->first, itr2->second);

			PortIdentifier pid;
			pid.deviceIP = device.ipAddress;
			pid.portID = itr2->first;

			PortVolumnState *pvs = new PortVolumnState();
			pvs->portState = unresolved;		// all ports are unresolved at inital state
			pvs->bandwidth = itr2->second;

			portMap[pid] = pvs;
		}
	}

	// iterate all FlowSegements from all Flows, insert into corresponding PortVolumnState

	//fprintf(stdout, "---------------------------------------------------------------------------\n");

	unordered_map< uint64_t, Flow *>::const_iterator itr3;

	for(itr3 = network.flowMap.begin(); itr3 != network.flowMap.end(); itr3++){

		const Flow *flow = itr3->second;

		vector< FlowSegment *>::const_iterator itr4;

		for(itr4 = flow->flowPath.begin(); itr4 != --flow->flowPath.end(); itr4++){

			// current segment's destination, is the device ID
			// insert current segment into the port

			FlowSegment *current = *itr4;

			if(current->segmentID != 0){
				current->in_rate = -1.0;
				current->type = unsettled;
			}

			PortIdentifier pid = current->destPort;

			if(portMap.find(pid) == portMap.end()){
				fprintf(stdout, "ERROR: wrong port identifier\n");
			}
			PortVolumnState *pvs = portMap[pid];
			pvs->in_segment.insert(current);	
		}
	}

	// after the initial setup, clear the PortMap, so that the only ones with FlowSegments left
	map< PortIdentifier, PortVolumnState * >::const_iterator itr5;
	
	for(itr5 = this->portMap.begin(); itr5 != this->portMap.end();)
	{
		//const PortIdentifier pid = itr5->first;
		const PortVolumnState *pvs = itr5->second;
		const unordered_set< FlowSegment *> segments = pvs->in_segment;

		//fprintf(stdout, "simply print all the pid, this one has device = %ld, port = %d\n", pid.deviceID, pid.portID);

		
		if(segments.empty()){
			//fprintf(stdout, "it's empty !!!!!!!!!!!!!, the port with switch = %ld, port = %d\n", pid.deviceID, pid.portID);
			itr5 = portMap.erase(itr5);
		}else{
			//fprintf(stdout, "it's not empty, the port with switch = %ld, port = %d\n", pid.deviceID, pid.portID);
			itr5++;
		}
	}
}

bool Simulation::ruleBasedUpdate(Network& network){

	map< PortIdentifier, PortVolumnState * >::iterator itr1;

	/*
		Put the whole procedure in a big while loop
		if this iteration has upgraded NOTHING, then the loop terminate
		if there is still upgrade, then continue
	*/

	bool newChangeHappend = false;
	int counter = 1;

	do{
		//fprintf(stdout, "**************************************************************************************\n");
		//fprintf(stdout, "Iteration %d\n", counter);
		//fprintf(stdout, "**************************************************************************************\n");

		newChangeHappend = false;

		for(itr1 = this->portMap.begin(); itr1 != this->portMap.end(); itr1++)
		{
			PortIdentifier pid = itr1->first;
			PortVolumnState *pvs = itr1->second;
			unordered_set< FlowSegment *> segments = pvs->in_segment;

			unordered_set< FlowSegment *>::iterator it;

			double in_rate_total;
			double in_rate_settled_total;
			double product;

			//fprintf(stdout, "Examing port < %s, %d >------------------------------------------------------------------\n", pid.deviceIP.c_str(), pid.portID);

			if(pvs->portState == resolved){
				
				//fprintf(stdout, "This port is resolved\n");
				
				continue;

			}else if(pvs->portState == transparent){
				
				//fprintf(stdout, "This port is transparent, see what I can do\n");

				// Rule 3 and 4
				for(it = segments.begin(); it != segments.end(); it++){
						
					FlowSegment *fs = *it;
					uint64_t flowID = fs->myFlowID;
					uint64_t myID = fs->segmentID;
					Flow *flow = network.flowMap[flowID];
					FlowSegment *next = flow->flowPath[++myID];
					
					if(fs->type == settled && next->type != settled){
						
						next->type = settled;
						next->in_rate = fs->in_rate;
						//fprintf(stdout, "next segment will have rate %lf\n", next->in_rate);
						newChangeHappend = true;

					}else if(fs->type == bounded){
						
						if( next->type == unsettled || (next->type == bounded && fs->in_rate < next->in_rate) ){
							next->type = bounded;
							next->in_rate = fs->in_rate;
							//fprintf(stdout, "next segment will have (bounded) rate %lf\n", next->in_rate);
							newChangeHappend = true;
						}
					} 
				}

			}else{		// unresolved
				
				//fprintf(stdout, "This port is unresolved\n");

				//1. Check if all the flows are settled
				bool allSettled = true;
				bool noUnsettled = true;

				for(it = segments.begin(); it != segments.end(); it++){
					FlowSegment *fs = *it;
					if(fs->type != settled){
						allSettled = false;
					}
					if(fs->type == unsettled){
						noUnsettled = false;
					}
				}

				//2. if all settled, rule 1
				if(allSettled){

					//fprintf(stdout, "All flows resolved!!!!\n");
					
					pvs->portState = resolved;

					// calculate sum of flows
					in_rate_total = 0.0;
					for(it = segments.begin(); it != segments.end(); it++){
						FlowSegment *fs = *it;
						in_rate_total += fs->in_rate;
					}

					//fprintf(stdout, "The total rate is %lf\n", in_rate_total);

					product = 1.0;
					if (in_rate_total > pvs->bandwidth){
						product = pvs->bandwidth / in_rate_total;
					}

					for(it = segments.begin(); it != segments.end(); it++){
						
						FlowSegment *fs = *it;
						uint64_t flowID = fs->myFlowID;
						uint64_t myID = fs->segmentID;
						Flow *flow = network.flowMap[flowID];
						FlowSegment *next = flow->flowPath[++myID];
						next->type = settled;
						next->in_rate = fs->in_rate * product;	// This update will resolve on the fly!!!!
						//fprintf(stdout, "next segment will have rate %lf\n", next->in_rate);
					}

					newChangeHappend = true;

				}else if(noUnsettled){		// settled and bounded

					//fprintf(stdout, "No flow unsettled!!!!\n");

					// calculate sum of bounded flows
					in_rate_total = 0.0;
					for(it = segments.begin(); it != segments.end(); it++){
						FlowSegment *fs = *it;
						in_rate_total += fs->in_rate;
					}

					//fprintf(stdout, "The total (bounded) rate is %lf\n", in_rate_total);
					
					if(in_rate_total <= pvs->bandwidth){

						// Rule 2, transparent
						//fprintf(stdout, "Total (bounded) rate <= bandwidth, this port is going to become transparent!!!!\n");
						pvs->portState = transparent;

						for(it = segments.begin(); it != segments.end(); it++){
						
							FlowSegment *fs = *it;
							uint64_t flowID = fs->myFlowID;
							uint64_t myID = fs->segmentID;
							Flow *flow = network.flowMap[flowID];
							FlowSegment *next = flow->flowPath[++myID];
							next->type = fs->type;
							next->in_rate = fs->in_rate;	
							//fprintf(stdout, "next segment will have rate %lf\n", next->in_rate);
						}

						newChangeHappend = true;

					}else{

						//fprintf(stdout, "Total (bounded) rate > bandwidth, NOT transparent, see what I can do\n");

						// it's NOT transparent, but we can still bound ALL flows
						// Rule 5 and 6
						in_rate_settled_total = 0.0;
					
						for(it = segments.begin(); it != segments.end(); it++){
						
							FlowSegment *fs = *it;
							if(fs->type == settled){
								in_rate_settled_total += fs->in_rate;
							}
						}

						for(it = segments.begin(); it != segments.end(); it++){
						
							FlowSegment *fs = *it;
							uint64_t flowID = fs->myFlowID;
							uint64_t myID = fs->segmentID;
							Flow *flow = network.flowMap[flowID];
							FlowSegment *next = flow->flowPath[++myID];
							
							if(fs->type == settled){

								product = 1.0;

								if(product > pvs->bandwidth / in_rate_settled_total){
									product = pvs->bandwidth / in_rate_settled_total;
								}

								double newBound = fs->in_rate * product;
								if(next->type == unsettled || (next->type == bounded && next->in_rate > newBound) ){
									//fprintf(stdout, "flow ID = %ld, next flow is given a new bound, which is %lf\n", next->myFlowID, newBound);
									next->type = bounded;
									next->in_rate = newBound;

									newChangeHappend = true;
								}

							}else if(fs->type == bounded){

								product = 1.0;

								if(product > pvs->bandwidth / (in_rate_settled_total+fs->in_rate) ){
									product = pvs->bandwidth / (in_rate_settled_total+fs->in_rate);
								}
							
								double newBound = fs->in_rate * product;
								if(next->type == unsettled || (next->type == bounded && next->in_rate > newBound) ){
									//fprintf(stdout, "flow ID = %ld, next flow is given a new bound, which is %lf\n", next->myFlowID, newBound);
									next->type = bounded;
									next->in_rate = newBound;

									newChangeHappend = true;
								}
							}
						}
					}

				}else{		// there is unsettled flow

					//fprintf(stdout, "There is unsettled flows!!!!\n");

					bool allUnsettled = true;

					in_rate_settled_total = 0.0;
					
					for(it = segments.begin(); it != segments.end(); it++){
						
						FlowSegment *fs = *it;
						if(fs->type == settled){
							allUnsettled = false;
							in_rate_settled_total += fs->in_rate;
						}else if(fs->type == bounded){
							allUnsettled = false;
						}else{
							;
						}
					}

					if(allUnsettled){
						//fprintf(stdout, "All flows are unsettled, skip this port!!!!\n");
						continue;
					}

					// Rule 5 and 6
					for(it = segments.begin(); it != segments.end(); it++){
						
						FlowSegment *fs = *it;
						uint64_t flowID = fs->myFlowID;
						uint64_t myID = fs->segmentID;
						Flow *flow = network.flowMap[flowID];
						FlowSegment *next = flow->flowPath[++myID];
						
						if(fs->type == settled){

							product = 1.0;

							if(product > pvs->bandwidth / in_rate_settled_total){
								product = pvs->bandwidth / in_rate_settled_total;
							}

							double newBound = fs->in_rate * product;
							if(next->type == unsettled || (next->type == bounded && next->in_rate > newBound) ){
								//fprintf(stdout, "Flow ID = %ld, next flow is given a new bound, which is %lf\n", next->myFlowID, newBound);
								next->type = bounded;
								next->in_rate = newBound;

								newChangeHappend = true;
							}

						}else if(fs->type == bounded){

							product = 1.0;

							if(product > pvs->bandwidth / (in_rate_settled_total+fs->in_rate) ){
								product = pvs->bandwidth / (in_rate_settled_total+fs->in_rate);
							}
						
							double newBound = fs->in_rate * product;
							if(next->type == unsettled || (next->type == bounded && next->in_rate > newBound) ){
								//fprintf(stdout, "Flow ID = %ld, next flow is given a new bound, which is %lf\n", next->myFlowID, newBound);
								next->type = bounded;
								next->in_rate = newBound;

								newChangeHappend = true;
							}
						}
					}
				}
			}
			//fprintf(stdout, "Done------------------------------------------------------------------------------------------\n");
		}

		counter++;

	}while(newChangeHappend);

	// check if all ports are resolved and transparent
	for(map< PortIdentifier, PortVolumnState* >::const_iterator it = portMap.begin(); it != portMap.end(); it++){
		PortVolumnState *pvs = it->second;
		if(pvs->portState == unresolved){
			return false;
		}
	}

	return true;
}


void Simulation::generateDependenceGraph(Network& network){

	// 1. find all nodes (unresolved ports) of the dependence graph
	map< PortIdentifier, PortVolumnState *>::const_iterator itr1;
	for(itr1 = this->portMap.begin(); itr1 != this->portMap.end(); itr1++){

		PortIdentifier pid = itr1->first;
		PortVolumnState *pvs = itr1->second;

		if(pvs->portState == unresolved){

			dependenceGraph.insert(new DependenceGraphNode(pid));
		}
	}

	unordered_set< DependenceGraphNode * >::iterator it;
	for(it = dependenceGraph.begin(); it != dependenceGraph.end(); it++){
		
		DependenceGraphNode *node = *it;
		PortIdentifier pid = node->myNode;
		PortVolumnState *pvs = portMap[pid];
		
		fprintf(stdout, "Port deviceID = %s, portID = %d, is %d\n", pid.deviceIP.c_str(), pid.portID, pvs->portState);

	}

	
	// 2. find all edges of the dependence graph

	//fprintf(stdout, "=======================iterate every flow on every unresolved port=====================================\n");

	unordered_set< DependenceGraphNode * >::iterator itr2;
	
	for(itr2 = dependenceGraph.begin(); itr2 != dependenceGraph.end(); itr2++){

		// find each unresolved port
		DependenceGraphNode *node = *itr2;


		PortIdentifier pid = node->myNode;
		//fprintf(stdout, "this port has deviceID = %s portID = %d:\n", pid.deviceIP.c_str(), pid.portID);

		PortVolumnState *pvs = portMap[pid];
		unordered_set< FlowSegment *> segments = pvs->in_segment;	// This set may contain bounded and settled segments, DO we need to execlude settled ones?
		
		// trace each flow on this port
		for(unordered_set< FlowSegment *>::const_iterator it = segments.begin(); it != segments.end(); it++){

			// TODO
			// trace until the end, or until find a segment into an unresolved port

			FlowSegment *original = *it;
			FlowSegment *current = *it;

			uint64_t flowID = current->myFlowID;
			Flow *flow = network.flowMap[flowID];

			//fprintf(stdout, "Flow ID = %ld\n", flowID);

			do{

				uint64_t myID = current->segmentID;
				//fprintf(stdout, "current segment ID = %ld\n", myID);
				
				FlowSegment *next = flow->flowPath[myID+1];

				// check if "next" is pointed to an unresolved port, if so, break, NEXT flow
				// if not, continue to next segment
					
				string nextDeviceIP = next->destPort.deviceIP;
				//string nextDeviceAddress = network.idToIpAddressMap[nextDeviceID];
				ForwardingDevice nextDevice = network.deviceMap[nextDeviceIP];

				if(nextDevice.endDevice){
				
					//fprintf(stdout, "next device is an end device !!!\n");
					break;		// cannot be an unresolved port, because end devices don't have an unresolved port
				
				}else{

					PortVolumnState *nextPVS = portMap[next->destPort];
					if(nextPVS->portState == unresolved){
						//fprintf(stdout, "next port is unresolved, generate this edge, and save the corresponding flow segment\n");
						node->directedNodes.push_back(next->destPort);
						criticalSegments.insert(next);

						causalRelation[next] = original;
					}
				}
				current = next;
			}
			while(current != flow->flowPath.back());
		}
 	}

	// 3. remove those ports with no out edges
	
	bool nodesDeleted = false;
	list< PortIdentifier > deletedNodes;


	do{
		nodesDeleted = false;

		unordered_set< DependenceGraphNode * >::const_iterator it;
		
		for(it = dependenceGraph.begin(); it != dependenceGraph.end(); ){
			
			DependenceGraphNode *node = *it;

			if(node->directedNodes.empty()){
				deletedNodes.push_back(node->myNode);
				it = dependenceGraph.erase(it);

				//fprintf(stdout, "Node <%s, %d> got deleted, since it doesn't have any out edge!!!\n", node->myNode.deviceIP.c_str(), node->myNode.portID);
				nodesDeleted = true;
			}else{
				it++;
			}
		}

		// delete the edges which pointed to this deleted node; and corresponding critical segment
		for(it = dependenceGraph.begin(); it != dependenceGraph.end(); it++){

			DependenceGraphNode *node = *it;

			for(list< PortIdentifier >::iterator iter = node->directedNodes.begin(); iter != node->directedNodes.end();){

				if(std::find( deletedNodes.begin(), deletedNodes.end(), *iter ) != deletedNodes.end() ){
					iter = node->directedNodes.erase(iter);
					// delete corresponding flow segment?
					// YES, because there are "critical segment" that point to those nodes
					// Then, they are NOT really critical

				}else{
					iter++;
				}
			}
		}

		// delete corresponding critical segment
		for(set< FlowSegment *>::const_iterator it2 = criticalSegments.begin(); it2 != criticalSegments.end();){

			FlowSegment *fs = *it2;
			PortIdentifier pid = fs->destPort;
			if(std::find( deletedNodes.begin(), deletedNodes.end(), pid) != deletedNodes.end() ){
				it2 = criticalSegments.erase(it2);
			}else{
				it2++;
			}
		}
	}
	while(nodesDeleted);
	
	printDependenceGraph();
} 

void Simulation::fixedPointIteration(Network& network){

	// objective, settle all the flow segments in the list "critical segment"

	map< FlowSegment *, double> previousIteration;	// record each critical segment's previous values

	// initialize previous iteration
	for(set< FlowSegment *>::iterator itr1 = criticalSegments.begin(); itr1 != criticalSegments.end(); itr1++){

		FlowSegment *fs = *itr1;
		previousIteration[fs] = fs->in_rate;
	}

	/*
	fprintf(stdout, "Initial values of critical segments\n");
	
	for( map< FlowSegment *, double>::const_iterator it = previousIteration.begin(); it != previousIteration.end(); it++){
		FlowSegment *fs = it->first;
		double value = it->second;
		fprintf(stdout, "segment [ flow id = %ld, segment id = %ld ] has value = %lf\n", fs->myFlowID, fs->segmentID, value);
	}
	*/



	bool foundFixedPoint;

	do{
		
		for(set< FlowSegment *>::iterator itr1 = criticalSegments.begin(); itr1 != criticalSegments.end(); itr1++){
	
			FlowSegment *current = *itr1;
			FlowSegment *causing = causalRelation[current];
			PortIdentifier pid = causing->destPort;				// causing is on port pid

			// Now, calculate current rate, using Previous values!!!

			//fprintf(stdout, "calculating flow [flow id = %ld, segment id = %ld]: \n", current->myFlowID, current->segmentID);
			//fprintf(stdout, "	the causing flow is [flow id = %ld, segment id = %ld], which is on the port <%ld, %d> \n", 
			//			causing->myFlowID, causing->segmentID, pid.deviceID, pid.portID);

			PortVolumnState *pvs = portMap[pid];
			unordered_set< FlowSegment *> segments = pvs->in_segment;

			double in_rate_total = 0.0;

			for(unordered_set< FlowSegment *>::const_iterator itr2=segments.begin(); itr2 != segments.end(); itr2++){

				FlowSegment *fs = *itr2;
				if(fs->type == settled){
					in_rate_total += fs->in_rate;
				}else{

					if( criticalSegments.find(fs) == criticalSegments.end() ){
						//fprintf(stdout, "ERROR: segment neither settled nor in critical!!!!!!!!!!!!!!\n");
						continue;
					}else{
						in_rate_total += previousIteration[fs];
					}
				}
			}

			double product = 1.0;
			if(product > pvs->bandwidth / in_rate_total){
				product = pvs->bandwidth / in_rate_total;
			}

			current->in_rate = causing->in_rate * product;
		}

		if(valuesFixed(previousIteration)){

			// values didn't change, terminate
			foundFixedPoint = true;
		}else{
			
			for(set< FlowSegment *>::iterator itr1 = criticalSegments.begin(); itr1 != criticalSegments.end(); itr1++){
				FlowSegment *fs = *itr1;
				previousIteration[fs] = fs->in_rate;
			}
			foundFixedPoint = false;

			//fprintf(stdout, "after this iteration, fixed point not found.. new values: \n");
			for( map< FlowSegment *, double>::const_iterator it = previousIteration.begin(); it != previousIteration.end(); it++){
				FlowSegment *fs = it->first;
				double value = it->second;
				fprintf(stdout, "segment [ flow id = %ld, segment id = %ld ] has value = %lf\n", fs->myFlowID, fs->segmentID, value);
			}
		}
	}
	while(!foundFixedPoint);

	//fprintf(stdout, "Now values are fixed: \n");
	set< FlowSegment *>::iterator itr3;
	
	for(itr3 = criticalSegments.begin(); itr3 != criticalSegments.end(); itr3++){
		
		FlowSegment *fs = *itr3;

		//fprintf(stdout, "final value for critical segment [ flow id = %ld, segment id = %ld ] is %lf\n", fs->myFlowID, fs->segmentID, fs->in_rate);

		fs->type = settled;
	}

}

bool Simulation::valuesFixed(map< FlowSegment *, double> previousIteration){

	for(set< FlowSegment *>::iterator itr1 = criticalSegments.begin(); itr1 != criticalSegments.end(); itr1++){

		FlowSegment *fs = *itr1;
		double previousValue = previousIteration[fs];

		fprintf(stdout, "comparing current rate with previous rate:\n");
		fprintf(stdout, "critical segment: flow = %ld, segment = %ld	", fs->myFlowID, fs->segmentID);
		fprintf(stdout, "previous value = %lf, current value = %lf\n", previousValue, fs->in_rate);

		/*
		if(previousValue != fs->in_rate){
			return false;
		}
		*/

		if(abs(previousValue - fs->in_rate) > 1.0){
			return false;
		}
	}

	return true;
}



void Simulation::printAllPort() const {

	map< PortState, std::string > portEnumMap;
	
	portEnumMap[resolved] = "resolved";
	portEnumMap[transparent] = "transparent";
	portEnumMap[unresolved] = "unresolved";

	map< FlowType, std::string > flowEnumMap;
	flowEnumMap[settled] = "settled";
	flowEnumMap[unsettled] = "unsettled";
	flowEnumMap[bounded] = "bounded";


	fprintf(stdout, "----------------------print all ports-----------------------\n");

	map< PortIdentifier, PortVolumnState * >::const_iterator itr1;
	
	for(itr1 = this->portMap.begin(); itr1 != this->portMap.end(); itr1++)
	{
		const PortIdentifier pid = itr1->first;
		const PortVolumnState *pvs = itr1->second;
		const unordered_set< FlowSegment *> segments = pvs->in_segment;

		PortState ps = pvs->portState;
		string s = portEnumMap[ps];
		fprintf(stdout, "port: switch=%s port=%d bandwidth=%lfkb/s. It's state is %s\n", pid.deviceIP.c_str(), pid.portID, pvs->bandwidth, s.c_str());

		if(segments.empty()){
			fprintf(stdout, "WTF, it's empty!!!\n");
			
		}
		
		for(unordered_set< FlowSegment* >::const_iterator it = segments.begin(); it != segments.end(); it++){

			FlowSegment *fs = *it;
			FlowType ft = fs->type;
			string str = flowEnumMap[ft];

			fprintf(stdout, "	segment: ");
			fprintf(stdout, "flowID = %ld sourcePort = <%s, %d> destPort = <%s, %d> state = %s rate = %lf\n", 
								fs->myFlowID, fs->sourcePort.deviceIP.c_str(), fs->sourcePort.portID, fs->destPort.deviceIP.c_str(), fs->destPort.portID, str.c_str(), fs->in_rate);	

		}
	}
}

void Simulation::printDependenceGraph() {

	map< PortState, std::string > portEnumMap;
	
	portEnumMap[resolved] = "resolved";
	portEnumMap[transparent] = "transparent";
	portEnumMap[unresolved] = "unresolved";

	map< FlowType, std::string > flowEnumMap;
	flowEnumMap[settled] = "settled";
	flowEnumMap[unsettled] = "unsettled";
	flowEnumMap[bounded] = "bounded";

	fprintf(stdout, "----------------print dependence graph----------------------------------\n");

	unordered_set< DependenceGraphNode * >::const_iterator it;
	for(it = dependenceGraph.begin(); it != dependenceGraph.end(); it++){
		
		DependenceGraphNode *node = *it;
		PortIdentifier pid = node->myNode;
		PortVolumnState *pvs = portMap[pid];
		
		PortState ps = pvs->portState;
		string s = portEnumMap[ps];
		fprintf(stdout, "Node: Port deviceID = %s, portID = %d, is %s\n", pid.deviceIP.c_str(), pid.portID, s.c_str());

		list< PortIdentifier >::const_iterator it2;

		for(it2 = node->directedNodes.begin(); it2 != node->directedNodes.end(); it2++){
			PortIdentifier edgePID = *it2;
			fprintf(stdout, "	Edges: Port deviceID = %s, portID = %d\n", edgePID.deviceIP.c_str(), edgePID.portID);
		}

	}

	fprintf(stdout, "corresponding critical segments\n");

	for(set< FlowSegment *>::const_iterator iter3 = criticalSegments.begin(); iter3 != criticalSegments.end(); iter3++){

		FlowSegment *fs = *iter3;
		fprintf(stdout, "	criticalSegment: flow id = %ld, segment id = %ld\n", fs->myFlowID, fs->segmentID);

	}

	for(map< FlowSegment*, FlowSegment *>::iterator iter4 = causalRelation.begin(); iter4 != causalRelation.end(); iter4++){

		FlowSegment *caused = iter4->first;
		FlowSegment *causing = iter4->second;

		PortIdentifier pid = causing->destPort;

		fprintf(stdout, "segment A is caused by segment B, A: flow ID = %ld, segment ID = %ld; B: flow ID = %ld, segment ID = %ld\n"
						, caused->myFlowID, caused->segmentID, causing->myFlowID, causing->segmentID);
		fprintf(stdout, "segment B is on the port < %s, %d >\n", pid.deviceIP.c_str(), pid.portID);
	}
}