#include <sys/types.h>
#include <unistd.h>
#include <cstdio>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <queue>
#include <algorithm>
#include "Simulation.h"
#include "Network.h"
#include "ForwardingDevice.h"
#include "PortVolumnState.h"
#include "VeriCongestion.h"
#include "Flow.h"

using namespace std;

static FILE *timeStatsFile = NULL;
static FILE *numCongestionFile = NULL; 
static FILE *congestedFlowsFile = NULL;

VeriCongestion::VeriCongestion(Network& network){
	this->wholeNetwork = network;
}

void VeriCongestion::addNewFlow(Flow flow){
	this->wholeNetwork.addFlow(flow);

/*
	vector<FlowSegment *>::const_iterator iter;

	// add each segment into the port share
	for(iter = flow.flowPath.begin(); iter != --flow.flowPath.end(); iter++){
		FlowSegment *fs = *iter;
		PortIdentifier port = fs->destPort;
		
		if(this->portShare.find(port) == this->portShare.end()){
			// there is not such port in share state yet
			vector<FlowSegment *> *newVec = new vector<FlowSegment *>();
			newVec->push_back(fs);
			this->portShare[port] = newVec;
		}else{
			vector<FlowSegment *> *vec = this->portShare[port];
			vec->push_back(fs);
		}
	}
*/
}

void VeriCongestion::deleteFlow(Flow flow){
	uint64_t flowID = flow.flowID;
	bool deleted = this->wholeNetwork.deleteFlow(flowID);
	if(!deleted){
		fprintf(stdout, "error when deleting\n");
	}else{
		//fprintf(stdout, "delete this flow with ID %ld\n", flowID);
	}
/*
	// delete each segment from the port share
	vector<FlowSegment *>::const_iterator iter;
	for(iter = flow.flowPath.begin(); iter != --flow.flowPath.end(); iter++){
		FlowSegment *fs = *iter;
		PortIdentifier port = fs->destPort;
		
		vector<FlowSegment *> *vec = this->portShare[port];
		// delete fs from the vector vec
		vec->erase(std::remove(vec->begin(), vec->end(), fs), vec->end());
	}
*/
}


unordered_set<Flow> VeriCongestion::findAffectedFlows(Flow flow){

	// find all affected flows by the new flow

	unordered_set<Flow> affectedFlows;
	affectedFlows.insert(flow);

	queue<Flow> flowQueue;
	flowQueue.push(flow);

	unordered_map<PortIdentifier, bool> portChecked;
	for(unordered_map<PortIdentifier, vector<FlowSegment *> *>::const_iterator iter = this->portShare.begin(); iter != portShare.end(); iter++){
		PortIdentifier pid = iter->first;
		portChecked[pid] = false;
	}
	//fprintf(stdout, "start finding shared flows: \n");

	while(!flowQueue.empty()){

		Flow flow = flowQueue.front();
		//fprintf(stdout, "Pull this flow from the queue: flow ID = %ld \n", flow.flowID);

		for(unsigned int i = 0; i < flow.flowPath.size()-1; i++){
		
			FlowSegment *fs = flow.flowPath[i];
			PortIdentifier pid = fs->destPort;

			//fprintf(stdout, "	flow segment: <%s, %d> -> <%s, %d>\n", fs->sourcePort.deviceIP.c_str(), fs->sourcePort.portID, pid.deviceIP.c_str(), pid.portID);
		
			if(this->portShare.find(pid) == this->portShare.end()){
				// this port is NOT shared by any flow yet
				//fprintf(stdout, "	the dest PID is NOT shared by any flow, skip this segment\n");
			}else{

				//fprintf(stdout, "	it's shared by some other segment\n");

				if(portChecked[pid] == true){
				//	fprintf(stdout, "this port is already checked, skip\n");
				}else{
					
					vector<FlowSegment *> *sharedSegments = portShare[pid];
					for(unsigned int i = 0; i < sharedSegments->size(); i++){
					
						FlowSegment *fs = sharedSegments->at(i);
				//		fprintf(stdout, "	this segment: flow id = %ld, segment id = %ld\n", fs->myFlowID, fs->segmentID);

						uint64_t flowID = fs->myFlowID;
						unordered_map< uint64_t, Flow *> flowMap = this->wholeNetwork.getFlowMap();
						Flow f = *(flowMap[flowID]);

				//		fprintf(stdout, "	I got the corresponding flow, flow id = %ld\n", f.flowID);



						if(f.flowID != flow.flowID && affectedFlows.find(f) == affectedFlows.end()){
							affectedFlows.insert(f);
							flowQueue.push(f);
						}
					}
					portChecked[pid] = true;
				}
			}
		}

		flowQueue.pop();
	}

	return affectedFlows;
}

Network VeriCongestion::generateSubNetwork(unordered_set<Flow> affectedFlows){

	Network subNetwork;
	subNetwork = this->wholeNetwork;
	subNetwork.clearFlowMap();

	for(Flow f: affectedFlows){

		f.egressRate = 0.0;
		for(unsigned int i = 1; i < f.flowPath.size(); i++){
			FlowSegment *fs = f.flowPath[i];
			fs->type = unsettled;
			fs->in_rate = -1.0;
		}
		subNetwork.addFlow(f);
	}

	return subNetwork;
}

bool VeriCongestion::verifyAFlow(Flow flow, bool exp_mode){

	clock_t before, after;
	clock_t t1, t2;
	double diff;

	if(exp_mode){

		timeStatsFile = fopen("timeStats.txt", "a");
		numCongestionFile = fopen("numCongestion.txt", "a");
		congestedFlowsFile = fopen("congestedFlows.txt", "a");


		before = clock();
	}


	fprintf(stdout, "This time I'm going to verify flow with id = %ld\n", flow.flowID);
	
	if(exp_mode){
		
		this->num_verification++;
		fprintf(timeStatsFile, "%d ", num_verification);
		fprintf(numCongestionFile, "%d ", num_verification);
		fprintf(congestedFlowsFile, "%d ", num_verification);
	}


	uint64_t flowID = flow.flowID;
	bool verifyDelete = this->wholeNetwork.flowExists(flowID);	// if this ID already exists, then we want to delete it

	//fprintf(stdout, "dddddddd\n");

	if(verifyDelete){
		flow = this->wholeNetwork.getFlow(flowID);	// get the actual flow from my own network
		deleteFlow(flow);
	}else{
		addNewFlow(flow);
	}

	int num_congested_flows = 0;

	if(exp_mode){
		t1 = clock();
	}

	//fprintf(stdout, "before simulate\n");

	num_congested_flows = simulate(this->wholeNetwork);
	
	//fprintf(stdout, "after simulate\n");

	if(exp_mode){
		/*
		t2 = clock();
		diff = ((float)t2-(float)t1) / CLOCKS_PER_SEC;
		fprintf(timeStatsFile, "%lf ", diff);
		this->total_simulation_time += diff;
		*/
	}

	if(num_congested_flows > 0){
		fprintf(stdout, "there is congestion!\n");
		
		if(exp_mode){
			num_congestion++;
		}

		if(verifyDelete){
			addNewFlow(flow);
		}else{
			deleteFlow(flow);
		}
	}

	if(exp_mode){

		after = clock();
		diff = ((float)after-(float)before) / CLOCKS_PER_SEC;
		fprintf(timeStatsFile, "%lf\n", diff);

		fprintf(numCongestionFile, "%d\n", num_congestion);
		fprintf(congestedFlowsFile, "%d\n", num_congested_flows);

		fclose(timeStatsFile);
		fclose(numCongestionFile);
		fclose(congestedFlowsFile);
	}

/*
	if(num_congested_flows > 0){
		return false;
	}else{
		return true;
	}
*/
	return (num_congested_flows == 0);
}

int VeriCongestion::simulate(Network& network){

	int num_congestion = 0;
	//network.print();

	this->simulation.setup(network);
	//this->simulation.printAllPort();		

	//fprintf(stdout, "----------------Now rule based update-----------------\n");
	//simulation.printAllPort();
	bool allPortsResolved = simulation.ruleBasedUpdate(network);

	//fprintf(stdout, "after ruleBasedUpdate-------------------------------\n");
	//this->simulation.printAllPort();

	if(allPortsResolved){
		//fprintf(stdout, "ALL PORTS RESOLVED!!\n");
		num_congestion = network.calculateEgress();		// egress rate == in rate of the end hosts
		//network.printFlows();
	}else{

		fprintf(stdout, "There is a dependence graph!!!!!!!!!!!!!!!!!!\n");
		num_dependencyGraph++;
		//fprintf(numCongestionFile, "number of dependence graph = %d\n", num_dependencyGraph);
		
		fprintf(stdout, "before generate dependency\n");

		this->simulation.generateDependenceGraph(network);
		
		fprintf(stdout, "after generate dependency\n");


		this->simulation.fixedPointIteration(network);
		//fprintf(stdout, "after fixed point iteration-----------------------------\n");
		//simulation.printAllPort();
		allPortsResolved = this->simulation.ruleBasedUpdate(network);

		//fprintf(stdout, "after ruleBasedUpdate AGAIN!!!-----------------------------\n");
		//simulation.printAllPort();

		if(allPortsResolved){
			//fprintf(stdout, "ALL PORTS RESOLVED\n");
			num_congestion = network.calculateEgress();		// egress rate == in rate of the end hosts
			network.printFlows();
		}else{
			fprintf(stdout,"ERROR: still not resolved!!!\n");
		}
	}
	return num_congestion;
}

void VeriCongestion::printPortShare() const{
	unordered_map<PortIdentifier, vector<FlowSegment *> *>::const_iterator iter;
	for(iter = this->portShare.begin(); iter != this->portShare.end(); iter++){
		PortIdentifier pid = iter->first;
		vector<FlowSegment *> *vec = iter->second;
		fprintf(stdout, "Port <%s, %d>'s flow segments:\n", pid.deviceIP.c_str(), pid.portID);
		
		
		for(unsigned int i = 0; i < vec->size(); i++){
			FlowSegment* fs = (*vec)[i];
			fprintf(stdout, "	flow ID = %ld, segment ID = %ld\n", fs->myFlowID, fs->segmentID);
		}
		
	}
}

void VeriCongestion::printFlows() const{
	this->wholeNetwork.printFlows();
}

void VeriCongestion::printStats() const{
	;
}
