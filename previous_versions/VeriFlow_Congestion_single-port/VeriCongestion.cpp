#include <sys/types.h>
#include <unistd.h>
#include <cstdio>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <queue>
#include <algorithm>
#include "Simulation.h"
#include "Network.h"
#include "ForwardingDevice.h"
#include "PortVolumnState.h"
#include "VeriCongestion.h"
#include "Flow.h"

using namespace std;

static FILE *timeStatsFile = NULL;
static FILE *numCongestionFile = NULL; 
static FILE *congestedFlowsFile = NULL;
static FILE *numDependencyGraphFile = NULL;
static FILE *graphSizeFile = NULL;

static FILE *simulationBreakdownFile = NULL;

VeriCongestion::VeriCongestion(Network& network){
	this->wholeNetwork = network;
}


void VeriCongestion::addNewFlow(Flow flow){

	this->wholeNetwork.addFlow(flow);

	vector<FlowSegment *>::const_iterator iter;
	/*
	// add each segment into the port share
	for(iter = flow.flowPath.begin(); iter != --flow.flowPath.end(); iter++){
		FlowSegment *fs = *iter;
		PortIdentifier port = fs->destPort;
		
		if(this->portShare.find(port) == this->portShare.end()){
			// there is not such port in share state yet
			vector<FlowSegment *> *newVec = new vector<FlowSegment *>();
			newVec->push_back(fs);
			this->portShare[port] = newVec;
		}else{
			vector<FlowSegment *> *vec = this->portShare[port];
			vec->push_back(fs);
		}
	}
	*/
}


void VeriCongestion::deleteFlow(Flow flow){
	uint64_t flowID = flow.flowID;
	bool deleted = this->wholeNetwork.deleteFlow(flowID);
	if(!deleted){
		fprintf(stdout, "error when deleting\n");
	}else{
		//fprintf(stdout, "delete this flow with ID %ld\n", flowID);
	}
/*
	// delete each segment from the port share
	vector<FlowSegment *>::const_iterator iter;
	for(iter = flow.flowPath.begin(); iter != --flow.flowPath.end(); iter++){
		FlowSegment *fs = *iter;
		PortIdentifier port = fs->destPort;
		
		vector<FlowSegment *> *vec = this->portShare[port];
		// delete fs from the vector vec
		vec->erase(std::remove(vec->begin(), vec->end(), fs), vec->end());
	}
*/
}

/*
unordered_set<Flow> VeriCongestion::findAffectedFlows(Flow flow){

	// find all affected flows by the new flow

	unordered_set<Flow> affectedFlows;
	affectedFlows.insert(flow);

	queue<Flow> flowQueue;
	flowQueue.push(flow);

	unordered_map<string, bool> portChecked;

	for(unordered_map<string, vector<FlowSegment *> *>::const_iterator iter = this->portShare.begin(); iter != portShare.end(); iter++){
		string ip = iter->first;
		portChecked[ip] = false;
	}
	//fprintf(stdout, "start finding shared flows: \n");

	while(!flowQueue.empty()){

		Flow flow = flowQueue.front();
		//fprintf(stdout, "Pull this flow from the queue: flow ID = %ld \n", flow.flowID);

		for(unsigned int i = 0; i < flow.flowPath.size()-1; i++){
		
			FlowSegment *fs = flow.flowPath[i];
			string ip = fs->destIPAddress;

			//fprintf(stdout, "	flow segment: <%s, %d> -> <%s, %d>\n", fs->sourcePort.deviceIP.c_str(), fs->sourcePort.portID, pid.deviceIP.c_str(), pid.portID);
		
			if(this->portShare.find(ip) == this->portShare.end()){
				// this port is NOT shared by any flow yet
				//fprintf(stdout, "	the dest PID is NOT shared by any flow, skip this segment\n");
			}else{

				//fprintf(stdout, "	it's shared by some other segment\n");

				if(portChecked[ip] == true){
				//	fprintf(stdout, "this port is already checked, skip\n");
				}else{
					
					vector<FlowSegment *> *sharedSegments = portShare[ip];
					for(unsigned int i = 0; i < sharedSegments->size(); i++){
					
						FlowSegment *fs = sharedSegments->at(i);
				//		fprintf(stdout, "	this segment: flow id = %ld, segment id = %ld\n", fs->myFlowID, fs->segmentID);

						uint64_t flowID = fs->myFlowID;
						unordered_map< uint64_t, Flow *> flowMap = this->wholeNetwork.getFlowMap();
						Flow f = *(flowMap[flowID]);

				//		fprintf(stdout, "	I got the corresponding flow, flow id = %ld\n", f.flowID);



						if(f.flowID != flow.flowID && affectedFlows.find(f) == affectedFlows.end()){
							affectedFlows.insert(f);
							flowQueue.push(f);
						}
					}
					portChecked[ip] = true;
				}
			}
		}

		flowQueue.pop();
	}

	return affectedFlows;
}
*/

/*
Network VeriCongestion::generateSubNetwork(unordered_set<Flow> affectedFlows){

	Network subNetwork;
	subNetwork = this->wholeNetwork;
	subNetwork.clearFlowMap();

	for(Flow f: affectedFlows){

		f.egressRate = -1.0;
		for(unsigned int i = 1; i < f.flowPath.size(); i++){
			FlowSegment *fs = f.flowPath[i];
			fs->type = unsettled;
			fs->in_rate = -1.0;
		}
		subNetwork.addFlow(f);
	}

	return subNetwork;
}
*/

bool VeriCongestion::verifyAFlow(Flow flow, bool exp_mode){

	clock_t before, after;
	clock_t t1, t2;
	double diff;

	if(exp_mode){

		timeStatsFile = fopen("timeStats.txt", "a");
		numCongestionFile = fopen("numCongestion.txt", "a");
		congestedFlowsFile = fopen("congestedFlows.txt", "a");
		numDependencyGraphFile = fopen("numDependencyGraph.txt", "a");
		graphSizeFile = fopen("graphSize.txt", "a");
		simulationBreakdownFile = fopen("simulationBreakdown.txt", "a");

		before = clock();
	}

	fprintf(stdout, "\n");
	fprintf(stdout, "\n");
	fprintf(stdout, "This time I'm going to verify flow with id = %ld\n", flow.flowID);
	
	if(exp_mode){
		
		this->num_verification++;
		fprintf(timeStatsFile, "%d ", num_verification);
		fprintf(numCongestionFile, "%d ", num_verification);
		fprintf(congestedFlowsFile, "%d ", num_verification);
		fprintf(numDependencyGraphFile, "%d ", num_verification);
		fprintf(graphSizeFile, "%d ", num_verification);
		fprintf(simulationBreakdownFile, "%d ", num_verification);
	}

	uint64_t flowID = flow.flowID;
	bool verifyDelete = this->wholeNetwork.flowExists(flowID);	// if this ID already exists, then we want to delete it

	if(verifyDelete){
		flow = this->wholeNetwork.getFlow(flowID);	// get the actual flow from my own network
		//deleteFlow(flow);	dont delete now, because we need to modify the portList in simulation
	}else{
		addNewFlow(flow);	//dont add it now, we can add into portList individually
	}

	int num_congested_flows = 0;

	if(exp_mode){
		t1 = clock();
	}

	//fprintf(stdout, "before simulate\n");
	bool addFlow;
	if(verifyDelete){
		addFlow = false;
	}else{
		addFlow = true;
	}

	if(exp_mode){
		num_congested_flows = simulate(&this->wholeNetwork, flow, addFlow, simulationBreakdownFile, true);
	}else{
		num_congested_flows = simulate(&this->wholeNetwork, flow, addFlow, simulationBreakdownFile, false);
	}

	
	//fprintf(stdout, "after simulate\n");

	if(exp_mode){
		
		t2 = clock();
		diff = ((float)t2-(float)t1) / CLOCKS_PER_SEC;
		
		fprintf(simulationBreakdownFile, "%lf\n", diff);
		fclose(simulationBreakdownFile);
	}

	if(num_congested_flows > 0){
		fprintf(stdout, "there is congestion!\n");
		
		if(exp_mode){
			num_congestion++;
		}

		if(verifyDelete){
			;
		}else{
			deleteFlow(flow);
		}

	}else{
		// no congestion, perform the actual add/delete finally
		if(verifyDelete){
			deleteFlow(flow);
		}else{
			;
		}
	}

	if(exp_mode){

		after = clock();
		diff = ((float)after-(float)before) / CLOCKS_PER_SEC;
		fprintf(timeStatsFile, "%lf\n", diff);

		if(current_graph_size > 0){
			this->num_dependencyGraph++;
		}

		fprintf(numCongestionFile, "%d\n", num_congestion);
		fprintf(congestedFlowsFile, "%d\n", num_congested_flows);
		fprintf(numDependencyGraphFile, "%d\n", num_dependencyGraph);
		fprintf(graphSizeFile, "%d\n", current_graph_size);

		fclose(timeStatsFile);
		fclose(numCongestionFile);
		fclose(congestedFlowsFile);
		fclose(numDependencyGraphFile);
		fclose(graphSizeFile);
	}

	return (num_congested_flows == 0);
}

int VeriCongestion::simulate(Network* network, Flow flow, bool addFlow, FILE *pFile, bool exp_mode){

	clock_t t1, t2;
	double diff;

	int num_congestion = 0;

	if(exp_mode){
		t1 = clock();
	}

	//fprintf(stdout, "before setup\n");

	simulation.setup(network, flow, addFlow);

	//fprintf(stdout, "after setup\n");

	if(exp_mode){
		t2 = clock();
		diff = ((float)t2-(float)t1) / CLOCKS_PER_SEC;
		fprintf(simulationBreakdownFile, "%lf ", diff);
		t1 = clock();
	}

	//fprintf(stdout, "before rule based\n");

	bool allPortsResolved = simulation.ruleBasedUpdate(network);

	//fprintf(stdout, "after rule based\n");


	if(exp_mode){
		t2 = clock();
		diff = ((float)t2-(float)t1) / CLOCKS_PER_SEC;
		fprintf(simulationBreakdownFile, "%lf ", diff);
		t1 = clock();
	}

	if(allPortsResolved){

		//fprintf(stdout, "Time to calculate the egress rate for each flow\n");
		num_congestion = simulation.calculateEgress(network, flow, addFlow);		// egress rate == in rate of the end hosts
		
		this->current_graph_size = 0;
		diff = 0.0;

		if(exp_mode){
			fprintf(simulationBreakdownFile, "%lf %lf %lf ", diff, diff, diff);
		}

	}else{

		fprintf(stdout, "there is dependency graph!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");

		//fprintf(stdout, "before generate graph\n");

		this->current_graph_size = simulation.generateDependenceGraph(network);

		//fprintf(stdout, "after generate graph\n");

		if(exp_mode){
			t2 = clock();
			diff = ((float)t2-(float)t1) / CLOCKS_PER_SEC;
			fprintf(simulationBreakdownFile, "%lf ", diff);
			t1 = clock();
		}

		//fprintf(stdout, "before fixed pointer iteration\n");

		simulation.fixedPointIteration(network);

		//fprintf(stdout, "after fixed point iteration\n");
		
		if(exp_mode){
			t2 = clock();
			diff = ((float)t2-(float)t1) / CLOCKS_PER_SEC;
			fprintf(simulationBreakdownFile, "%lf ", diff);
			t1 = clock();
		}

		allPortsResolved = simulation.ruleBasedUpdate(network);

		if(exp_mode){
			t2 = clock();
			diff = ((float)t2-(float)t1) / CLOCKS_PER_SEC;
			fprintf(simulationBreakdownFile, "%lf ", diff);
			t1 = clock();
		}

		if(allPortsResolved){
			//fprintf(stdout, "ALL PORTS RESOLVED\n");
			//fprintf(stdout, "Time to calculate the egress rate for each flow\n");
			num_congestion = simulation.calculateEgress(network, flow, addFlow);		// egress rate == in rate of the end hosts
		}else{
			fprintf(stdout,"ERROR: still not resolved!!!\n");
		}
	}

	bool congestion;
	if(num_congestion > 0){
		congestion = true;
	}else{
		congestion = false;
	}

	simulation.clearup(network, flow, addFlow, congestion);	

	return num_congestion;
}

/*
void VeriCongestion::printPortShare() const{
	unordered_map<PortIdentifier, vector<FlowSegment *> *>::const_iterator iter;
	for(iter = this->portShare.begin(); iter != this->portShare.end(); iter++){
		PortIdentifier pid = iter->first;
		vector<FlowSegment *> *vec = iter->second;
		fprintf(stdout, "Port <%s, %d>'s flow segments:\n", pid.deviceIP.c_str(), pid.portID);
		
		
		for(unsigned int i = 0; i < vec->size(); i++){
			FlowSegment* fs = (*vec)[i];
			fprintf(stdout, "	flow ID = %ld, segment ID = %ld\n", fs->myFlowID, fs->segmentID);
		}
		
	}
}
*/

void VeriCongestion::printFlows() const{
	this->wholeNetwork.printFlows();
}

void VeriCongestion::printStats() const{
	;
}
