#ifndef FLOW_H_
#define FLOW_H_

#include <sys/types.h>
#include <unistd.h>
#include <stdint.h>
#include <string>
#include <unordered_map>
#include <vector>
#include "ForwardingDevice.h"

using namespace std;

enum FlowType {settled, unsettled, bounded};

struct ForwardingDevice;

struct FlowSegment{
public:
	uint64_t myFlowID;	// which flow this segment belongs to
	uint64_t segmentID;	// this segment's ID

	FlowType type;
	string sourceIPAddress;
	string destIPAddress;

	double in_rate;

	void printMyself(){
		fprintf(stdout, "flow ID = %ld, segmentID = %ld, source = %s, dest = %s, in_rate = %lf\n", 
					this->myFlowID, this->segmentID, this->sourceIPAddress.c_str(), this->destIPAddress.c_str(), this->in_rate);
	}
};

struct Flow
{
public:
	uint64_t flowID;
	double ingressRate;
	double egressRate;
	vector<FlowSegment *> flowPath;

	void setEgress(double e){
		this->egressRate = e;
	}


	bool operator==(const Flow &o) const {
		return flowID == o.flowID;
	}

	bool operator<(const Flow &o) const {
		return flowID < o.flowID;
	}

	bool operator!=(const Flow &o) const{
		return flowID != o.flowID;
	}

	void printMyself(){
		fprintf(stdout, "Flow_id: %lu ingressRate %f egressRate %f \n", this->flowID, this->ingressRate, this->egressRate);
		vector< FlowSegment *>::const_iterator it;
		for(it = this->flowPath.begin(); it != this->flowPath.end(); it++){
			(*it)->printMyself();
		}
	}

};

namespace std{

  template <>
  struct hash<Flow>
  {
    std::size_t operator()(const Flow& f) const
    {
      using std::size_t;
      using std::hash;
      using std::string;

      // Compute individual hash values for first,
      // second and third and combine them using XOR
      // and bit shifting:

      return ( (hash<uint64_t>()(f.flowID) ^ (hash<double>()(f.ingressRate) << 1) ) >> 1);
    }
  };
}

#endif /* FLOW_H_ */
