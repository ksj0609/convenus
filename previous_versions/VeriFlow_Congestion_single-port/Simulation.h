#ifndef SIMULATION_H_
#define SIMULATION_H_

#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <map>
#include <unordered_map>
#include <set>
#include <list>
#include "Network.h"
#include "PortVolumnState.h"

using namespace std;

struct PortTuple{
	string portIP;
	PortVolumnState *pvs;

	PortTuple(string s, PortVolumnState *p){
		this->portIP = s;
		this->pvs = p;
	}
};


struct DependenceGraphNode{

	string myNode;
	list< string > directedNodes;	// this is the set of edges, each edge will have one or more critical segments


	DependenceGraphNode(string ip){
		myNode = ip;
	}
};

class Simulation
{
private:

	int counterExample = 0;

	//map< string, PortVolumnState* > portMap;
	list<PortTuple> portList;

	unordered_set< DependenceGraphNode *> dependenceGraph;
	set< FlowSegment *> criticalSegments;

	map< FlowSegment *, FlowSegment *> causalRelation;	// This data structure record the causal relation between segments

public:
	void setup(Network *network, Flow flow, bool addFlow);
	bool ruleBasedUpdate(Network network);		// return true if all ports resolved; else return false
	int generateDependenceGraph(Network network);
	void fixedPointIteration(Network network);
	int calculateEgress(Network network, Flow flow, bool addFlow);

	bool valuesFixed(map< FlowSegment *, double> previousIteration);
	void clearup(Network network, Flow flow, bool addFlow, bool congestion);

	void printAllPort() const;
	void printDependenceGraph(Network network, int size);
	void printPortList();

};

#endif /* SIMULATION_H_ */