import matplotlib.pyplot as plt
import csv
import numpy as np

# use csv

x = []
y = []

with open('../numDependencyGraph.txt','r') as csvfile:
	plots = csv.reader(csvfile, delimiter=' ')
	for row in plots:
		x.append(int(row[0]))
		y.append(float(row[1]))

f1 = plt.figure(1)

plt.plot(x, y)

plt.xlabel('verification #')
plt.ylabel('# graphs')
plt.title('Number of Dependency Graphs')

#plt.show()
plt.savefig('num_graph_timeline.png')
