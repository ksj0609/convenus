import matplotlib.pyplot as plt
import csv
import numpy as np

# use csv

x = []
y = []

with open('../graphSize.txt','r') as csvfile:
	plots = csv.reader(csvfile, delimiter=' ')
	for row in plots:
		x.append(int(row[0]))
		y.append(int(row[1]))

f1 = plt.figure(1)

plt.plot(x, y)

plt.xlabel('verification #')
plt.ylabel('graph size')
plt.title('Size of Each Graph')

#plt.show()
plt.savefig('graph_size_timeline.png')
