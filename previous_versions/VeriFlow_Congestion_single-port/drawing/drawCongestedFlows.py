import matplotlib.pyplot as plt
import csv
import numpy as np

# use csv

x = []
y = []

with open('../congestedFlows.txt','r') as csvfile:
	plots = csv.reader(csvfile, delimiter=' ')
	for row in plots:
		x.append(int(row[0]))
		y.append(float(row[1]))

f1 = plt.figure(1)

plt.plot(x, y)

plt.xlabel('verification #')
plt.ylabel('# congested flows')
plt.title('Number of Congested Flows')

#plt.show()
plt.savefig('congested_flows.png')
