import matplotlib.pyplot as plt
import csv
import numpy as np

# use csv

x = []	# verification number
y = []	# setup time
z = []	# phase 1
a = []	# phase 2
b = []	# phase 3
c = []	# phase 4
d = []	# total
with open('../simulationBreakdown.txt','r') as csvfile:
	plots = csv.reader(csvfile, delimiter=' ')
	for row in plots:
		x.append(int(row[0]))
		y.append(float(row[1]))
		z.append(float(row[2]))
		a.append(float(row[3]))
		b.append(float(row[4]))
		c.append(float(row[5]))
		d.append(float(row[6]))

plt.plot(x, y, label='setup Time')
plt.plot(x, z, label='phase 1')
plt.plot(x, a, label='phase 2')
plt.plot(x, b, label='phase 3')
plt.plot(x, c, label='phase 4')
plt.plot(x, d, label='total')

plt.xlabel('Verification #')
plt.ylabel('Verification Time')
plt.title('Simulation Breakdown')

plt.legend(loc='upper center')
#plt.show()
plt.savefig('simulation_breakdown.png')
