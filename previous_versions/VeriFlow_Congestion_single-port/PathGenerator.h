#ifndef PATHGENERATOR_H_
#define PATHGENERATOR_H_

#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <map>
#include <unordered_map>
#include <set>
#include <list>
#include <stack>
#include "Network.h"
#include "PortVolumnState.h"
#include "Flow.h"

using namespace std;

class PathGenerator
{
private:
	Network *myNetwork;

public:
	PathGenerator(Network network);
	//void printMyNetwork();
	//void printMyNetworkFlows();
	bool constructFlow(uint64_t flowID, double ingressRate, string source, string dest);
	bool runDFS(string source, string dest, unordered_map<string, bool> *states, stack<PortIdentifier> *path);
};

#endif /* PATHGENERATOR_H_ */