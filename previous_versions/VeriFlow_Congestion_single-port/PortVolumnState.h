#ifndef PORTVOLUMNSTATE_H_
#define PORTVOLUMNSTATE_H_

#include <sys/types.h>
#include <unistd.h>
#include <stdint.h>
#include <string>
#include <unordered_set>
#include <vector>
#include <map>
#include "Flow.h"

using namespace std;

enum PortState {resolved, transparent, unresolved};

struct PortVolumnState{
public:
	PortState portState;
	double bandwidth;				
	unordered_set< FlowSegment *> in_segment;	// The flows come into of this port

	void printMyself(){

		fprintf(stdout, "bandwidth = %lf, port state = %d\n", this->bandwidth, this->portState);
		fprintf(stdout, "segments\n");
		for(unordered_set<FlowSegment *>::const_iterator it = this->in_segment.begin(); it != this->in_segment.end(); it++){
			FlowSegment *fs = *it;
			fs->printMyself();
		}
	}	
};

#endif /* PORTVOLUMNSTATE_H_ */