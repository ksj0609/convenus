#ifndef VeriCongestion_H_
#define VeriCongestion_H_

#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <list>
#include "Network.h"
#include "PortVolumnState.h"
#include "Simulation.h"
#include "Flow.h"

using namespace std;

class VeriCongestion
{
private:
	Network wholeNetwork;
	Simulation simulation;

	unordered_map<string, uint64_t> portShare;	// record each port is shared by which flows

	int num_verification = 0;
	int num_congestion = 0;
	int num_dependencyGraph = 0;
	int current_graph_size = 0;
	double avg_affectedFlows = 0.0;

	// statistics to measure
	double total_find_affected_time = 0.0;
	double total_generate_subnetwork_time = 0.0;
	double total_simulation_time = 0.0;

public:
	VeriCongestion(Network& network);
	void addNewFlow(Flow flow);
	void deleteFlow(Flow flow);
	
	//unordered_set<Flow> findAffectedFlows(Flow flow);
	//Network generateSubNetwork(unordered_set<Flow> affectedFlows);

	int simulate(Network* network, Flow flow, bool addFlow, FILE *pFile, bool exp_mode);
	bool verifyAFlow(Flow flow, bool exp_mode);

	void printPortShare() const;
	void printFlows() const;
	void printStats() const;
};

#endif /* VeriCongestion_H_ */