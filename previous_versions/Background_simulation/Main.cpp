#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <signal.h>
#include <stdint.h>
#include "StringTokenizer.h"
#include "Network.h"
#include "Flow.h"
#include "PortVolumnState.h"
#include "Simulation.h"
#include <list>
#include <vector>
#include <sys/time.h>
#include <unordered_map>
#include <unordered_set>

using namespace std;

void parseTopologyFile(const string& fileName, Network& network)
{
	char buffer[1024];
	ifstream fin(fileName.c_str());
	while(fin.eof() == false)
	{
		fin.getline(buffer, 1023);
		if(strlen(buffer) == 0)	// This line is empty
		{
			continue;
		}

		if(strstr(buffer, "#") == buffer)	// This line is comment
		{
			continue;
		}

		// # Format: id ipAddress endDevice port1 nextHopIpAddress1 bandwidth1 port2 nextHopIpAddress2 bandwidth2 ...

		StringTokenizer st(buffer, " ");
		if(st.countTokens() < 3)	// This line is wrong
		{
			continue;
		}

		unsigned int id = atoi(st.nextToken().c_str());		// First token, ID
		string ipAddress = st.nextToken();					// Second token, IP
		bool endDevice = atoi(st.nextToken().c_str());		// Third token, isEndDevice
		
		/*
		double bandwidth = -1.0;

		if(endDevice == 0){
			bandwidth = stod(st.nextToken().c_str());	// Forth token, switch bandwidth
		}

		network.addDevice(id, ipAddress, endDevice, bandwidth);
		*/

		network.addDevice(id, ipAddress, endDevice);
		while(st.hasMoreTokens() == true)
		{
			string port = st.nextToken();
			if(port.compare("#") == 0)		// If this port == '#', it's an in-line comment
			{
				break;
			}

			string nextHopIpAddress = st.nextToken();
			
			double bandwidth = -1.0;	// if it's an end host, the bandwidth doesn't matter
			if(endDevice == 0){
				bandwidth = stod(st.nextToken().c_str());
			}

			network.addPort(ipAddress, atoi(port.c_str()), nextHopIpAddress,bandwidth);
		}
	}

	fin.close();
}



void parseFlowFile(const string& fileName, Network& network){

	char buffer[1024];
	ifstream fin(fileName.c_str());
	
	while(fin.eof() == false){

		fin.getline(buffer, 1023);
		if(strlen(buffer) == 0)	// This line is empty
		{
			continue;
		}

		if(strstr(buffer, "#") == buffer)	// This line is comment
		{
			continue;
		}

		// # Format: id volumn hostID port switch 1, port, switch 2, port, ... switch n, port, hostID port
		StringTokenizer st(buffer, " ");
		if(st.countTokens() < 6)	// This line is wrong
		{
			continue;
		}

		unsigned int id = atoi(st.nextToken().c_str());		// First token, flow ID
		double rate = stod(st.nextToken().c_str());			// Second token, volumn

		network.addFlow(id, rate, 0.0);		// rate is ingress rate, egress rate is unknown, so it's set 0.0

		uint64_t previousDeviceID = atoi(st.nextToken().c_str()); 	// Host ID
		uint64_t previousDevicePort = atoi(st.nextToken().c_str()); // Host port

		uint64_t currentDeviceID = atoi(st.nextToken().c_str()); 	// first switch's ID
		uint64_t currentDevicePort = atoi(st.nextToken().c_str());	// first switch's port

		uint64_t segmentCounter = 0;

		network.addFlowSegment(id, segmentCounter, previousDeviceID, previousDevicePort, currentDeviceID, currentDevicePort, rate, settled);
		segmentCounter++;

		
		while(st.hasMoreTokens() == true)
		{
			// add each flow segment to Flow ID = "id"
			// first segment is "settled", from Host to first switch
			// the rest are "unsettled"
			// push into the flow's "flowPath" vector

			string currentTokenDevice = st.nextToken();

			if(currentTokenDevice.compare("#") == 0)		// If this port == '#', it's an in-line comment
			{
				break;
			}

			string currentTokenPort = st.nextToken();

			previousDeviceID = currentDeviceID;
			previousDevicePort = currentDevicePort;
		
			currentDeviceID = atoi(currentTokenDevice.c_str());
			currentDevicePort = atoi(currentTokenPort.c_str());

			network.addFlowSegment(id, segmentCounter, previousDeviceID, previousDevicePort, currentDeviceID, currentDevicePort, -1.0, unsettled);	// This id is Flow_id
			segmentCounter++;
		}
		
	}

	fin.close();
}

int main(int argc, char** argv)
{	
  	Network network;
  	Simulation simulation;

	string topologyFileName = argv[1];
	string flowFileName = argv[2];

	parseTopologyFile(topologyFileName, network);
	parseFlowFile(flowFileName, network);

	network.print();
	network.printFlows();

	// Having topology and flow file, now run the four-phase algorithm
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	simulation.setup(network);
	simulation.printAllPort();		

	
	bool allPortsResolved = simulation.ruleBasedUpdate(network);

	fprintf(stdout, "after ruleBasedUpdate-------------------------------\n");
	simulation.printAllPort();

	if(allPortsResolved){
		fprintf(stdout, "ALL PORTS RESOLVED!!\n");
		network.calculateEgress();	
		network.printFlows();
	}else{
		simulation.generateDependenceGraph(network);
		simulation.fixedPointIteration(network);
		fprintf(stdout, "after fixed point iteration-----------------------------\n");
		//simulation.printAllPort();
		allPortsResolved = simulation.ruleBasedUpdate(network);

		fprintf(stdout, "after ruleBasedUpdate AGAIN!!!-----------------------------\n");
		//simulation.printAllPort();

		if(allPortsResolved){
			fprintf(stdout, "ALL PORTS RESOLVED\n");
			network.calculateEgress();		// egress rate == in rate of the end hosts
			network.printFlows();
		}else{
			fprintf(stdout,"ERROR: still not resolved!!!\n");
		}
	}

}
