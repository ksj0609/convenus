#ifndef FLOW_H_
#define FLOW_H_

#include <sys/types.h>
#include <unistd.h>
#include <stdint.h>
#include <string>
#include <unordered_map>
#include <vector>
#include "ForwardingDevice.h"

using namespace std;

struct PortIdentifier{

	uint64_t deviceID;
	unsigned int portID;

	bool operator==(const PortIdentifier &o) const {
		return deviceID == o.deviceID && portID == o.portID;
	}

	bool operator<(const PortIdentifier &o) const {
		return deviceID < o.deviceID || (deviceID == o.deviceID && portID < o.portID);
	}
};

enum FlowType {settled, unsettled, bounded};

struct ForwardingDevice;

struct FlowSegment{
public:
	uint64_t myFlowID;	// which flow this segment belongs to
	uint64_t segmentID;	// this segment's ID
	FlowType type;
	//uint64_t sourceDeviceID;	// TO DO: change this to source port
	//uint64_t destDeviceID;		// TO DO: change this to dest port
	
	PortIdentifier sourcePort;
	PortIdentifier destPort;

	double in_rate;
};

struct Flow
{
public:
	uint64_t flowID;
	double ingressRate;
	double egressRate;
	vector<FlowSegment *> flowPath;

	void setEgress(double e){
		this->egressRate = e;
	}
};

#endif /* FLOW_H_ */
