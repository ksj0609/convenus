%**************************************************************************
%*
%*  Paper: ``INSTRUCTIONS FOR AUTHORS OF LATEX (Poster) ABSTRACTS''
%*
%*  Publication: 2016 Winter Simulation Conference Author Kit
%*
%*  Filename: wsc16poster.tex
%*
%*      DATE: Jan 14, 2016
%*
%*  Word Processing System: TeXnicCenter and MiKTeX
%*
%*
%*  All files need the following
\input{wsc16style.tex}     % download from author kit.  Style files for wsc formatting

\documentclass{wscposterproc}
\usepackage{latexsym}
%\usepackage{caption}
\usepackage{graphicx}
\usepackage{mathptmx}
%
%****************************************************************************
% AUTHOR: You may want to use some of these packages. (Optional)
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsbsy}
\usepackage{amsthm}
\usepackage{subcaption}
%****************************************************************************


%
%****************************************************************************
% AUTHOR: If you do not wish to use hyperlinks, then just comment
% out the hyperref usepackage commands below.

%% This version of the command is used if you use pdflatex. In this case you
%% cannot use ps or eps files for graphics, but pdf, jpeg, png etc are fine.

\usepackage[pdftex,colorlinks=true,urlcolor=blue,citecolor=black,anchorcolor=black,linkcolor=black]{hyperref}

%% The next versions of the hyperref command are used if you adopt the
%% traditional latex-dvips-ps2pdf route in generating your pdf file. In
%% this case you can use ps or eps files for graphics, but not pdf, jpeg, png etc.
%% If you are using WinEdt or PCTeX, then use the following. If you are using
%% Y&Y TeX then replace "dvips" with "dvipsone"

%%\usepackage[dvips,colorlinks=true,urlcolor=blue,citecolor=black,%
%% anchorcolor=black,linkcolor=black]{hyperref}
%****************************************************************************



%****************************************************************************
% AUTHOR: You may need to use the following command to adjust the space on the title page
% to properly list all authors.
% Please use: 2.5in for 1 row of authors, 3.75in [default] for 2 rows of authors
%\setlength{\titlevboxsize}{2.5in}  % 2.5in for 1 row of authors, 3.75in for 2 rows of authors
%****************************************************************************


%
%****************************************************************************
%*
%* AUTHOR: YOUR CALL!  Document-specific macros can come here.
%*
%****************************************************************************


\newcommand{\comment}[1]{}

%\setlength\textfloatsep{-0.3pt}	% reduce space between a float and text 
%\setlength\intextsep{-0.3pt}		% reduce space between a float and text
\setlength{\belowcaptionskip}{-0.1cm}	% reduce space below caption


%#########################################################
%*
%*  The Document.
%*
\begin{document}
%***************************************************************************
% AUTHOR: AUTHOR NAMES GO HERE
% FORMAT AUTHORS NAMES Like: Author1, Author2 and Author3 (last names)
%
%		You need to change the author listing below!
%               Please list ALL authors using last name only, separate by a comma except
%               for the last author, separate with "and"
%
% These names will be used from the 2nd page on - thus for one page abstracts this can stay as it is.
% DON'T REMOVE THIS LINE!
%
\WSCpagesetup{Liu and Jin}

% AUTHOR: Enter the title, all letters in upper case
\title{AUTOMATIC COMPOSITION AND CONFLICT RECONCILIATION OF HIGH-LEVEL NETWORK POLICY WITH STATE MACHINE BASED ABSTRACTION}

% AUTHOR: Enter the author(s) of the poster abstract
\author{Xin Liu\\[12pt]
Department of Computer Science \\
Illinois Institute of Technology\\
10 West 31st Street\\
Chicago, IL 60608, USA
\and
Dong Jin\\[12pt]
Department of Computer Science \\
Illinois Institute of Technology\\
10 West 31st Street\\
Chicago, IL 60608, USA
}

\maketitle

\vspace{-1cm}
\section*{ABSTRACT}
%less than 150 words.
Software-defined networking (SDN) offers high-level abstractions to specify network policies for efficient network management. However, existing frameworks do not support automatic composition of polices written by different applications in the same physical network. In this work, we propose a new framework, policy state machine abstraction (PSMA) that provides a library for applications to specify their policies using state machines as well as an underlying system to compose multiple state machines into a global one, while reconciling possible conflicts that arise during the composition.
%\begin{center}
\vspace{-0.1cm}
\section{INTRODUCTION}
\vspace{-0.1cm}
%\end{center}
With the rapid development of cloud computing, big data, and the Internet of Things, traditional network architectures become inadequate to manage the demanding and imposed traffic load. To address those challenges, Software-defined networking (SDN) offers a new network architecture by extracting the control logic from the network devices to a logically centralized controller (see Figure \ref{fig: sdn}). By standardizing the communication protocol between the SDN controllers and the network devices (e.g., OpenFlow), users/applications can directly program the network with a global view. Furthermore, to enable network operators to write modular and reusable applications, numerous research projects focus on providing high-level abstractions for the applications to specify the core network management logic  \cite{pyretic}. 
\begin{figure}[!htb]
\centering
\begin{subfigure}{.45\textwidth}
	\includegraphics[height=0.18\textheight]{sdn}
	\caption{SDN High-Level Architecture.} 
	\label{fig: sdn}
\end{subfigure}
\begin{subfigure}{.45\textwidth}
	\centering
	\includegraphics[height=0.18\textheight]{architecture}
	\caption{PSMA System Design.}
	\label{fig: arch}
\end{subfigure}  
\caption{System Architecture of SDN and PSMA.}
\label{fig: figure}
\end{figure}      

However, to the best of our knowledge, most existing works do not support automatic composition of network policies.\comment{For example, \cite{pyretic} requires user to explicitly specify how to compose two policy modules using sequential and parallel operators. } Network management may involve multiple domains, each of which has their own control logic to apply to the same underlying physical devices \cite{PGA}. These domains may be unaware of each others' behaviors, thus the policy composition requires manually analyzing the internal logic to reconstruct the global policy. \comment{In their work, network policy are specified using graphs, where the nodes represent end-points, and the path from one end-point to another represents the communication is enabled between them. Along the path there may be a sequence of network boxes, indicating the traffic should pass through a set of waypoints, such as load balancer, firewall, etc. Composing two network policies involves taking the union of the two graphs and determining the order of network boxes on the paths.}
\comment{The authors of \cite{PGA} provide an example of automating the policy composition. However, their graph representation is not general enough. In particular, the specification of middlebox functionality should be a part of network policy itself, rather than a black box to send traffic to.}
To address this problem, we propose a new framework called PSMA (Policy State Machine Abstraction) for individual applications to specify their network policies, which are abstracted as state machines and then composed into a single global state machine. Our framework also consists of a role-based model to constrain how each application can specify the policy and to provide rules to reconcile conflicts that arise during the composition. 
\vspace{-0.1cm}
\section{STATE MACHINE ABSTRACTION}
\vspace{-0.1cm}
%\end{center}
A network policy specifies how a network flow is handled by the network. %Typically, the action to take on a network flow depends on its properties.
For example, a forwarding policy may look like ``if the flow is originated from host A, then forward it to host B"; Similarly, a security policy may be expressed as ``if the flow passes through an infected node, then drop it". Based on this observation, we model a network policy as a state transition process, where each network flow entering the network has an initial state, which is a set of state variables, and gets transferred to a different state upon leaving the network. The two example policies therefore are expressed as $<location = A> \rightarrow <location = B>$ and $<data = malicious> \rightarrow <location = none_{dropped}>$. The composition of the two state machines is processed as: (1) construct new states by applying the cross product on the variables; and (2) impose the transitions of the original states to the new states with the overlapping variables. The resulting state machine of composing the two example policies would be $<location = A, data = malicious> \rightarrow <location = B, data = malicious> \rightarrow <location = none_{dropped}>$

In order to detect conflicts among policies, we define the semantics of a state machine expression $<v = a> \rightarrow <v' = b>$ as ``if a flow satisfies $v=a$ when it enters the network, it will satisfy $v'=b$ when leaving the network". After composition, various flow inputs are tested on the global state machine against all policies to check whether all the statements still hold.
\vspace{-0.1cm}
\section{SYSTEM OVERVIEW}
\vspace{-0.1cm}
Figure \ref{fig: arch} depicts the system design of PSMA. PSMA resides between the application layer and the SDN control plane. Each application is assigned a role, which is associated with a set of state variables that the application is allowed to use; then the application specifies the policy using the state machine library and sends the state machine to the composition system. The composition system then tries to produce a global state machine from all the individual state machines without violating any application's policy. If such composition fails, the system will feedback to certain applications based on their roles, and recommend those applications to modify and resend their state machines. Finally, the composed global state machine is sent to the SDN controller, in which the high-level specification will be transformed to the low-level rules and then be installed on the network devices.
\vspace{-0.1cm}
\section{CHALLENGES AND FUTURE WORK}
\vspace{-0.1cm}
We will address several challenges to make PSMA work in practice, including (1) how to reduce the state explosion problem in PSMA, which occurs in many large-scale and complex state machine based systems; and (2) how to precisely and quickly transform the state machine into the low-level instructions (i.e., flow rules).

% Please don't exchange the bibliographystyle style
\bibliographystyle{wsc}
% AUTHOR: Include your bib file here
\bibliography{demobib}

\end{document}

