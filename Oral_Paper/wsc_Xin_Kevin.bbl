\begin{thebibliography}{}

\bibitem[\protect\citeauthoryear{Agarwal, Kodialam, and Lakshman}{Agarwal
  et~al.}{2013}]{TrafficEngineering}
Agarwal, S., M.~Kodialam, and T.~V. Lakshman. 2013, April.
\newblock ``Traffic Engineering in Software Defined Networks''.
\newblock In {\em INFOCOM, 2013 Proceedings IEEE},  2211--2219.

\bibitem[\protect\citeauthoryear{Cadar, Dunbar, and Engler}{Cadar
  et~al.}{2008}]{cadar2008klee}
Cadar, C., D.~Dunbar, and D.~Engler. 2008.
\newblock ``KLEE: Unassisted and Automatic Generation of High-Coverage Tests
  for Complex Systems Programs.''.
\newblock In {\em Proceedings of the 8th USENIX Symposium on Operating Systems
  Design and Implementation (OSDI)}, Volume~8,  209--224.

\bibitem[\protect\citeauthoryear{Darema}{Darema}{2004}]{darema2004dynamic}
Darema, F. 2004.
\newblock ``Dynamic Data Driven Applications Systems: A New Paradigm for
  Application Simulations and Measurements''.
\newblock In {\em Computational Science-ICCS},  662--669. Springer.

\bibitem[\protect\citeauthoryear{Jin and Nicol}{Jin and
  Nicol}{2010}]{fast-background-traffic}
Jin, D., and D.~Nicol. 2010.
\newblock ``Fast simulation of background traffic through Fair Queueing
  networks''.
\newblock In {\em Proceedings of the 2010 Winter Simulation Conference},
  2935--2946.
\newblock Piscataway, New Jersey: Institute of Electrical and Electronics
  Engineers, Inc.

\bibitem[\protect\citeauthoryear{Jin, Liu, Gandhi, Kandula, Mahajan, Zhang,
  Rexford, and Wattenhofer}{Jin et~al.}{2014}]{Dionysus}
Jin, X., H.~H. Liu, R.~Gandhi, S.~Kandula, R.~Mahajan, M.~Zhang, J.~Rexford,
  and R.~Wattenhofer. 2014.
\newblock ``Dynamic Scheduling of Network Updates''.
\newblock In {\em Proceedings of the 2014 ACM Conference on SIGCOMM},
  539--550.

\bibitem[\protect\citeauthoryear{Kazemian, Chang, Zeng, Varghese, McKeown, and
  Whyte}{Kazemian et~al.}{2013}]{NetPlumber2013}
Kazemian, P., M.~Chang, H.~Zeng, G.~Varghese, N.~McKeown, and S.~Whyte. 2013.
\newblock ``Real Time Network Policy Checking Using Header Space Analysis''.
\newblock In {\em Proceedings of the 10th USENIX Symposium on Networked Systems
  Design and Implementation (NSDI)},  99--112.
\newblock Berkeley, CA, USA.

\bibitem[\protect\citeauthoryear{Kazemian, Varghese, and McKeown}{Kazemian
  et~al.}{2012}]{PHA2012}
Kazemian, P., G.~Varghese, and N.~McKeown. 2012.
\newblock ``Header Space Analysis: Static Checking for Networks''.
\newblock In {\em Proceedings of the 9th USENIX Symposium on Networked Systems
  Design and Implementation (NSDI)},  113--126.
\newblock San Jose, CA.

\bibitem[\protect\citeauthoryear{Khurshid, Zou, Zhou, Caesar, and
  Godfrey}{Khurshid et~al.}{2013}]{VeriFlow}
Khurshid, A., X.~Zou, W.~Zhou, M.~Caesar, and P.~B. Godfrey. 2013.
\newblock ``VeriFlow: Verifying Network-Wide Invariants in Real Time''.
\newblock In {\em Proceedings of the 10th USENIX Symposium on Networked Systems
  Design and Implementation (NSDI)},  15--27.
\newblock Lombard, IL.

\bibitem[\protect\citeauthoryear{Liu, Wu, Zhang, Yuan, Wattenhofer, and
  Maltz}{Liu et~al.}{2013}]{zUpdate}
Liu, H.~H., X.~Wu, M.~Zhang, L.~Yuan, R.~Wattenhofer, and D.~Maltz. 2013.
\newblock ``zUpdate: Updating Data Center Networks with Zero Loss''.
\newblock In {\em Proceedings of the 2013 ACM Conference on SIGCOMM},
  411--422.

\bibitem[\protect\citeauthoryear{Mai, Khurshid, Agarwal, Caesar, Godfrey, and
  King}{Mai et~al.}{2011}]{Anteater2011}
Mai, H., A.~Khurshid, R.~Agarwal, M.~Caesar, P.~B. Godfrey, and S.~T. King.
  2011.
\newblock ``Debugging the Data Plane with Anteater''.
\newblock In {\em Proceedings of the 2011 ACM Conference on SIGCOMM},
  290--301.

\bibitem[\protect\citeauthoryear{Nicol}{David Nicol}{2009}]{campus}
David Nicol 2009.
\newblock ``Standard Baseline DARPA NMS Challenge Topology''.
\newblock \url{http://www.ssfnet.org/ Exchange/gallery/baseline/index.html}.

\bibitem[\protect\citeauthoryear{Nicol and Yan}{Nicol and
  Yan}{2006}]{Nicol_simulation}
Nicol, D.~M., and G.~Yan. 2006, January.
\newblock ``cc''.
\newblock {\em Simulation\/}~82 (1): 21--42.


\bibitem[\protect\citeauthoryear{ONF}{ONF}{2014}]{openflow}
ONF Accessed 2014.
\newblock ``{OpenFlow Specification}''.
\newblock
  \url{https://www.opennetworking.org/images/stories/downloads/sdn-resources/onf-specifications/openflow/openflow-switch-v1.5.0.noipr.pdf}.

\bibitem[\protect\citeauthoryear{ONF}{ONF}{2016}]{onf}
ONF Accessed 2016.
\newblock ``{Open Networking Foundation}''.
\newblock \url{https://www.opennetworking.org}.

\bibitem[\protect\citeauthoryear{Zhou, Jin, Croft, Caesar, and Godfrey}{Zhou
  et~al.}{2015}]{CCG}
Zhou, W., D.~Jin, J.~Croft, M.~Caesar, and P.~B. Godfrey. 2015.
\newblock ``Enforcing Customizable Consistency Properties in Software-Defined
  Networks''.
\newblock In {\em Proceedings of 12th USENIX Symposium on Networked Systems
  Design and Implementation (NSDI)},  73--85.

\end{thebibliography}
