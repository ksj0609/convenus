\section{EVALUATION}
%In this section, we introduce the experimental evaluation of our verification tool. Our experiments are based on simulation, where a network called "campus network topology" is studied with a simulated controller issuing flow updates. Our verification tool seated between them to verify if each update will cause congestion in the network. Our experiments mainly focus on the performance of our tool (i.e., how fast we are able to verify the flow updates). Thus, in this experiment, each time we find out that current update will cause congestion, we simply reject it without any feedback information.

%\subsection{Campus Network Topology}

\subsection{Experiment Setup}

To evaluate the performance of \name, we design network scenarios based on the \emph{campus network} model, which is a key baseline network model originally designed for benchmarking parallel network simulation \xin{\cite{campus}}.\comment{More details about the campus network are described in \cite{campus}.} The entire topology is an abstraction of a ring of simplified campus networks as shown in the left portion of Figure \ref{fig: campus}. Each simplified campus network consists of a ring of access switches, each of which has a number of hosts directly connected to it, as shown in the right portion of Figure \ref{fig: campus}.\comment{Every simplified campus network has a gateway to connect to the main ring.} Communication across different campus networks must pass through the ring of the exchange switches \xin{connected by their own gateways}.

\begin{figure}[htb]
{
\centering
\includegraphics[height=0.25\textheight]{campus_network}
\caption{An example of campus network topology. \label{fig: campus}}
}
\end{figure}

In this work, we constructed a network topology consisting of a ring of eight campus networks, which requires eight gateway and eight exchange switches. Within a campus network, every access switch connects to eight hosts. Each switch is modeled as one port in \name. Therefore, the network has 80 ports and 512 hosts in total. We set the link bandwidth to be 10 Mbps between the hosts and the access switches, 100 Mbps between the access switches themselves as well as between the access switches and the gateways, and 1 Gbps between the gateways and exchanges switches as well as between the exchange switches themselves. To generate a flow insertion update, the SDN controller randomly selected two different hosts in the network, one as the source and the other as the destination of the flow. The flow update contains state variables including a unique identifier, an ingress rate of 10 Mbps, and a shortest path between the two hosts. To generate a flow deletion, the controller randomly selected an existing flow in the network using the flow identifier. While the random flow selection is a reasonable assumption, we plan to deploy \name on a physical SDN network in order to \xin{perform high fidelity evaluation}\comment{evaluate the system with more realistic network update scenarios}. 
%\textcolor{red}{Since the behavior of the controller is complicated due to the interleaving flow rule updates issued from different applications running on it, random selection of the hosts and paths is the best option we currently have. In our future work, we will further implement our prototype system to deploy on a real SDN network, and observe the characteristics of the updates under different circumstances} 
Each experiment consisted of two stages. During stage 1, initially there was no flow in the network, and we issued random flow insertion updates (one update at a time) until 250 flows were successfully inserted in the network. We configured \name not to apply the flow update to the network if the update did not pass the congestion verification. During stage 2, with 250 flows in the network, we randomly generated 400 flow updates (50\% are flow insertions and 50\% are flow deletions), and passed them to \name. We repeated 10 times for each set of experiments, and the results are discussed in the next section. 


\subsection{Experimental Results and Analysis}
We first count the number of times that ConVenus reported congestion \xin{during the verification.}\comment{by verifying the updates.} Among those updates causing congestion, we also count the number of times that circular dependence occurred. The results are recorded in Table \ref{tab: num_congestion}. We observe that in stage 1, around 174 flow insertions that would lead to network congestion were detected before we successfully inserted 250 congestion-free flows into the network. Among those congestion cases, more than one-third of them resulted in circular dependence, which required further processing using Phase-II through Phase-IV of our simulation algorithm described in Section 4.3.  In stage 2, there were much less congestion cases (41 on average) and circular dependence cases (3.5 on average). This is because the flow deletion updates did not cause any ports in the network to be congested as described in Section 5.1. We further plot the number of congested flows and ports for every flow updates in one trial for stage 1 and stage 2 in Figure \ref{fig:insertion_congestion} and \ref{fig:update_congestion}. In worst-case scenario, 4 ports are congested in total. The number is small because congestion can only occur on those ports along the path of the newly inserted flow.

\begin{table}[htb]
\centering
\caption{Congestion verification: \# of flow updates resulting in network congestion and circular dependence. $N_{c}$ is the \# of network congestion occurrence and $N_{d}$ is the \# of circular dependence occurrence.\label{tab: num_congestion}}
\small
\begin{tabular}{rllll}
\hline
 & \multicolumn{2}{c}{Stage 1} & \multicolumn{2}{c}{Stage 2} \\
 & $N_{c}$ & $N_{d}$ & $N_{c}$ & $N_{d}$\\ \hline
Average & 174.8 & 67.5 & 41.0 & 3.5\\
Standard Deviation & 52.8 & 23.8 & 7.9 & 2.2\\
\hline
\end{tabular}
\end{table}

%In stage 1, the first 200 insertions caused no congestion in the network, since there are zero flows and ports congested. However, the second half of stage 1 has frequent congestion found. On the contrary, in stage 2 shown in Figure \ref{fig:update_time}, the congested cases appear more frequently at the beginning and then fewer times in the second half, as the network becomes stabilized. 

%As we look at it in more details, it is shown that the majority times, the number of congested flows is fixed at two specific values: one around 30 and another around 55. This is because the bandwidth of an access switch is set to be 280 Mb/s, and each flow has ingress rate to be 10 Mb/s. Thus, to keep the network congestion-free, an access switch can have at most 28 flows passes through it; when the switches are nearly saturated, once the controller issue another flow insertion which it is going to pass these switches, there are at least 29 flows congested, including the new flow. To compare with number of congested ports, it is obvious that the number increases if there is more flows congested. But even in worst-case scenario, there are 4 ports congested in total. It is not a surprise since we have proved in the previous section that the only ports could be congested are the ones on the new flow's path.  

%\vspace{-4mm}
\begin{figure}[h]
\centering
\begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{congested_flows}
  \caption{Stage 1: Flow insertion updates.}
  \label{fig:insertion_congestion}
\end{subfigure}%
\begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{congested_flows_2}
  \caption{Stage 2: Flow insertion and deletion updates.}
  \label{fig:update_congestion}
\end{subfigure}%
\caption{Number of congested flows and ports for each flow update.}
\label{fig:congested_flows}
\end{figure}

We next evaluate the verification speed of \name. We record the execution time to perform congestion verification of each flow update for all the 10 experiments. We also record the total time and break down into the time for (1) generating the minimum affected network graph and (2) executing the simulation algorithm for flow computation and congestion verification. Figure  \ref{fig:insertion_time} and Figure \ref{fig:update_time} plot the cumulative distribution functions (CDFs) of the verification time in stage 1 and stage 2.

\begin{figure}[h]
\centering
\begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{time_stats_CDF}
  \caption{Stage 1: Flow insertion updates.}
  \label{fig:insertion_time}
\end{subfigure}
\begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{time_stats_CDF_2}
  \caption{Stage 2: Flow insertion and deletion updates.}
  \label{fig:update_time}
\end{subfigure}
\caption{Cumulative distribution function of the update verification time, which consists of: (1) time to generate the graph to model the minimum affected network and (2) time to run the 4-phase simulation algorithm.}
\label{fig:verification_time_CDF}
\end{figure}

The verification speed is high. Around 80\% of the verification takes less than 5 ms in stage 1, and around 90\% of the verification takes less than 5 ms in stage 2. We did not observe the long-tail behavior in the CDFs, and the verification time is bounded by 10 ms (the maximum time is 9.3 ms in stage 1 and 8.5 ms in stage 2). The evaluation results indicate that \name is a suitable online verification tool for many network scenarios within such a delay bound.
We also observe that 95.6\% of the minimum affected network graph generation time is less than 2 ms in both stages, and most time was spent on executing the simulation algorithm. Therefore, we further break down the time spent in each of the four phases in the simulation. We observe that in both stages, the phase-I (i.e., flow rate computation) takes the majority of time (92.7\% in stage 1 and 96.9\% in stage 2). It is because (1) if no congestion is detected, the simulation stops at phase-I, (2) even if congestion occurs, but no circular dependence is generated, simulation does not have to run through phase II to IV; and (3) even if a circular dependence is generated, the graph size is bounded by the number of congested ports, which is small as shown in Figure \ref{fig:congested_flows}.

\if 0
\begin{figure}[h]
\centering
\begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{breakdown_CDF}
  \caption{Stage 1: Flow insertion updates.}
  \label{fig:insertion_breakdown}
\end{subfigurted_flows}
\begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{breakdown_CDF_2}
  \caption{Stage 2: Flow insertion and deletion updates.}
  \label{fig:update_breakdown}
\end{subfigure}
\caption{Cumulative distribution function of simulation time, which consists of phase-I: flow rate computation; phase-II: dependency graph identification; phase-III: fixed-point iteration and phase-IV: residue flow rate computation.}
\label{fig:breakdown_CDF}
\end{figure}
\fi
