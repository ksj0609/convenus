%\section{Network Flow Simulation and Modeling for Congestion Verification}
\section{NETWORK FLOW SIMULATION AND MODELING FOR CONGESTION VERIFICATION}

This section presents the network flow model in \name and the simulation algorithm for fast flow rate computation of the entire network upon receiving a controller update. The estimated flow rates are then compared with the desired bandwidth requirements to determine whether congestion would occur if the update is applied to the network. We describe the modeling assumptions, the problem formulation, and the four-phase simulation algorithm for fast flow rate computation.

%\subsection{MODELING ASSUMPTIONS }
\subsection{Modeling Assumptions}
\paragraphb{Scheduling Policy.} When multiple flows are aggregated at a switch port, the scheduling policy determines \xin{the bandwidth allocation to}\comment{how to allocate the bandwidth to} each flow. In theory, all the existing scheduling policy in traditional switches can be realized in SDN switches too. In practice, First-Come-First-Serve (FCFS) scheduling policy is commonly used\comment{in SDN switches}. In \name, we assume that multiple flows aggregated in a port are scheduled according \xin{to} the FCFS scheduling policy. Our model is designed to be easily extended to other scheduling policies by changing the bandwidth allocation rules. For instance, the rules for Fair Queuing scheduling policy were investigated in a prior work \cite{fast-background-traffic}. 

\paragraphb{Buffering Strategy.} In \name, we assume every switch adopts the output buffering strategy, although other buffering strategies can be easily incorporated into our algorithms described in Section \ref{fourstep}. According to the OpenFlow specification \cite{openflow}, each output port is assigned with one or more output queues. The action of forwarding a packet is required to specify which port to send the packet to, but it is optional to specify the specific queue. 
 
\paragraphb{Ingress Flow Rate.} In \name, we assume the ingress rates of all the existing flows in the network are known. The rates can be derived from the application specification and/or be estimated from the flow statistics stored in the SDN switches. How to obtain the precise flow ingress rates is not a focus of this paper, and we leave the development of a flow rate monitor (e.g., continuous interception of OpenFlow statistic messages from switches to controller) to compute ingress flow rates as our future work.

%\subsection{PROBLEM FORMULATION AND NOTATIONS}
\subsection{Problem Formulation and Notations}
All the notations used in remainder of this paper are summarized in Table \ref{tab: first}.

\begin{table}[htb]
\centering
\small
\caption{Notations. \label{tab: first}}
\begin{tabular}{rlrl}
\hline
Symbol & Explanation & Symbol & Explanation\\ \hline
$Q$ & Set of all ports in the network & $\mu_{q}$ & Bandwidth of port $q$ \\
$Q_{ingress}$ & Set of ingress ports in the network & $\lambda^{in}_{f,q}$ & Input rate of flow $f$ at port $q$ \\
$Q_{egress}$ & Set of egress ports in the network & $\lambda^{out}_{f,q}$ & Output rate of flow $f$ at port $q$ \\
$Q_{f}$ & Ordered Set of ports in $f$'s path & $\Lambda^{in}_{q}$ & Aggregated input rate at port $q$ \\
$A_{f}$ & Affected flow set in respect to flow $f$ & $\Lambda^{out}_{q}$ & Aggregated output rate at port $q$ \\ 
$N_{f}$ & Minimum affected network in respect to flow $f$ & $R_{f}$ & Ordered Set of the rates of $f$ at $q \in Q_{f}$  \\
$\lambda_{f,q}$ & \multicolumn{3}{l}{Flow rate of $f$ at port $q$, including both input and output rate} \\
$S(\lambda_{f,q})$ & \multicolumn{3}{l}{State of a flow $f$ at a port $q$; $\{settled, bounded, unsettled\}$}\\
\end{tabular}
\end{table} 

The data plane is modeled as a collection of switches connected by unidirectional links. The sending endpoint of a link is attached to a switch's output port. There is an output buffer associated with each output port. Essentially, we can model a network $N$ as a set of output ports $Q$ and a set of flows $F$. Each output port $q\in Q$ resides either on an end-host or a switch. Each flow $f \in F$ is represented as a tuple $<Q_{f}$, $R_{f}>$, where $Q_{f}$ is an ordered set of ports $(q_{1}, q_{2},..., q_{|Q_{f}|})$, which is the path that the flow passes through, i.e., $q_{1}$ is the output port of the first switch in the flow that connects to the source, $q_{|Q_{f}|}$ is the output port of the last switch in the flow that connects to the sink, and the remaining ports are on the intermediate switches along the communication path. Note that any adjacent ports $(q_{i}, q_{j})$ in the sequence must be connected. $R_{f}$ is an ordered set of input and output rates of $f$ passing through the same sequence of ports, $q \in Q_{f}$.

We denote the bandwidth of an output port $q$ as $\mu_{q}$, and the input and output rate of a flow $f$ that passes through $q$ as $\lambda^{in}_{f,q}$ and $\lambda^{out}_{f,q}$. $F_{q}$ denotes the set of flows passing through {q}. The aggregated input rate at port $q$ is denoted by $\Lambda^{in}_q$, which equals to the summation of the input rates of all the flows that pass through $q$, i.e., $\Lambda^{in}_q = \sum_{f \in F_{q}} \lambda^{in}_{f,q}$. We have the following two definitions of congestion.

\begin{definition}
A port $q$ is congested if and only if $\mu_{q} < \Lambda^{in}_q$.
\end{definition}
\begin{definition}\label{def:congestion}
A network is congested if and only if at least one port is congested.
\end{definition}
 
Given the bandwidth of every port and the ingress rate of every flow, our first objective is to determine whether the network has congestion. Our second objective is to discover, for each flow, the input rate and the output rate at each port along its path (i.e., $\lambda^{in}_{f,q}$ and $\lambda^{out}_{f,q}$ for every $f$ and $q$). Note that the output rate of a flow leaving the current port is equal to the input rate of the flow entering the next port, and the egress rate of the flow is equal to the flow output rate leaving the last switch along the path.


%\subsection{FOUR-PHASE SIMULATION ALGORITHM} 
\subsection{Simulation Algorithm of Flow Rate Computation} \label{fourstep}
A key component of \name is the module for quickly computing the flow rate changes of all the affected flows due to the new network update from the controller. The results are used to  identify (1) whether the new update will cause network congestion, (2) the set of switch ports that the congestion occur, and (3) the input and output rate of the congested flows along the communication paths. We developed a four-phase simulation algorithm to achieve fast flow rate computation in \name as motivated by several prior works \cite{Nicol_simulation}, \cite{fast-background-traffic}. The four phases include: (1) flow rate update, (2) reduced dependency graph generation, (3) flow rate computation using fixed-point iteration, and (4) residue flow rate computation, as shown in Figure \ref{fig: flow chart}. To illustrate the algorithm, we first present the basic rules for the flow rate computation with FCFS scheduling policy, and then the circular dependence among the affected flows, and finally the step-by-step description of each phase in the simulation algorithm.

%\subsubsection{FLOW RATE COMPUTATION UNDER FCFS POLICY ASSUMPTION}
\subsubsection{Flow Rate Computation under the FCFS Scheduling Policy}

We define the following rules for calculating the output rates of all the flows aggregated at a particular switch port, given the corresponding input rates and the bandwidth information.
\setlength{\abovedisplayskip}{1pt}
\setlength{\belowdisplayskip}{1pt}
\begin{equation} \label{eq:FCFS}
\lambda^{out}_{f,q} = 
\begin{cases}
	\lambda^{in}_{f,q}, & \text{if}\ \Lambda^{in}_{q} \leq \mu_{q} \\
	\lambda^{in}_{f,q} \times \frac{\mu_{q}}{\Lambda^{in}_{q}}, & \text{otherwise}
\end{cases}
\end{equation}

If the aggregated rate is less than or equal to the port's bandwidth, every flow's output rate is the same as the input rate; If the aggregated rate is greater than the bandwidth (i.e., the port is congested), the flow output rate is proportional to its arrival rate under the FCFS scheduling policy. 

%\subsubsection{CIRCULAR DEPENDENCY AMONG FLOWS}
\subsubsection{Circular Dependence Among Affected Flows}
The objective of the simulation algorithm is to find the input and output rates of all flows at all the ports along the path. The basic idea is to propagate and update the flow rate values along the path (i.e., a sequence of ports) for every flow based on the Equation \ref{eq:FCFS}. We are necessarily left with circular dependences among some flow variables. Let us illustrate the circular dependence with a simple example shown in Figure \ref{fig: dependency}. Assume both $q_1$ and $q_2$ are congested, we have the following equations for flow $f_1$ and $f_2$:
\begin{equation} \label{eq:circular}
\begin{aligned}
\lambda^{out}_{f1,q1} = \lambda^{in}_{f1,q1} \times \frac{\lambda^{in}_{f1,q1}}{\lambda^{in}_{f1,q1}+\lambda^{in}_{f2,q1}} \\
\lambda^{out}_{f2,q2} = \lambda^{in}_{f2,q2} \times \frac{\lambda^{in}_{f2,q2}}{\lambda^{in}_{f1,q2}+\lambda^{in}_{f2,q2}}
\end{aligned}
\end{equation}
Since $\lambda^{in}_{f2,q1} = \lambda^{out}_{f2,q2}$ and $\lambda^{in}_{f1,q2} = \lambda^{out}_{f1,q1}$, we find that $\lambda^{out}_{f1,q1}$ and $\lambda^{out}_{f2,q2}$ essentially depend on each other. Such circular dependence relationship can be extended to multiple flows with multiple ports involved. We address this issue by identifying all the flow variables that are involved in each circular dependence, constructing a dependency graph, and applying fixed-point iteration to solve the equations to derive the output flow rates, and the details are presented in the next section.

\begin{figure}[htb]
{
\centering
\includegraphics[width=0.4\textwidth]{dependency_graph}
\caption{An example of circular dependence between two flows. \label{fig: dependency}}
}
\end{figure}

\subsubsection{Simulation Algorithm for Network Flow Rate Computation}

Given a set of flow input rates, the objective of the algorithm is to efficiently compute the output flow rates at the destination hosts as well as the traffic loads at all the intermediate switches along the flow paths, in order to detect the network congestion. Each flow is in one of the three state: settled, bounded or unsettled. A settled flow has a finalized flow rate; a bounded flow has a known upper bound on its flow rate; an unsettled flow is neither bounded or settled. Figure \ref{fig: flow chart} illustrates the procedures of the flow rate computation algorithm, which consists of four phases.

\begin{itemize}
\item Phase-I propagates the flow rate and state from ingress points throughout the network. The goal is to settle flows and resolve as many ports as possible. We calculate the flow rate and state of all the output flows of a port based on Equation \ref{eq:FCFS} under the FCFS scheduling policy, and then pass the rate and state of the output flow to the next switch's input along the flow path. In the case of no circular dependence among the involved ports and flows, all the flow rates are settled, and the output results are used to check and determine the flow-level congestion in the network. In the case of circular dependencies among some flow variables, Phase-I will assign an upper bound of the rate to those flows. If the input flow is settled, the upper bound is derived by ignoring all bounded input flows. If the input flow is bounded, the upper bound is derived by ignoring all other bounded input flows and treat it as settled with the rate set to the bounded rate. The remaining three phases mainly focus on addressing the circular dependencies to compute the flow rates. 

\item Phase-II identifies all the flow variables whose values are circularly dependent, and constructs one or more (directed) dependency graphs. The vertex set is composed of all output ports containing unsettled and bounded flows. The directed edges are the flows involved in the circular dependence.

\item Phase-III formulates a set of non-linear equations for the flows in each dependency graph, as illustrated in Equation \ref{eq:circular}. We use the fixed-point iteration method to solve those equations. Note that the initial values are the bounded values calculated in Phase-I. 

\item Phase-IV substitutes the solutions into the system and continues to compute and update the rates of the remaining unsettled flows that are affected by those flows in the circular dependency graph(s). 
\end{itemize}


\begin{figure}[htb]
\centering
\includegraphics[height=0.2\textheight]{flow_chart}
\caption{Flow Chart of the Four-phase Simulation Algorithm for Network Flow Rate Computation.
%Note that $\lambda^{in}_{f,q}$ is the input rate of flow $f$ through port $q$, and $\lambda^{out}_{f,q}$ is the output rate of flow $f$ through port $q$.
}
\label{fig: flow chart}
\end{figure}
 
