#ifndef FLOW_H_
#define FLOW_H_

#include <sys/types.h>
#include <unistd.h>
#include <stdint.h>
#include <string>
#include <unordered_map>
#include <vector>
#include "ForwardingDevice.h"

using namespace std;

enum FlowType {settled, unsettled, bounded};

struct ForwardingDevice;

/**A segment of a flow.  
* A segment is between two concecutive hops (switches), 
* which is along the path of a Flow.
*/
struct FlowSegment{
public:
	uint64_t myFlowID;	/**< The ID of the flow that this segment belongs to*/ 
	uint64_t segmentID;	/**< The ID of this segment*/

	FlowType type;		/**< The type of the FlowSegment, can be one of the thee types: unsettled, bounded, and settled*/
	string sourceIPAddress;  /**< The source end of the flowSegment*/
	string destIPAddress;	 /**< The destination end of the flowSegment*/
	double in_rate;			 /**< The incoming rate of the segment, from the source end*/

	void printMyself()		 /**< Print the information of this FlowSegment object*/
	{
		fprintf(stdout, "flow ID = %ld, segmentID = %ld, source = %s, dest = %s, in_rate = %lf\n", 
					this->myFlowID, this->segmentID, this->sourceIPAddress.c_str(), this->destIPAddress.c_str(), this->in_rate);
	}
};

/**Model of a flow in the network. A flow is consist of an ingress rate,  
*  an egress rate, and devided into a path of a flow segments,
*  each one is between to consecutive hops (switches).
*/
struct Flow
{
public:
	uint64_t flowID;				/**< ID of the flow*/
	double ingressRate;				/**< The incoming rate from an end-host into the network*/
	double egressRate;				/**< The outgoing rate from the network to an end-host*/
	vector<FlowSegment *> flowPath; /**< A sequence of segments that devide the flow along its path*/

	void setEgress(double e){
		this->egressRate = e;
	}

	bool operator==(const Flow &o) const {
		return flowID == o.flowID;
	}

	bool operator<(const Flow &o) const {
		return flowID < o.flowID;
	}

	bool operator!=(const Flow &o) const{
		return flowID != o.flowID;
	}

	void printMyself(){
		fprintf(stdout, "Flow_id: %lu ingressRate %f egressRate %f \n", this->flowID, this->ingressRate, this->egressRate);
		vector< FlowSegment *>::const_iterator it;
		for(it = this->flowPath.begin(); it != this->flowPath.end(); it++){
			(*it)->printMyself();
		}
	}

};

namespace std{

  template <>
	/** Define the hash function of the Flow class, by calculating the 
	*	key based on operation on the flow ID and the flow's ingress rate
	*/
  struct hash<Flow>
  {
    std::size_t operator()(const Flow& f) const
    {
      using std::size_t;
      using std::hash;
      using std::string;

      // Compute individual hash values for first,
      // second and third and combine them using XOR
      // and bit shifting:

      return ( (hash<uint64_t>()(f.flowID) ^ (hash<double>()(f.ingressRate) << 1) ) >> 1);
    }
  };
}

#endif /* FLOW_H_ */
