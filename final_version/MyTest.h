
#ifndef MYTEST_H_
#define MYTEST_H_

#include <sys/types.h>
#include <unistd.h>

/** Setup the experiment, including generate the 
*	campus network topology to run the experiment.
*
*/
class MyTest
{
public:
	static void myTest();
};

#endif /* TEST_H_ */
