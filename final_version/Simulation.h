#ifndef SIMULATION_H_
#define SIMULATION_H_

#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <map>
#include <unordered_map>
#include <set>
#include <list>
#include "Network.h"
#include "PortVolumnState.h"

using namespace std;

/*
struct PortTuple{
	string portIP;
	PortVolumnState *pvs;

	PortTuple(string s, PortVolumnState *p){
		this->portIP = s;
		this->pvs = p;
	}
};
*/

/** A structure to represent a graph node in the 
*   dependency graph mentioned in the Simulation class.
*	Each node represent a port in the network, and 
*	there is an edge (u,v) if there is at least one
*	critical segment from port u to port v
*/
struct DependenceGraphNode{

	string myNode;					/**< The id of the port*/
	list< string > directedNodes;	/**< The list of neighbors that this port directs to*/

	DependenceGraphNode(string ip)  /**< Constructor*/
	{
		myNode = ip;
	}
};

/** A 4-phase Simulation Algorithm to calculate the  
*	ingress and egress rates of all the flows and 
*	the intermediate throughput at each switch in the
*   network.
*
*	Given the network topology, bandwidth information,  
*	and each flow's ingress rate as input, the 4-phase
*	algorithm (1) propagate the flow rate based on flow
*	calculation rules (2) identify circular dependencies 
*	which forms a graph cycle (3) use fix-point iteration
*	to calculate the flow rate of circular flows and (4) 
*	calculate the residue flows' rate.
*/


class Simulation
{
private:

	//int counterExample = 0;		
	int numCongestedPorts = 0;	/**< The number of congested ports during one simulation*/

	//list<PortTuple> portList;
	unordered_map<string, PortVolumnState *> portMap;  		/**< A HashMap that maps a port's name to its state, which is represented by a PortVolumnState structure*/
	unordered_set< DependenceGraphNode *> dependenceGraph;  /**< A graph to identify the circular dependency among flows*/
	set< FlowSegment *> criticalSegments;				    /**< A set of segments that causing the circluar dependency*/
	map< FlowSegment *, FlowSegment *> causalRelation;		/**< A tuple that record the causal relation between segments*/

public:

	int getCongestedPorts(){ return this->numCongestedPorts; }  /**< Getter function*/

	void setup(Network *network, Flow flow);					/**< Given the network and flow information, initialize the states of the ports and flows before starting the simulation*/
	bool ruleBasedUpdate(Network *network);						/**< Propagate the flow rate along the paths, according to calculation rules. Return true if all ports are resolved; otherwise return false*/ 
	int generateDependenceGraph(Network *network);				/**< Generate dependency graph, return the number of nodes in this graph*/
	void fixedPointIteration(Network *network);					/**< Fixed-point iteration to calculate the flow rate in the dependency graph*/
	int calculateEgress(Network *network, Flow flow);			/**< Calculate the egress rates of all the flows*/

	bool valuesFixed(map< FlowSegment *, double> previousIteration);	/**< Check whether the current iteration reaches a fixed point*/
	//void clearup(Network* network, Flow flow, bool addFlow, bool congestion);

	void printAllPort();									/**< Print the information of all the ports in the network*/
	void printDependenceGraph(Network *network, int size);	/**< Print the information of the dependency graph*/
	//void printPortList();									
};

#endif /* SIMULATION_H_ */