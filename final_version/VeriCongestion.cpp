#include <sys/types.h>
#include <unistd.h>
#include <cstdio>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <queue>
#include <algorithm>
#include "Simulation.h"
#include "Network.h"
#include "ForwardingDevice.h"
#include "PortVolumnState.h"
#include "VeriCongestion.h"
#include "Flow.h"

using namespace std;

static FILE *timeStatsFile = NULL;
static FILE *numCongestionFile = NULL; 
static FILE *congestedFlowsFile = NULL;
static FILE *numDependencyGraphFile = NULL;
static FILE *graphSizeFile = NULL;

static FILE *simulationBreakdownFile = NULL;

VeriCongestion::VeriCongestion(Network& network){
	this->wholeNetwork = network;
}


void VeriCongestion::addNewFlow(Flow flow){

/*
	fprintf(stdout, "This time i'm going to add this flow:\n");
	flow.printMyself();
	fprintf(stdout, "--------------------------------------------------\n");
*/
	this->wholeNetwork.addFlow(flow);

	vector<FlowSegment *>::const_iterator iter;
	
	// add each segment into the port share
	for(iter = flow.flowPath.begin(); iter != --flow.flowPath.end(); iter++){
		FlowSegment *fs = *iter;
		string destIP = fs->destIPAddress;
		
		if(this->portShare.find(destIP) == this->portShare.end()){
			// there is not such port in share state yet
			unordered_set<uint64_t> *newSet = new unordered_set<uint64_t>();
			newSet->insert(flow.flowID);
			this->portShare[destIP] = newSet;
		}else{
			unordered_set<uint64_t> *set = this->portShare[destIP];
			set->insert(flow.flowID);
		}
	}

	//printPortShare();
}


void VeriCongestion::deleteFlow(Flow flow){

/*
	fprintf(stdout, "This time i'm going to delete this flow:\n");
	flow.printMyself();
	fprintf(stdout, "--------------------------------------------------\n");
*/

	// delete each segment from the port share
	vector<FlowSegment *>::const_iterator iter;
	for(iter = flow.flowPath.begin(); iter != --flow.flowPath.end(); iter++){
		FlowSegment *fs = *iter;
		string destIP = fs->destIPAddress;
		
		unordered_set<uint64_t> *set = this->portShare[destIP];
		set->erase(flow.flowID);
	}

	uint64_t flowID = flow.flowID;
	bool deleted = this->wholeNetwork.deleteFlow(flowID);
	if(!deleted){
		fprintf(stdout, "error when deleting\n");
	}else{
		//fprintf(stdout, "delete this flow with ID %ld\n", flowID);
	}

	//printPortShare();
}



bool VeriCongestion::verifyAFlow(Flow flow, bool exp_mode, int x){	// x indicates x_th experiment

	clock_t before, after;
	clock_t t1, t2;
	double diff;

	string str = to_string(x);

	if(exp_mode){

		//string fileName = "timeStats"+str+".txt";
		timeStatsFile = fopen( ("timeStats_"+str+".txt").c_str(), "a");
		numCongestionFile = fopen( ("numCongestion_"+str+".txt").c_str(), "a");
		congestedFlowsFile = fopen( ("congestedFlows_"+str+".txt").c_str(), "a");
		numDependencyGraphFile = fopen( ("numDependencyGraph_"+str+".txt").c_str(), "a");
		graphSizeFile = fopen( ("graphSize_"+str+".txt").c_str(), "a");
		simulationBreakdownFile = fopen( ("simulationBreakdown_"+str+".txt").c_str(), "a");

		before = clock();
	}

	fprintf(stdout, "\n");
	fprintf(stdout, "\n");
	fprintf(stdout, "This time I'm going to verify flow with id = %ld\n", flow.flowID);
	
	if(exp_mode){
		
		this->num_verification++;
		fprintf(timeStatsFile, "%d ", num_verification);
		fprintf(numCongestionFile, "%d ", num_verification);
		fprintf(congestedFlowsFile, "%d ", num_verification);
		fprintf(numDependencyGraphFile, "%d ", num_verification);
		fprintf(graphSizeFile, "%d ", num_verification);
		fprintf(simulationBreakdownFile, "%d ", num_verification);
	}

	uint64_t flowID = flow.flowID;
	bool verifyDelete = this->wholeNetwork.flowExists(flowID);	// if this ID already exists, then we want to delete it

	if(verifyDelete){
		flow = this->wholeNetwork.getFlow(flowID);	// get the actual flow from my own network
		//deleteFlow(flow);	dont delete now, because we need to modify the portList in simulation
	}else{
		addNewFlow(flow);	//dont add it now, we can add into portList individually
	}

	int num_congested_flows = 0;

	if(exp_mode){
		t1 = clock();
	}

/*
	fprintf(stdout, "all flows are: \n");
	this->wholeNetwork.printFlows();

	fprintf(stdout, "current port share state\n");
	printPortShare();
*/
/*
	fprintf(stdout, "current flow: ");
	flow.printMyself();
*/

	Network *subnetwork = generateRelevantNetwork(flow);


	//fprintf(stdout, "before simulate\n");

	if(exp_mode){

		t2 = clock();
		diff = ((float)t2-(float)t1) / CLOCKS_PER_SEC * 1000;
		fprintf(timeStatsFile, "%lf ", diff);			// generation time
		t1 = clock();


		num_congested_flows = simulate(subnetwork, flow, simulationBreakdownFile, true);
	}else{
		num_congested_flows = simulate(subnetwork, flow, simulationBreakdownFile, false);
	}

	//fprintf(stdout, "after simulate\n");

	if(exp_mode){
		
		t2 = clock();
		diff = ((float)t2-(float)t1) / CLOCKS_PER_SEC * 1000;
		fprintf(timeStatsFile, "%lf ", diff);				// simulation time
		fprintf(simulationBreakdownFile, "%lf\n", diff);	// simulation time
		fclose(simulationBreakdownFile);
	}

	if(num_congested_flows > 0){
		fprintf(stdout, "there is congestion!\n");
		
		if(exp_mode){
			num_congestion++;
		}

		if(verifyDelete){
			;
		}else{
			deleteFlow(flow);
		}

	}else{
		// no congestion, perform the actual add/delete finally
		if(verifyDelete){
			deleteFlow(flow);
		}else{
			;
		}
	}

	if(exp_mode){

		after = clock();
		diff = ((float)after-(float)before) / CLOCKS_PER_SEC * 1000;	// total running time
		fprintf(timeStatsFile, "%lf\n", diff);

		if(current_graph_size > 0){
			this->num_dependencyGraph++;
		}

		fprintf(numCongestionFile, "%d\n", num_congestion);
		fprintf(congestedFlowsFile, "%d\n", num_congested_flows);
		fprintf(numDependencyGraphFile, "%d\n", num_dependencyGraph);
		fprintf(graphSizeFile, "%d\n", current_graph_size);

		fclose(timeStatsFile);
		fclose(numCongestionFile);
		fclose(congestedFlowsFile);
		fclose(numDependencyGraphFile);
		fclose(graphSizeFile);
	}

	return (num_congested_flows == 0);
}

int VeriCongestion::simulate(Network* network, Flow flow, FILE *pFile, bool exp_mode){

	Simulation simulation;

	clock_t t1, t2;
	double diff;

	int num_congestion = 0;
	int num_ports = 0;

	if(exp_mode){
		t1 = clock();
	}

	//fprintf(stdout, "before setup\n");

	simulation.setup(network, flow);

	//fprintf(stdout, "after setup\n");

	//fprintf(stdout, "before rule based\n");

	bool allPortsResolved = simulation.ruleBasedUpdate(network);

	//fprintf(stdout, "after rule based\n");


	if(exp_mode){
		t2 = clock();
		diff = ((float)t2-(float)t1) / CLOCKS_PER_SEC * 1000;
		fprintf(simulationBreakdownFile, "%lf ", diff);
		t1 = clock();
	}

	if(allPortsResolved){

		//fprintf(stdout, "Time to calculate the egress rate for each flow\n");
		num_congestion = simulation.calculateEgress(network, flow);		// egress rate == in rate of the end hosts
		num_ports = simulation.getCongestedPorts();
		
		if(num_congestion > 0){
			this->current_graph_size = 0;
		}else{
			this->current_graph_size = -1;
		}
		

		diff = 0.0;

		if(exp_mode){
			fprintf(simulationBreakdownFile, "%lf %lf %lf ", diff, diff, diff);
			fprintf(congestedFlowsFile, "%d ", num_ports);

		}

	}else{

		fprintf(stdout, "there is dependency graph!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");

		//fprintf(stdout, "before generate graph\n");

		this->current_graph_size = simulation.generateDependenceGraph(network);

		//fprintf(stdout, "after generate graph\n");

		if(exp_mode){
			t2 = clock();
			diff = ((float)t2-(float)t1) / CLOCKS_PER_SEC * 1000;
			fprintf(simulationBreakdownFile, "%lf ", diff);
			t1 = clock();
		}

		//fprintf(stdout, "before fixed pointer iteration\n");

		simulation.fixedPointIteration(network);

		//fprintf(stdout, "after fixed point iteration\n");
		
		if(exp_mode){
			t2 = clock();
			diff = ((float)t2-(float)t1) / CLOCKS_PER_SEC * 1000;
			fprintf(simulationBreakdownFile, "%lf ", diff);
			t1 = clock();
		}

		allPortsResolved = simulation.ruleBasedUpdate(network);

		if(exp_mode){
			t2 = clock();
			diff = ((float)t2-(float)t1) / CLOCKS_PER_SEC * 1000;
			fprintf(simulationBreakdownFile, "%lf ", diff);
			t1 = clock();
		}

		if(allPortsResolved){
			//fprintf(stdout, "ALL PORTS RESOLVED\n");
			//fprintf(stdout, "Time to calculate the egress rate for each flow\n");
			num_congestion = simulation.calculateEgress(network, flow);		// egress rate == in rate of the end hosts
			num_ports = simulation.getCongestedPorts();

			if(exp_mode){
				fprintf(congestedFlowsFile, "%d ", num_ports);
			}


		}else{
			fprintf(stdout,"ERROR: still not resolved!!!\n");
		}
	}

	return num_congestion;
}

Network *VeriCongestion::generateRelevantNetwork(Flow flow){

	Network *network = new Network();

	unordered_set<uint64_t> affectedFlows;

	for(vector<FlowSegment *>::iterator iter = flow.flowPath.begin(); iter != --flow.flowPath.end(); iter++){
		FlowSegment *fs = *iter;
		string destIP = fs->destIPAddress;
		unordered_set<uint64_t> *set = this->portShare[destIP];
		for(unordered_set<uint64_t>::iterator iter2 = set->begin(); iter2 != set->end(); iter2++){
			uint64_t id = *iter2;
			affectedFlows.insert(id);
		}
	}
/*
	fprintf(stdout, "affected flows are \n");
	for(unordered_set<uint64_t>::iterator iter3 = affectedFlows.begin(); iter3 != affectedFlows.end(); iter3++){
		fprintf(stdout, "flow id = %ld\n", *iter3);
	}
*/


	unordered_set<string> ports;
	for(vector<FlowSegment *>::iterator iter = flow.flowPath.begin(); iter != --flow.flowPath.end(); iter++){
		FlowSegment *fs = *iter;
		ports.insert(fs->destIPAddress);
	}

	unordered_map< string, ForwardingDevice > map = this->wholeNetwork.getDeviceMap();
	network->setDeviceMap(map);

	// insert every flow, and corresponding devices in the set into the subnetwork
	for(unordered_set<uint64_t>::iterator iter3 = affectedFlows.begin(); iter3 != affectedFlows.end(); iter3++){
		Flow flow = this->wholeNetwork.getFlow(*iter3);
		
		vector<FlowSegment*>::iterator previous_it = flow.flowPath.begin();

		// modify the flow segments, so that they only contain the critical ports
		for(vector<FlowSegment*>::iterator itr4 = next(flow.flowPath.begin()); itr4 != flow.flowPath.end(); ){

			FlowSegment *previousFS = *(previous_it);

			if(itr4 == flow.flowPath.end()-1){
				FlowSegment *fs = *itr4;
				fs->sourceIPAddress = previousFS->destIPAddress;
				fs->segmentID = previousFS->segmentID+1;
				previous_it = itr4;

				itr4++;

			}else{
				
				FlowSegment *fs = *itr4;
				if(ports.find(fs->destIPAddress) == ports.end()){
					// erase the segments that don't pass the "port"
					itr4 = flow.flowPath.erase(itr4);
				}else{
					FlowSegment *fs = *itr4;
					fs->sourceIPAddress = previousFS->destIPAddress;
					fs->segmentID = previousFS->segmentID+1;
					previous_it = itr4;

					itr4++;
				}
			}
		}
		network->addFlow(flow);
	}
/*
	fprintf(stdout, "----------------------------print network----------\n");
	network->print();
	fprintf(stdout, "----------------------------print flows---------------\n");
	network->printFlows();
*/
	//network->print();

	return network;
}



void VeriCongestion::printPortShare() {
	
	unordered_map<string, unordered_set<uint64_t> *>::const_iterator iter;
	
	for(iter = this->portShare.begin(); iter != this->portShare.end(); iter++){
		string portIP = iter->first;
		unordered_set<uint64_t> *set = iter->second;
		fprintf(stdout, "Port %s 's flows:\n", portIP.c_str());
		
		unordered_set<uint64_t>::const_iterator iter2;
		for(iter2 = set->begin(); iter2 != set->end(); iter2++){
			uint64_t flowID = *(iter2);
			Flow flow = this->wholeNetwork.getFlow(flowID);
			fprintf(stdout, "flow ID = %ld\n", flow.flowID);
		}
	}
}

void VeriCongestion::printFlows() const{
	this->wholeNetwork.printFlows();
}

void VeriCongestion::printStats() const{
	;
}
