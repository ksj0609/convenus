
#ifndef VeriCongestion_H_
#define VeriCongestion_H_

#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <list>
#include "Network.h"
#include "PortVolumnState.h"
#include "Simulation.h"
#include "Flow.h"

using namespace std;

/** The prototype of our congestion-verification tool (Convenus).
*	This tool contains the network model, the simulation engine, and
*	a mechanism to verify congestion and update the network state. It
*	provide interfaces for the applications to issue flow updates.
*/
class VeriCongestion
{
private:
	Network wholeNetwork;	/**< The model of the whole network, represented by a Network object*/			

	unordered_map<string, unordered_set<uint64_t> *> portShare;	/**< A HashMap that maps a port to the set of flows sharing it*/

	int num_verification = 0;							/**< Experiment parameter: how many verifications performed*/
	int num_congestion = 0;								/**< Experiment parameter: how many verifications have congestion */
	int num_dependencyGraph = 0;						/**< Experiment parameter: how many verifications have dependency graph*/
	int current_graph_size = 0;							/**< Experiment parameter: the size of current dependency graph, 0 if no dependency graph generated*/
	double avg_affectedFlows = 0.0;						/**< Experiment parameter: how many flows pass through the congested ports*/

	// statistics to measure
	double total_find_affected_time = 0.0;				/**< Experiment stats: time spent to identify the effected flows by the incoming new flow update*/
	double total_generate_subnetwork_time = 0.0;		/**< Experiment stats: time spent to construct the minimum affected sub-network*/
	double total_simulation_time = 0.0;					/**< Experiment stats: total time spent in simulation*/

public:
	VeriCongestion(Network& network);					/**< Constructor*/
	void addNewFlow(Flow flow);							/**< An interface called by the "applications" that are trying to issue a new flow into the network*/
	void deleteFlow(Flow flow);							/**< An interface called by the "applications" that are trying to issue a flow deletion from the network*/
	
	Network *generateRelevantNetwork(Flow flow);		/**< Generate the minimum affected sub-network, given the current network state and a new flow update*/

	int simulate(Network* network, Flow flow, FILE *pFile, bool exp_mode); /**< Run the 4-phase simulation algorithm implemented by the Simulation class, and collect some experement data*/
	bool verifyAFlow(Flow flow, bool exp_mode, int x);	/**< For flow addition and deletion, verify if the operation is goint to cause congestion in the network*/

	void printPortShare();								/**< Print the port-sharing information*/
	void printFlows() const;							/**< Print the flows in the network*/
	void printStats() const;							/**< Print the experiment statistics*/
};

#endif /* VeriCongestion_H_ */