#include <sys/types.h>
#include <unistd.h>
#include <cstdio>
#include <string>
#include <unordered_map>
#include <map>
#include <set>
#include <list>
#include <algorithm>
#include "Simulation.h"
#include "Network.h"
#include "ForwardingDevice.h"
#include "PortVolumnState.h"

using namespace std;

void Simulation::setup(Network* network, Flow currentFlow) {

	// iterate through all flows, initialize all segments to be -1.0 and unsettled
	unordered_map< uint64_t, Flow >::iterator itr1;

	for(itr1 = network->flowMap.begin(); itr1 != network->flowMap.end(); itr1++){

		Flow flow = itr1->second;
		flow.egressRate = -1.0;

		vector< FlowSegment *>::iterator itr2;

		for(itr2 = next(flow.flowPath.begin()); itr2 != flow.flowPath.end(); itr2++){
			
			//start from the second segment, re-initialize 
			FlowSegment *fs = *itr2;
			fs->in_rate = -1.0;
			fs->type = unsettled;	
		}
	}
/*
	// second, store in the corresponding position in the portList
	for(itr1 = network->flowMap.begin(); itr1 != network->flowMap.end(); itr1++){

		Flow flow = itr1->second;

		// insert each flow's each segment into the portList
		vector<FlowSegment *>::iterator itr3;

		for(itr3 = flow.flowPath.begin(); itr3 != --flow.flowPath.end(); itr3++){
					
			FlowSegment *fs = *itr3;
			string ip = fs->destIPAddress;
			PortVolumnState *currentPVS = NULL;
			list<PortTuple>::iterator itr4;

			for(itr4 = this->portList.begin(); itr4 != this->portList.end(); itr4++){
				PortTuple pt = *itr4;
				if(pt.portIP == ip){
					currentPVS = pt.pvs;
					itr4 = this->portList.erase(itr4);
					break;
				}
			}

			if(currentPVS != NULL){
				currentPVS->in_segment.insert(fs);	
			}else{

				// initialize a new pvs
				currentPVS = new PortVolumnState();
				currentPVS->in_segment.insert(fs);
				currentPVS->portState = unresolved;
				const ForwardingDevice& device = network->deviceMap[ip];
				currentPVS->bandwidth = device.outPortBandwidth;
			}

			PortTuple pt = PortTuple(ip, currentPVS);
			this->portList.push_back(pt);		
		}
	}
*/

	// second, store in the corresponding position in the portList
	for(itr1 = network->flowMap.begin(); itr1 != network->flowMap.end(); itr1++){

		Flow flow = itr1->second;

		vector<FlowSegment *>::iterator itr2;
		for(itr2 = flow.flowPath.begin(); itr2 != --flow.flowPath.end(); itr2++){
			FlowSegment *fs = *itr2;
			if(this->portMap.find(fs->destIPAddress) == this->portMap.end()){
				PortVolumnState *pvs = new PortVolumnState();
				const ForwardingDevice& device = network->deviceMap[fs->destIPAddress];
				pvs->bandwidth = device.outPortBandwidth;
				pvs->portState = unresolved;
				pvs->in_segment.insert(fs);
				this->portMap[fs->destIPAddress] = pvs;
			}
			else{
				PortVolumnState *pvs = this->portMap[fs->destIPAddress];
				pvs->in_segment.insert(fs);
			}
		}
	}
}

bool Simulation::ruleBasedUpdate(Network* network){

//	list< PortTuple >::iterator itr1;

	unordered_map<string, PortVolumnState *>::iterator itr1;
	/*
		Put the whole procedure in a big while loop
		if this iteration has upgraded NOTHING, then the loop terminate
		if there is still upgrade, then continue
	*/

	bool newChangeHappend = false;
	int counter = 1;

	do{

		/*
		fprintf(stdout, "**************************************************************************************\n");
		fprintf(stdout, "Iteration %d\n", counter);
		fprintf(stdout, "**************************************************************************************\n");
		*/

		newChangeHappend = false;

		//for(itr1 = this->portList.begin(); itr1 != this->portList.end(); itr1++)
		
		for(itr1 = this->portMap.begin(); itr1 != this->portMap.end(); itr1++)
		{
			//PortTuple pt = *itr1;

			//string ip = pt.portIP;
			//PortVolumnState *pvs = pt.pvs;
			
			string ip = itr1->first;
			PortVolumnState *pvs = itr1->second;


			unordered_set< FlowSegment *> segments = pvs->in_segment;

			unordered_set< FlowSegment *>::iterator it;

			double in_rate_total;
			double in_rate_settled_total;
			double product;

			//fprintf(stdout, "Examing port %s ------------------------------------------------------------------\n", ip.c_str());

			if(pvs->portState == resolved){
				
				//fprintf(stdout, "This port is resolved, skip\n");
				
				continue;

			}else if(pvs->portState == transparent){
				
				//fprintf(stdout, "This port is transparent, see what I can do\n");

				// Rule 3 and 4
				for(it = segments.begin(); it != segments.end(); it++){
						
					//fprintf(stdout, "current segment: \n");
					//(*it)->printMyself();

					FlowSegment *fs = *it;
					uint64_t flowID = fs->myFlowID;
					uint64_t myID = fs->segmentID;
					Flow flow = network->flowMap[flowID];
					FlowSegment *next = flow.flowPath[++myID];
					
					if(fs->type == settled && next->type != settled){
						
						next->type = settled;
						next->in_rate = fs->in_rate;
						
						//fprintf(stdout, "next segment will have rate %lf\n", next->in_rate);
						
						newChangeHappend = true;

					}else if(fs->type == bounded){
						
						if( next->type == unsettled || (next->type == bounded && fs->in_rate < next->in_rate) ){
							next->type = bounded;
							next->in_rate = fs->in_rate;
							
							//fprintf(stdout, "next segment will have (bounded) rate %lf\n", next->in_rate);
							newChangeHappend = true;
						}
					} 
				}

			}else{		// unresolved
				
				//fprintf(stdout, "This port is unresolved\n");

				//1. Check if all the flows are settled
				bool allSettled = true;
				bool noUnsettled = true;

				for(it = segments.begin(); it != segments.end(); it++){
					FlowSegment *fs = *it;
					if(fs->type != settled){
						allSettled = false;
					}
					if(fs->type == unsettled){
						noUnsettled = false;
					}
				}

				//2. if all settled, rule 1
				if(allSettled){

					//fprintf(stdout, "All flows resolved!!!!\n");
					
					pvs->portState = resolved;

					// calculate sum of flows
					in_rate_total = 0.0;
					for(it = segments.begin(); it != segments.end(); it++){
						FlowSegment *fs = *it;
						in_rate_total += fs->in_rate;
					}

					//fprintf(stdout, "The total rate is %lf\n", in_rate_total);

					product = 1.0;
					if (in_rate_total > pvs->bandwidth){
						product = pvs->bandwidth / in_rate_total;
					}

					for(it = segments.begin(); it != segments.end(); it++){
						
						//fprintf(stdout, "current segment: \n");
						//(*it)->printMyself();

						FlowSegment *fs = *it;
						uint64_t flowID = fs->myFlowID;
						uint64_t myID = fs->segmentID;
						Flow flow = network->flowMap[flowID];
						FlowSegment *next = flow.flowPath[++myID];
						next->type = settled;
						next->in_rate = fs->in_rate * product;	// This update will resolve on the fly!!!!
						
						//fprintf(stdout, "next segment will have rate %lf\n", next->in_rate);
					}

					newChangeHappend = true;

				}else if(noUnsettled){		// settled and bounded

					//fprintf(stdout, "No flow unsettled!!!!\n");

					// calculate sum of bounded flows
					in_rate_total = 0.0;
					for(it = segments.begin(); it != segments.end(); it++){
						FlowSegment *fs = *it;
						in_rate_total += fs->in_rate;
					}

					//fprintf(stdout, "The total (bounded) rate is %lf\n", in_rate_total);
					
					if(in_rate_total <= pvs->bandwidth){

						// Rule 2, transparent
						
						//fprintf(stdout, "Total (bounded) rate <= bandwidth, this port is going to become transparent!!!!\n");
						pvs->portState = transparent;

						for(it = segments.begin(); it != segments.end(); it++){
						
							//fprintf(stdout, "current segment: \n");
							//(*it)->printMyself();


							FlowSegment *fs = *it;
							uint64_t flowID = fs->myFlowID;
							uint64_t myID = fs->segmentID;
							Flow flow = network->flowMap[flowID];
							FlowSegment *next = flow.flowPath[++myID];
							next->type = fs->type;
							next->in_rate = fs->in_rate;	
							
							//fprintf(stdout, "next segment will have rate %lf\n", next->in_rate);
						}

						newChangeHappend = true;

					}else{

						//fprintf(stdout, "Total (bounded) rate > bandwidth, NOT transparent, see what I can do\n");

						// it's NOT transparent, but we can still bound ALL flows
						// Rule 5 and 6
						in_rate_settled_total = 0.0;
					
						for(it = segments.begin(); it != segments.end(); it++){
						
							FlowSegment *fs = *it;
							if(fs->type == settled){
								in_rate_settled_total += fs->in_rate;
							}
						}

						for(it = segments.begin(); it != segments.end(); it++){
						
							FlowSegment *fs = *it;
							uint64_t flowID = fs->myFlowID;
							uint64_t myID = fs->segmentID;
							Flow flow = network->flowMap[flowID];
							FlowSegment *next = flow.flowPath[++myID];
							
							if(fs->type == settled){

								product = 1.0;

								if(product > pvs->bandwidth / in_rate_settled_total){
									product = pvs->bandwidth / in_rate_settled_total;
								}

								double newBound = fs->in_rate * product;
								if(next->type == unsettled || (next->type == bounded && next->in_rate > newBound) ){
									
									//fprintf(stdout, "flow ID = %ld, next flow is given a new bound, which is %lf\n", next->myFlowID, newBound);
									
									next->type = bounded;
									next->in_rate = newBound;

									newChangeHappend = true;
								}

							}else if(fs->type == bounded){

								product = 1.0;

								if(product > pvs->bandwidth / (in_rate_settled_total+fs->in_rate) ){
									product = pvs->bandwidth / (in_rate_settled_total+fs->in_rate);
								}
							
								double newBound = fs->in_rate * product;
								if(next->type == unsettled || (next->type == bounded && next->in_rate > newBound) ){
									
									//fprintf(stdout, "flow ID = %ld, next flow is given a new bound, which is %lf\n", next->myFlowID, newBound);
									
									next->type = bounded;
									next->in_rate = newBound;

									newChangeHappend = true;
								}
							}
						}
					}

				}else{		// there is unsettled flow

					//fprintf(stdout, "There is unsettled flows!!!!\n");

					bool allUnsettled = true;

					in_rate_settled_total = 0.0;
					
					for(it = segments.begin(); it != segments.end(); it++){
						
						FlowSegment *fs = *it;
						if(fs->type == settled){
							allUnsettled = false;
							in_rate_settled_total += fs->in_rate;
						}else if(fs->type == bounded){
							allUnsettled = false;
						}else{
							;
						}
					}

					if(allUnsettled){
						
						//fprintf(stdout, "All flows are unsettled, skip this port!!!!\n");
						
						continue;
					}

					// Rule 5 and 6
					for(it = segments.begin(); it != segments.end(); it++){
						
						FlowSegment *fs = *it;
						uint64_t flowID = fs->myFlowID;
						uint64_t myID = fs->segmentID;
						Flow flow = network->flowMap[flowID];
						FlowSegment *next = flow.flowPath[++myID];
						
						if(fs->type == settled){

							product = 1.0;

							if(product > pvs->bandwidth / in_rate_settled_total){
								product = pvs->bandwidth / in_rate_settled_total;
							}

							double newBound = fs->in_rate * product;
							if(next->type == unsettled || (next->type == bounded && next->in_rate > newBound) ){
								
								//fprintf(stdout, "Flow ID = %ld, next flow is given a new bound, which is %lf\n", next->myFlowID, newBound);
								
								next->type = bounded;
								next->in_rate = newBound;

								newChangeHappend = true;
							}

						}else if(fs->type == bounded){

							product = 1.0;

							if(product > pvs->bandwidth / (in_rate_settled_total+fs->in_rate) ){
								product = pvs->bandwidth / (in_rate_settled_total+fs->in_rate);
							}
						
							double newBound = fs->in_rate * product;
							if(next->type == unsettled || (next->type == bounded && next->in_rate > newBound) ){
								
								//fprintf(stdout, "Flow ID = %ld, next flow is given a new bound, which is %lf\n", next->myFlowID, newBound);
								
								next->type = bounded;
								next->in_rate = newBound;

								newChangeHappend = true;
							}
						}
					}
				}
			}
			//fprintf(stdout, "Done------------------------------------------------------------------------------------------\n");
		}

		counter++;

	}while(newChangeHappend);

	fprintf(stdout, "rule based update has #iteration = %d\n", counter-1);

	// check if all ports are resolved and transparent
	//for(list< PortTuple >::const_iterator it = this->portList.begin(); it != this->portList.end(); it++)
	for(unordered_map<string, PortVolumnState *>::const_iterator it = this->portMap.begin(); it != this->portMap.end(); it++)
	{
		//PortVolumnState *pvs = (*it).pvs;
		
		PortVolumnState *pvs = it->second;
		if(pvs->portState == unresolved){
			return false;
		}
	}

	return true;
}


int Simulation::generateDependenceGraph(Network* network){

	// 1. find all nodes (unresolved ports) of the dependence graph
	//list< PortTuple >::const_iterator itr1;
	//for(itr1 = this->portList.begin(); itr1 != this->portList.end(); itr1++)

	for(unordered_map<string, PortVolumnState *>::const_iterator itr1 = this->portMap.begin(); itr1 != this->portMap.end(); itr1++)
	{

		//PortTuple pt = *itr1;
		//string ip = pt.portIP;
		//PortVolumnState *pvs = pt.pvs;

		string ip = itr1->first;
		PortVolumnState *pvs = itr1->second;

		if(pvs->portState == unresolved){

			dependenceGraph.insert(new DependenceGraphNode(ip));
		}
	}
	
	// 2. find all edges of the dependence graph

	//fprintf(stdout, "=======================iterate every flow on every unresolved port=====================================\n");

	unordered_set< DependenceGraphNode * >::iterator itr2;
	
	for(itr2 = dependenceGraph.begin(); itr2 != dependenceGraph.end(); itr2++){

		// find each unresolved port
		DependenceGraphNode *node = *itr2;

		string ip = node->myNode;
		//fprintf(stdout, "this port has deviceID = %s portID = %d:\n", pid.deviceIP.c_str(), pid.portID);

		PortVolumnState *pvs = NULL;
		
		/*
		for(list<PortTuple>::iterator it = this->portList.begin(); it != this->portList.end(); it++){
			if( (*it).portIP == ip ){
				pvs = (*it).pvs;
				break;
			}
		}
		*/

		for(unordered_map<string, PortVolumnState *>::iterator it = this->portMap.begin(); it != this->portMap.end(); it++){
			if(it->first == ip){
				pvs = it->second;
				break;
			}
		}


		if(pvs == NULL){
			fprintf(stdout, "ERROR: ip doesn't exist!!!!\n");
		}


		unordered_set< FlowSegment *> segments = pvs->in_segment;	// This set may contain bounded and settled segments, DO we need to execlude settled ones?
		
		// trace each flow on this port
		for(unordered_set< FlowSegment *>::const_iterator it = segments.begin(); it != segments.end(); it++){

			// TODO
			// trace until the end, or until find a segment into an unresolved port

			FlowSegment *original = *it;
			FlowSegment *current = *it;

			uint64_t flowID = current->myFlowID;
			Flow flow = network->flowMap[flowID];

			//fprintf(stdout, "Flow ID = %ld\n", flowID);

			do{

				uint64_t myID = current->segmentID;
				//fprintf(stdout, "current segment ID = %ld\n", myID);
				
				FlowSegment *next = flow.flowPath[myID+1];

				// check if "next" is pointed to an unresolved port, if so, break, NEXT flow
				// if not, continue to next segment
					
				string nextDeviceIP = next->destIPAddress;
				//string nextDeviceAddress = network.idToIpAddressMap[nextDeviceID];
				ForwardingDevice nextDevice = network->deviceMap[nextDeviceIP];

				if(nextDevice.endDevice){
				
					//fprintf(stdout, "next device is an end device !!!\n");
					break;		// cannot be an unresolved port, because end devices don't have an unresolved port
				
				}else{

					PortVolumnState *nextPVS = NULL;
/*
					for(list<PortTuple>::iterator it = this->portList.begin(); it != this->portList.end(); it++){
						if( (*it).portIP == next->destIPAddress ){
							nextPVS = (*it).pvs;
							break;
						}
					}
*/
					for(unordered_map<string, PortVolumnState *>::iterator it = this->portMap.begin(); it != this->portMap.end(); it++){
						if(it->first == next->destIPAddress){
							nextPVS = it->second;
							break;
						}
					}



					if(nextPVS == NULL){
						fprintf(stdout, "ERROR: ip doesn't exist!!!!\n");
					}

					if(nextPVS->portState == unresolved){
						//fprintf(stdout, "next port is unresolved, generate this edge, and save the corresponding flow segment\n");
						node->directedNodes.push_back(next->destIPAddress);
						criticalSegments.insert(next);

						causalRelation[next] = original;
						break;
					}
					else{
						current = next;
					}
				}
			}
			while(current != flow.flowPath.back());
		}
 	}

	// 3. remove those ports with no out edges
	
	bool nodesDeleted = false;
	list< string > deletedNodes;

	do{
		nodesDeleted = false;

		unordered_set< DependenceGraphNode * >::const_iterator it;
		
		for(it = dependenceGraph.begin(); it != dependenceGraph.end(); ){
			
			DependenceGraphNode *node = *it;

			if(node->directedNodes.empty()){
				deletedNodes.push_back(node->myNode);
				it = dependenceGraph.erase(it);

				//fprintf(stdout, "Node <%s, %d> got deleted, since it doesn't have any out edge!!!\n", node->myNode.deviceIP.c_str(), node->myNode.portID);
				nodesDeleted = true;
			}else{
				it++;
			}
		}

		// delete the edges which pointed to this deleted node; and corresponding critical segment
		for(it = dependenceGraph.begin(); it != dependenceGraph.end(); it++){

			DependenceGraphNode *node = *it;

			for(list< string >::iterator iter = node->directedNodes.begin(); iter != node->directedNodes.end();){

				if(std::find( deletedNodes.begin(), deletedNodes.end(), *iter ) != deletedNodes.end() ){
					iter = node->directedNodes.erase(iter);
					// delete corresponding flow segment?
					// YES, because there are "critical segment" that point to those nodes
					// Then, they are NOT really critical

				}else{
					iter++;
				}
			}
		}

		// delete corresponding critical segment
		for(set< FlowSegment *>::const_iterator it2 = criticalSegments.begin(); it2 != criticalSegments.end();){

			FlowSegment *fs = *it2;
			string ip = fs->destIPAddress;
			if(std::find( deletedNodes.begin(), deletedNodes.end(), ip) != deletedNodes.end() ){
				it2 = criticalSegments.erase(it2);
			}else{
				it2++;
			}
		}
	}
	while(nodesDeleted);
	
	if(dependenceGraph.empty()){
		fprintf(stdout, "after removal, becomes empty!!!\n");
	}

	int graphSize = dependenceGraph.size();

/*
	fprintf(stdout, "graph size is %d\n", graphSize);

	if(graphSize > 2){
		printDependenceGraph(network, graphSize);
	}
*/


	return graphSize;
} 

void Simulation::fixedPointIteration(Network* network){

	// objective, settle all the flow segments in the list "critical segment"

	map< FlowSegment *, double> previousIteration;	// record each critical segment's previous values

	// initialize previous iteration
	for(set< FlowSegment *>::iterator itr1 = criticalSegments.begin(); itr1 != criticalSegments.end(); itr1++){

		FlowSegment *fs = *itr1;
		previousIteration[fs] = fs->in_rate;
	}

	/*
	fprintf(stdout, "Initial values of critical segments\n");
	
	for( map< FlowSegment *, double>::const_iterator it = previousIteration.begin(); it != previousIteration.end(); it++){
		FlowSegment *fs = it->first;
		double value = it->second;
		fprintf(stdout, "segment [ flow id = %ld, segment id = %ld ] has value = %lf\n", fs->myFlowID, fs->segmentID, value);
	}
	*/

	bool foundFixedPoint;

	do{
		
		for(set< FlowSegment *>::iterator itr1 = criticalSegments.begin(); itr1 != criticalSegments.end(); itr1++){
	
			FlowSegment *current = *itr1;
			FlowSegment *causing = causalRelation[current];
			string ip = causing->destIPAddress;				// causing is on port pid

			// Now, calculate current rate, using Previous values!!!

			//fprintf(stdout, "calculating flow [flow id = %ld, segment id = %ld]: \n", current->myFlowID, current->segmentID);
			//fprintf(stdout, "	the causing flow is [flow id = %ld, segment id = %ld], which is on the port <%ld, %d> \n", 
			//			causing->myFlowID, causing->segmentID, pid.deviceID, pid.portID);

			PortVolumnState *pvs = NULL;
/*
			for(list<PortTuple>::iterator it = this->portList.begin(); it != this->portList.end(); it++){
				if( (*it).portIP == ip ){
					pvs = (*it).pvs;
					break;
				}
			}
*/
			for(unordered_map<string, PortVolumnState *>::iterator it = this->portMap.begin(); it != this->portMap.end(); it++){
				if(it->first == ip){
					pvs = it->second;
					break;
				}
			}


			if(pvs == NULL){
				fprintf(stdout, "ERROR: ip doesn't exist!!!!\n");
			}

			unordered_set< FlowSegment *> segments = pvs->in_segment;

			double in_rate_total = 0.0;

			for(unordered_set< FlowSegment *>::const_iterator itr2=segments.begin(); itr2 != segments.end(); itr2++){

				FlowSegment *fs = *itr2;
				if(fs->type == settled){
					in_rate_total += fs->in_rate;
				}else{

					if( criticalSegments.find(fs) == criticalSegments.end() ){
						//fprintf(stdout, "ERROR: segment neither settled nor in critical!!!!!!!!!!!!!!\n");
						continue;
					}else{
						in_rate_total += previousIteration[fs];
					}
				}
			}

			double product = 1.0;
			if(product > pvs->bandwidth / in_rate_total){
				product = pvs->bandwidth / in_rate_total;
			}

			current->in_rate = causing->in_rate * product;
		}

		if(valuesFixed(previousIteration)){

			// values didn't change, terminate
			foundFixedPoint = true;
		}else{
			
			for(set< FlowSegment *>::iterator itr1 = criticalSegments.begin(); itr1 != criticalSegments.end(); itr1++){
				FlowSegment *fs = *itr1;
				previousIteration[fs] = fs->in_rate;
			}
			foundFixedPoint = false;

			/*
			fprintf(stdout, "after this iteration, fixed point not found.. new values: \n");
			for( map< FlowSegment *, double>::const_iterator it = previousIteration.begin(); it != previousIteration.end(); it++){
				FlowSegment *fs = it->first;
				double value = it->second;
				fprintf(stdout, "segment [ flow id = %ld, segment id = %ld ] has value = %lf\n", fs->myFlowID, fs->segmentID, value);
			}
			*/
		}
	}
	while(!foundFixedPoint);

	//fprintf(stdout, "Now values are fixed: \n");
	set< FlowSegment *>::iterator itr3;
	
	for(itr3 = criticalSegments.begin(); itr3 != criticalSegments.end(); itr3++){
		
		FlowSegment *fs = *itr3;

		//fprintf(stdout, "final value for critical segment [ flow id = %ld, segment id = %ld ] is %lf\n", fs->myFlowID, fs->segmentID, fs->in_rate);

		fs->type = settled;
	}
}

bool Simulation::valuesFixed(map< FlowSegment *, double> previousIteration){

	for(set< FlowSegment *>::iterator itr1 = criticalSegments.begin(); itr1 != criticalSegments.end(); itr1++){

		FlowSegment *fs = *itr1;
		double previousValue = previousIteration[fs];
/*
		fprintf(stdout, "comparing current rate with previous rate:\n");
		fprintf(stdout, "critical segment: flow = %ld, segment = %ld	", fs->myFlowID, fs->segmentID);
		fprintf(stdout, "previous value = %lf, current value = %lf\n", previousValue, fs->in_rate);
*/
		/*
		if(previousValue != fs->in_rate){
			return false;
		}
		*/

		if(abs(previousValue - fs->in_rate) > 1.0){
			return false;
		}
	}

	return true;
}

int Simulation::calculateEgress(Network* network, Flow flow){

	//unordered_set<Flow> congestedFlowSet;	// used to verify my claim
	unordered_set<string> congestedPortSet;

	int num_congestion = 0;

	unordered_map< uint64_t, Flow >::iterator itr;

	for(itr = network->flowMap.begin(); itr != network->flowMap.end(); itr++){

		Flow f = itr->second;
		
		FlowSegment *lastSegment = f.flowPath.back();

		f.egressRate = lastSegment->in_rate;
		//fprintf(stdout, "egressRate = %lf\n", f.egressRate);

		if(f.egressRate < f.ingressRate){
			num_congestion++;
			//congestedFlowSet.insert(f);

			// now trace this flow, see which port is congested
			for(vector<FlowSegment *>::const_iterator it = f.flowPath.begin(); it != --f.flowPath.end(); it++){
				FlowSegment *fs = *it;
				FlowSegment *nextFS = *next(it);
				if(nextFS->in_rate < fs->in_rate){
					congestedPortSet.insert(fs->destIPAddress);
				}
				//fprintf(stdout, "current fs has destination %s, and incoming rate %lf\n", fs->destIPAddress.c_str(), fs->in_rate);
				//fprintf(stdout, "next fs has destination %s, and incoming rate %lf\n", nextFS->destIPAddress.c_str(), nextFS->in_rate);
			}

		}
	}

	this->numCongestedPorts = congestedPortSet.size();

	return num_congestion;
}

void Simulation::printAllPort() {

	map< PortState, std::string > portEnumMap;
	
	portEnumMap[resolved] = "resolved";
	portEnumMap[transparent] = "transparent";
	portEnumMap[unresolved] = "unresolved";

	map< FlowType, std::string > flowEnumMap;
	flowEnumMap[settled] = "settled";
	flowEnumMap[unsettled] = "unsettled";
	flowEnumMap[bounded] = "bounded";


	fprintf(stdout, "----------------------print all ports-----------------------\n");

	//list< PortTuple >::const_iterator itr1;
	
	//for(itr1 = this->portList.begin(); itr1 != this->portList.end(); itr1++)
	
	for(unordered_map<string, PortVolumnState *>::iterator it = this->portMap.begin(); it != this->portMap.end(); it++)
	{
//		PortTuple pt = *itr1;
//		const string ip = pt.portIP;
//		const PortVolumnState *pvs = pt.pvs;

		const string ip = it->first;
		const PortVolumnState *pvs = it->second;
		const unordered_set< FlowSegment *> segments = pvs->in_segment;

		PortState ps = pvs->portState;
		string s = portEnumMap[ps];
		fprintf(stdout, "port: switch=%s bandwidth=%lfkb/s. It's state is %s\n", ip.c_str(), pvs->bandwidth, s.c_str());

		if(segments.empty()){
			fprintf(stdout, "WTF, it's empty!!!\n");
			
		}
		
		for(unordered_set< FlowSegment* >::const_iterator it = segments.begin(); it != segments.end(); it++){

			FlowSegment *fs = *it;
			FlowType ft = fs->type;
			string str = flowEnumMap[ft];

			fprintf(stdout, "	segment: ");
			fprintf(stdout, "flowID = %ld sourceIP = %s, destIP = %s, state = %s rate = %lf\n", 
								fs->myFlowID, fs->sourceIPAddress.c_str(), fs->destIPAddress.c_str(), str.c_str(), fs->in_rate);	

		}
	}
}

void Simulation::printDependenceGraph(Network* network, int size){

	if(size > 0){

		unordered_set<uint64_t> flowInvolved;
		map< PortState, std::string > portEnumMap;
		
		portEnumMap[resolved] = "resolved";
		portEnumMap[transparent] = "transparent";
		portEnumMap[unresolved] = "unresolved";

		map< FlowType, std::string > flowEnumMap;
		flowEnumMap[settled] = "settled";
		flowEnumMap[unsettled] = "unsettled";
		flowEnumMap[bounded] = "bounded";

		fprintf(stdout, "----------------print dependence graph----------------------------------\n");

		unordered_set< DependenceGraphNode * >::const_iterator it;
		for(it = dependenceGraph.begin(); it != dependenceGraph.end(); it++){
			
			DependenceGraphNode *node = *it;
			string ip = node->myNode;

			PortVolumnState *pvs = NULL;
/*			
			for(list<PortTuple>::iterator it = this->portList.begin(); it != this->portList.end(); it++){
				if( (*it).portIP == ip ){
					pvs = (*it).pvs;
					break;
				}
			}
*/

			for(unordered_map<string, PortVolumnState *>::iterator it = this->portMap.begin(); it != this->portMap.end(); it++){
				if( it->first == ip){
					pvs = it->second;
					break;
				}
			}

			if(pvs == NULL){
				fprintf(stdout, "ERROR: ip doesn't exist!!!!\n");
			}
			
			PortState ps = pvs->portState;
			string s = portEnumMap[ps];
			fprintf(stdout, "Node: Port deviceID = %s, is %s\n", ip.c_str(), s.c_str());

			list< string >::const_iterator it2;

			for(it2 = node->directedNodes.begin(); it2 != node->directedNodes.end(); it2++){
				string edgeIP = *it2;
				fprintf(stdout, "	Edges: Port deviceID = %s ", edgeIP.c_str());
			}
			fprintf(stdout, "\n");

		}

		fprintf(stdout, "\n");

		fprintf(stdout, "corresponding critical segments\n");

		for(set< FlowSegment *>::const_iterator iter3 = criticalSegments.begin(); iter3 != criticalSegments.end(); iter3++){

			FlowSegment *fs = *iter3;
			fprintf(stdout, "	criticalSegment: flow id = %ld, segment id = %ld\n", fs->myFlowID, fs->segmentID);

			if(flowInvolved.find(fs->myFlowID) == flowInvolved.end()){
				flowInvolved.insert(fs->myFlowID);

			}
		}

		for(map< FlowSegment*, FlowSegment *>::iterator iter4 = causalRelation.begin(); iter4 != causalRelation.end(); iter4++){

			FlowSegment *caused = iter4->first;
			FlowSegment *causing = iter4->second;

			string ip = causing->destIPAddress;

			fprintf(stdout, "segment A is caused by segment B, A: flow ID = %ld, segment ID = %ld; B: flow ID = %ld, segment ID = %ld\n"
							, caused->myFlowID, caused->segmentID, causing->myFlowID, causing->segmentID);
			fprintf(stdout, "segment B is on the port %s\n", ip.c_str());
		}


	///////////////////////////////////////
		fprintf(stdout, "flows involved in the dependence graph: \n");

		unordered_set<uint64_t>::const_iterator iter3;
		for(iter3 = flowInvolved.begin(); iter3 != flowInvolved.end(); iter3++){
			
			uint64_t flowID = *(iter3);
			fprintf(stdout, "-------flow id = %ld\n", flowID);
			Flow flow = network->flowMap[flowID];
			fprintf(stdout, "Flow information: \n");
			fprintf(stdout, "Flow ID = %ld, ingerss rate = %lf, egress rate = %lf\n", flow.flowID, flow.ingressRate, flow.egressRate);
			fprintf(stdout, "segments: \n");
			vector<FlowSegment *> vec = flow.flowPath;
			for(unsigned int i = 0; i < vec.size(); i++){
				FlowSegment *fs = vec[i];
				fprintf(stdout, "segemnt ID = %ld, type = %d\n", fs->segmentID, fs->type);
				fprintf(stdout, "%s -> %s\n", fs->sourceIPAddress.c_str(), fs->destIPAddress.c_str());
			}
		}

		fprintf(stdout, "==========end printing =======================\n");
	}
}

/*
void Simulation::printPortList(){
	fprintf(stdout, "Now print the port list\n");
	list<PortTuple>::const_iterator it;
	for(it = this->portList.begin(); it != this->portList.end(); it++){
		PortTuple pt = *it;
		PortVolumnState *pvs = pt.pvs;

		fprintf(stdout, "Device %s's port volume state: \n", pt.portIP.c_str());
		pvs->printMyself();
	}
}
*/







