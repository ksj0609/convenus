#include <sys/types.h>
#include <unistd.h>
#include <cstdio>
#include <unordered_map>
#include <map>
#include <set>
#include <list>
#include <stack>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <cstring>
#include <string>
#include <signal.h>
#include <stdint.h>
#include "Simulation.h"
#include "../veriflow-util/StringTokenizer.h"
#include "Network.h"
#include "PathGenerator.h"
#include "Flow.h"

using namespace std;

PathGenerator::PathGenerator(Network *network){
	this->myNetwork = network;
}

void PathGenerator::printMyNetwork(){
	this->myNetwork->print();
}

void PathGenerator::printMyNetworkFlows(){

	this->myNetwork->printFlows();
}

bool PathGenerator::constructFlow(uint64_t flowID, double ingressRate, string source, string dest){

	if(myNetwork->getDeviceMap().find(source) == myNetwork->getDeviceMap().end()){
		fprintf(stdout, "ERROR: source doesn't exist in my network!\n");
		return false;
	}

	if(myNetwork->getDeviceMap().find(dest) == myNetwork->getDeviceMap().end()){
		fprintf(stdout, "ERROR: source doesn't exist in my network!\n");
		return false;
	}

	if(source == dest){
		fprintf(stdout, "ERROR: no path to itself!\n");
		return false;
	}

	unordered_map<string, bool> *visitedStates = new unordered_map<string, bool>();
	unordered_map<string, ForwardingDevice> deviceMap = this->myNetwork->getDeviceMap();
	
	unordered_map<string, ForwardingDevice>::iterator iter;
	for(iter = deviceMap.begin(); iter != deviceMap.end(); iter++){
		string deviceAddress = iter->first;
		(*visitedStates)[deviceAddress] = false;
	}
	
	stack<PortIdentifier> *path = new stack<PortIdentifier>();
	bool found = runDFS(source, dest, visitedStates, path);

	
	if(!found){
		fprintf(stdout, "ERROR: There is No path\n");
		return false;
	}else{

		fprintf(stdout, "There is a path: \n");

		this->myNetwork->addFlow(flowID, ingressRate, 0.0);
		uint64_t segmentID = 0;
		
		// add the first segment, assume at least 3 PID included in path: h->s->h
		PortIdentifier previousPID = path->top();
		path->pop();
		PortIdentifier currentPID = path->top();
		this->myNetwork->addFlowSegment(flowID, segmentID, previousPID, currentPID, ingressRate, settled);
		previousPID = currentPID;
		path->pop();
		segmentID++;

		while(!path->empty()){

			currentPID = path->top();
			fprintf(stdout, "<%s,%d > -> ", currentPID.deviceIP.c_str(), currentPID.portID);
			this->myNetwork->addFlowSegment(flowID, segmentID, previousPID, currentPID, -1.0, unsettled);
			previousPID = currentPID;
			segmentID++;
			path->pop();
		}
		fprintf(stdout, "\n");
		return true;
	}
}

bool PathGenerator::runDFS(string source, string dest, unordered_map<string, bool> *states, stack<PortIdentifier> *path){

	bool found = false;

	if(states->find(source) == states->end()){
		fprintf(stdout, "ERROR there is no %s\n", source.c_str());
		return false;
	}

	(*states)[source] = true;

	ForwardingDevice fd = (this->myNetwork->getDeviceMap())[source];
	
	unordered_map< unsigned int, string >::iterator iter;
	for(iter = fd.portToNextHopIpAddressMap.begin(); iter != fd.portToNextHopIpAddressMap.end(); iter++){
		
		unsigned int port = iter->first;
		string nextHopIP = iter->second;
		ForwardingDevice nextFD = (this->myNetwork->getDeviceMap())[nextHopIP];
		
		if(nextFD.endDevice){	
			if(nextFD.ipAddress == dest){

				PortIdentifier nextPID;
				nextPID.deviceIP = nextFD.ipAddress;
				nextPID.portID = 0;
				path->push(nextPID);

				PortIdentifier pid;
				pid.deviceIP = source;
				pid.portID = port;
				path->push(pid);
				return true;
			}
			continue;
		}

		if((*states)[nextHopIP] == false){
			found = runDFS(nextHopIP, dest, states, path);
		}

		if(found){
			PortIdentifier pid;
			pid.deviceIP = source;
			pid.portID = port;
			path->push(pid);
			return true;
		}
	}
		
	return false;
}