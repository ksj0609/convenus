
#ifndef STRINGTOKENIZER_H_
#define STRINGTOKENIZER_H_

#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <vector>

using namespace std;

/** An auxiliary tool to parse the configuration file of the network topology.
*
*/
class StringTokenizer
{
private:
	string str, delim;				/**< Delimiter*/
	vector<string> tokens;			/**< List to tokens parsed*/

public:
	StringTokenizer(const string& s, const string& d);	/**< Constructor*/
	int countTokens() const;							/**< Count the number of tokens generated*/
	bool hasMoreTokens() const;							/**< Check whether the string has more tokens*/
	string nextToken();									/**< Get the next token*/
	string toString() const;							/**< Print the string and delimiter*/
};

#endif /* STRINGTOKENIZER_H_ */
