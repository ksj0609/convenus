import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import csv
import numpy as np

x = []	# number of congestions of each experiment
y = []	# number of dependency graph cases of each experiment


for i in range(10):
	filePath = '../numCongestion_'+str(i)+'.txt'
	data = np.genfromtxt(filePath, delimiter = ' ')
	num_congestion = data[-1][1]
	x.append(num_congestion)

for i in range(10):
	filePath = '../numDependencyGraph_'+str(i)+'.txt'
	data = np.genfromtxt(filePath, delimiter = ' ')
	#num_congestion = len(data) - 250
	#x.append(num_congestion)
	num_dependency = data[-1][1]
	y.append(num_dependency)

print x
print y

mean_num_congestion = np.mean(x)
var_num_congestion = np.std(x)

mean_num_dependency = np.mean(y)
var_num_dependency = np.std(y)

print "This is for continuous udpate==========================================================="
print "avg number of congestions = %f, with variance = %f" %(mean_num_congestion, var_num_congestion)
print "avg number of dependency = %f, with variance = %f" %(mean_num_dependency, var_num_dependency)