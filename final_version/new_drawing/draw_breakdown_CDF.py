import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import csv
import numpy as np

x = []	# verification number
z = []	# phase 1
a = []	# phase 2
b = []	# phase 3
c = []	# phase 4
d = []	# total

for i in range(10):
	filePath = '../simulationBreakdown_'+str(i)+'.txt'
	with open(filePath,'r') as csvfile:
		plots = csv.reader(csvfile, delimiter=' ')
		for row in plots:
			x.append(int(row[0]))
			z.append(float(row[1]))
			a.append(float(row[2]))
			b.append(float(row[3]))
			c.append(float(row[4]))
			d.append(float(row[5]))

sorted_data_2 = np.sort(z)
yvals_2 = np.arange(len(sorted_data_2))/float(len(sorted_data_2))
sorted_data_3 = np.sort(a)
yvals_3 = np.arange(len(sorted_data_3))/float(len(sorted_data_3))
sorted_data_4 = np.sort(b)
yvals_4 = np.arange(len(sorted_data_4))/float(len(sorted_data_4))
sorted_data_5 = np.sort(c)
yvals_5 = np.arange(len(sorted_data_5))/float(len(sorted_data_5))
sorted_data_6 = np.sort(d)
yvals_6 = np.arange(len(sorted_data_6))/float(len(sorted_data_6))

matplotlib.rcParams.update({'font.size': 18})

plt.plot(sorted_data_2, yvals_2, label='Phase-I', linestyle='dashed', color='blue')
plt.plot(sorted_data_3, yvals_3, label='Phase-II', linestyle='dashdot', color='black')
plt.plot(sorted_data_4, yvals_4, label='Phase-III', linestyle='-', dashes=[1, 1], color='purple')
plt.plot(sorted_data_5, yvals_5, label='Phase-IV', linestyle='dotted', color='green')
plt.plot(sorted_data_6, yvals_6, label='Total Time', linestyle='solid', color='red')

plt.gca().xaxis.set_major_formatter(FormatStrFormatter('%d'))
plt.xlabel('Time duration (ms)')
plt.ylabel('Fraction of time')


plt.legend(loc='lower right')

#plt.show()
plt.savefig('Simulation_Breakdown_CDF.pdf')