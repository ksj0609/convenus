import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import csv
import numpy as np

x = []
y = []
z = []
w = []


for i in range(10):
	filePath = '../timeStats_'+str(i)+'.txt'
	with open(filePath,'r') as csvfile:
		plots = csv.reader(csvfile, delimiter=' ')
		for row in plots:
			x.append(int(row[0]))
			y.append(float(row[1]))
			z.append(float(row[2]))
			w.append(float(row[3]))

sorted_data_1 = np.sort(y)
yvals_1 = np.arange(len(sorted_data_1))/float(len(sorted_data_1))
sorted_data_2 = np.sort(z)
yvals_2 = np.arange(len(sorted_data_2))/float(len(sorted_data_2))
sorted_data_3 = np.sort(w)
yvals_3 = np.arange(len(sorted_data_3))/float(len(sorted_data_3))

matplotlib.rcParams.update({'font.size': 18})

plt.plot(sorted_data_1, yvals_1, label='Subgraph Time', linestyle='dashdot', color='black')
plt.plot(sorted_data_2, yvals_2, label='Simulation Time', linestyle='dashed', color='purple')
plt.plot(sorted_data_3, yvals_3, label='Total Time', linestyle='solid', color='blue')

plt.gca().xaxis.set_major_formatter(FormatStrFormatter('%d'))
plt.xlabel('Time duration (ms)')
plt.ylabel('Fraction of trials')

plt.legend(loc='lower right')


#plt.show()
plt.savefig('time_stats_CDF.pdf')