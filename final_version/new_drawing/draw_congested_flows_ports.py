import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import csv
import numpy as np

# use csv
x = []	# verification index
y = []	# congested ports
z = []	# congested flows

with open('../congestedFlows_0.txt','r') as csvfile:
	plots = csv.reader(csvfile, delimiter=' ')
	for row in plots:
		x.append(int(row[0]))
		y.append(int(row[1]))	# ports
		z.append(int(row[2]))   # flows

matplotlib.rcParams.update({'font.size': 18})





f1 = plt.figure(1).gca()
#f, axes = plt.subplots(2, 1)


#plt.yticks(np.arange(min(y), max(y), 1))


'''
yint=[0,1,2,3,4]
locs, labels = plt.yticks()
for each in locs:
	yint.append(int(each))
plt.yticks(yint)
'''

"""
axes[0].plot(x, y, color='green')
axes[0].set_ylabel('# of congested ports', size=15)

axes[1].plot(x, z, color='blue')
axes[1].set_ylabel('# of congested flows', size=15)
axes[1].set_xlabel('Flow update arrival index')
#plt.xlabel('verification index')
"""

ax1 = plt.subplot(211)
plt.yticks(np.arange(min(y), max(y)+1, 1))
plt.plot(x, y, color='green')
ax1.set_ylabel('# of congested ports', size=15)

ax2 = plt.subplot(212)
plt.yticks()
plt.plot(x, z, color='blue')
ax2.set_ylabel('# of congested flows', size=15)
ax2.set_xlabel('Flow update arrival index')



#plt.show()
plt.savefig('congested_flows.pdf')