
#ifndef FORWARDINGDEVICE_H_
#define FORWARDINGDEVICE_H_

#include <sys/types.h>
#include <unistd.h>
#include <stdint.h>
#include <string>
#include <unordered_map>
#include <unordered_set>

using namespace std;

/** Model of a forwarding device in the network.
*   A forwarding device contains a set of ourgoing links,
*	each one connects a neighbor device. This device has 
*	a global bandwidth that shared by all the links.
*/
struct ForwardingDevice
{
public:
	uint64_t id;		/**< The ID of the forwarding device*/
	string ipAddress;	/**< The ip of the forwarding device*/
	bool endDevice;		/**< A indicator of thether this device is an end device, meaning it connects to an end-host*/

	unordered_set< string > nextHops; /**< A set of output ports, each connects to a next-hop device*/
	double outPortBandwidth;		  /**< The bandwidth of this device, shared by all the output ports*/
};

#endif /* FORWARDINGDEVICE_H_ */
