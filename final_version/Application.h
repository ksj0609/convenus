#ifndef APPLICATION_H_
#define APPLICATION_H_

#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <map>
#include <unordered_map>
#include <set>
#include <list>
#include <stack>
#include "Network.h"
#include "PortVolumnState.h"
#include "Flow.h"

using namespace std;

/** Application model to issue random flows, for evaluation purpose only.
*   The "fake" application is only responsible to add / delete randomized
*   flows to / from the network. Each flow generated has random source / destination
*	end-hosts, and random ingress rate, with a shortest-path route.
*/
class Application
{
private:
	Network myNetwork;					/**< The network that flows are inserted to / deleted from*/
	uint64_t flowIDCounter;				/**< Count the number of existing flows in the network*/
	unordered_set<uint64_t> flowIDSet;	/**< The set of flows which is dynamically updated*/
	vector<string> endDevices;			/**< A set of end-hosts, each one represented by its address*/
public:
	Application(Network& network);											/**< Constructor*/
	Flow issueFlow(string soruce, string dest);								/**< Insert a random (rate) flow, given source and destination*/
	Flow issueFlow();														/**< Insert a flow with random source / destination, and random rate*/
	Flow issueFlowDeletion();												/**< Delete a random flow that exists in the network*/

	void addFlowID(uint64_t flowID){ this->flowIDSet.insert(flowID); }		/**< Insert a new flow's ID into flowIDSet*/
	void deleteFlowID(uint64_t flowID){ this->flowIDSet.erase(flowID); }	/**< Delete a flow's ID from the flowIDSet*/
	size_t getIDSetSize(){ return this->flowIDSet.size(); }					/**< Get the number of flow ID's in the flowIDSet*/

	bool generateBFSPath(string source, string dest, stack<string> *path);	/**< Generate the shortest route of a flow, using BFS search*/
	bool generateDFSPath(string source, string dest, stack<string> *path);	/**< Generate the shortest route of a flow, using DFS search*/
	bool runDFS(string source, string dest, unordered_map<string, bool> *states, stack<string> *path); /**< Trigger the DFS recursive function, which is a part of DFS search algorithm*/
};

#endif /* APPLICATION_H_ */