
#include <sys/types.h>
#include <unistd.h>
#include <cstdio>
#include <string>
#include <unordered_map>
#include <map>
#include "Network.h"
#include "ForwardingDevice.h"

using namespace std;

bool Network::addDevice(uint64_t id, const string& ipAddress, bool endDevice, double bandwidth)
{
	if(this->isDevicePresent(ipAddress) == false)
	{
		ForwardingDevice device;
		device.id = id;
		device.ipAddress = ipAddress;
		device.endDevice = endDevice;
		device.outPortBandwidth = bandwidth;
		
		this->deviceMap[ipAddress] = device;
		this->idToIpAddressMap[id] = ipAddress;

		return true;
	}
	else
	{
		return false;
	}
}

bool Network::addPort(const string& ipAddress, const string& nextHopIpAddress)
{
	if(this->isDevicePresent(ipAddress) == false)
	{
		return false;
	}
	else
	{
		this->deviceMap[ipAddress].nextHops.insert(nextHopIpAddress);
		//this->deviceMap[ipAddress].portToNextHopIpAddressMap[port] = nextHopIpAddress;
		//this->deviceMap[ipAddress].portToBandwidthMap[port] = bandwidth;
		return true;
	}
}

bool Network::isEndDevice(const string& ipAddress)
{
	if(this->isDevicePresent(ipAddress) == false)
	{
		return false;
	}
	else
	{
		return (this->deviceMap[ipAddress].endDevice == true);
	}
}

bool Network::isDevicePresent(const string& ipAddress) const
{
	if(this->deviceMap.find(ipAddress) == this->deviceMap.end())
	{
		return false;
	}
	else
	{
		return true;
	}
}

string Network::getDeviceIpAddress(uint64_t id)
{
	if(this->idToIpAddressMap.find(id) == this->idToIpAddressMap.end())
	{
		return "NULL";
	}
	else
	{
		return this->idToIpAddressMap[id];
	}
}


void Network::print() const
{
	unordered_map< string, ForwardingDevice >::const_iterator itr1;
	for(itr1 = this->deviceMap.begin(); itr1 != this->deviceMap.end(); itr1++)
	{
		const ForwardingDevice& device = itr1->second;
		fprintf(stdout, "id %lu ipAddress %s endDevice %d bandwidth %lf\n", device.id, device.ipAddress.c_str(), device.endDevice, device.outPortBandwidth);
		fprintf(stdout, "	next hops: ");

		unordered_set< string >::const_iterator itr2;
		for(itr2 = device.nextHops.begin(); itr2!= device.nextHops.end(); itr2++){
			fprintf(stdout, "%s, ", (*itr2).c_str());
		}
		fprintf(stdout, "\n");		
	}
}

//////////////////////////////////////// new functions ///////////////////////////////////////
bool Network::addDevice(ForwardingDevice device){
	string IP = device.ipAddress;
	if(this->deviceMap.find(IP) != this->deviceMap.end()){
		// device already exists
		return false;
	}else{
		this->deviceMap[IP] = device;
		this->idToIpAddressMap[device.id] = IP;
		return true;
	}
}

bool Network::addFlow(uint64_t id, double ingress, double egress){

	Flow flow = Flow();
	flow.flowID = id;
	flow.ingressRate = ingress;
	this->flowMap[id] = flow;

	return true;
}

bool Network::addFlow(Flow flow){

	if(this->flowMap.find(flow.flowID) != this->flowMap.end()){

		fprintf(stdout, "ERROR: existing flow id\n");
		return false;
	}else{

/*
		Flow *newFlow = new Flow();
		newFlow.flowID = flow.flowID;
		newFlow->ingressRate = flow.ingressRate;
		newFlow->egressRate = flow.egressRate;
		newFlow->flowPath = flow.flowPath;
*/
		this->flowMap[flow.flowID] = flow;
		
		return true;
	}
}
/*
bool Network::addFlowSegment(uint64_t id, uint64_t sid, string sDevice, unsigned int sPort, string dDevice, unsigned int dPort, double v, FlowType t){

	if(this->flowMap.find(id) == this->flowMap.end()){	// which means the map doesn't contain such id
		fprintf(stdout, "ERROR: the map doesn't have such flow id\n");
		return false;
	}else{
		FlowSegment *fs = new FlowSegment();
		fs->myFlowID = id;
		fs->segmentID = sid;

		//
		PortIdentifier sourcePID;
		sourcePID.deviceIP = sDevice;
		sourcePID.portID = sPort;
		fs->sourcePort = sourcePID;

		PortIdentifier destPID;
		destPID.deviceIP = dDevice;
		destPID.portID = dPort;
		fs->destPort = destPID;
		
		fs->in_rate = v;
		fs->type = t;
		this->flowMap[id]->flowPath.push_back(fs);
		return true;
	}
}

bool Network::addFlowSegment(uint64_t id, uint64_t sid, PortIdentifier sourcePID, PortIdentifier destPID, double v, FlowType t){

	if(this->flowMap.find(id) == this->flowMap.end()){	// which means the map doesn't contain such id
		fprintf(stdout, "ERROR: the map doesn't have such flow id\n");
		return false;
	}else{
		FlowSegment *fs = new FlowSegment();
		fs->myFlowID = id;
		fs->segmentID = sid;
		fs->sourcePort = sourcePID;
		fs->destPort = destPID;
		fs->in_rate = v;
		fs->type = t;
		this->flowMap[id]->flowPath.push_back(fs);
		return true;
	}
}
*/
bool Network::deleteFlow(uint64_t flowID){

	unordered_map<uint64_t, Flow>::iterator iter = this->flowMap.find(flowID);
	if(iter == this->flowMap.end()){
		return false;
	}else{
		//delete iter->second;

		vector<FlowSegment *>::iterator iter2;
		for(iter2 = iter->second.flowPath.begin(); iter2 != iter->second.flowPath.end(); iter2++){
			delete (*iter2);
		}
		this->flowMap.erase(iter);
		return true;
	}
}

bool Network::flowExists(uint64_t flowID){

	if(this->flowMap.find(flowID) == this->flowMap.end()){
		return false;	// does not exist
	}else{
		return true;
	}
}

Flow Network::getFlow(uint64_t flowID){

	unordered_map<uint64_t, Flow>::iterator iter = this->flowMap.find(flowID);

	if(iter == this->flowMap.end()){
		fprintf(stdout, "Didn't find this flow with id = %ld\n", flowID);
	}
	Flow flow = iter->second;
	return flow;
}

void Network::printFlows() const
{
	unordered_map< uint64_t, Flow >::const_iterator itr1;
	
	for(itr1 = this->flowMap.begin(); itr1 != this->flowMap.end(); itr1++)
	{
		Flow flow = itr1->second;
		flow.printMyself();
		fprintf(stdout, "\n");
	}
}

