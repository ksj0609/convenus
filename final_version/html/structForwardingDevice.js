var structForwardingDevice =
[
    [ "endDevice", "structForwardingDevice.html#a331e415eb8d0d77a3542bf3a1c9626f7", null ],
    [ "id", "structForwardingDevice.html#a8299b8b418cc504eab48e0fb480c3d98", null ],
    [ "ipAddress", "structForwardingDevice.html#ad86b46318af1514761d26be73f5e4faa", null ],
    [ "nextHops", "structForwardingDevice.html#a935099d13f7e6562708913c9b2861b58", null ],
    [ "outPortBandwidth", "structForwardingDevice.html#a1007450b4756e90f3bb8b990516c83bd", null ]
];