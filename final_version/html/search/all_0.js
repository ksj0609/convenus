var searchData=
[
  ['adddevice',['addDevice',['../classNetwork.html#a87ecd36d6d19f7f65e4a66cd848ede6b',1,'Network::addDevice(ForwardingDevice device)'],['../classNetwork.html#adcdac677a0fdad50318a6963caf0b9cb',1,'Network::addDevice(uint64_t id, const string &amp;ipAddress, bool endDevice, double bandwidth)']]],
  ['addflow',['addFlow',['../classNetwork.html#a96a403680346655a3f27331118c544d9',1,'Network::addFlow(Flow flow)'],['../classNetwork.html#a211534adbd1bd88889abcf50485cb858',1,'Network::addFlow(uint64_t id, double ingress, double egress)']]],
  ['addflowid',['addFlowID',['../classApplication.html#aa7edfae208ce392564fdd74e6a31d848',1,'Application']]],
  ['addnewflow',['addNewFlow',['../classVeriCongestion.html#a1ab22a70a16a1c5cc7d135bda3eb66a4',1,'VeriCongestion']]],
  ['addport',['addPort',['../classNetwork.html#add4c1f132f5df55c0183a6ec2fa3a027',1,'Network']]],
  ['application',['Application',['../classApplication.html',1,'Application'],['../classApplication.html#abdfd49a30496ccecea58e779d3956764',1,'Application::Application()']]],
  ['application_2ecpp',['Application.cpp',['../Application_8cpp.html',1,'']]],
  ['application_2eh',['Application.h',['../Application_8h.html',1,'']]],
  ['avg_5faffectedflows',['avg_affectedFlows',['../classVeriCongestion.html#a597019259c694c7de956351c2d81e6cd',1,'VeriCongestion']]]
];
