var indexSectionsWithContent =
{
  0: "abcdefghimnoprstuvw",
  1: "adfhmnpsv",
  2: "s",
  3: "afmnpsv",
  4: "acdfghimnoprstv",
  5: "abcdefimnopstw",
  6: "fp",
  7: "brstu",
  8: "s",
  9: "m"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "related",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Friends",
  9: "Pages"
};

