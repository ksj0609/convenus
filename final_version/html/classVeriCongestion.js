var classVeriCongestion =
[
    [ "VeriCongestion", "classVeriCongestion.html#a8c9347ae9a573991110feb779dfa38dc", null ],
    [ "addNewFlow", "classVeriCongestion.html#a1ab22a70a16a1c5cc7d135bda3eb66a4", null ],
    [ "deleteFlow", "classVeriCongestion.html#a65a32cf8658bae94d2e52795513155c9", null ],
    [ "generateRelevantNetwork", "classVeriCongestion.html#a5a5a77797bdc8f28b5056b3921877ebd", null ],
    [ "printFlows", "classVeriCongestion.html#a5231bab0c2b510391df05262e45f8c41", null ],
    [ "printPortShare", "classVeriCongestion.html#ae76fc5eeb228f37beda15a56b55831cb", null ],
    [ "printStats", "classVeriCongestion.html#a649eca29c6ba334e4c81ec45f7e39aab", null ],
    [ "simulate", "classVeriCongestion.html#a6ba7cf1eb9b7bd44389c171b1f678054", null ],
    [ "verifyAFlow", "classVeriCongestion.html#a6602fcd5ce58aaedb247c839c0155938", null ],
    [ "avg_affectedFlows", "classVeriCongestion.html#a597019259c694c7de956351c2d81e6cd", null ],
    [ "current_graph_size", "classVeriCongestion.html#ac69b374817840583ff2bc66f2831a732", null ],
    [ "num_congestion", "classVeriCongestion.html#a699c9cbdd494d2b4602704c7b1e14c43", null ],
    [ "num_dependencyGraph", "classVeriCongestion.html#a10399e8e8b8bbfefb139fe89ddbe8798", null ],
    [ "num_verification", "classVeriCongestion.html#a34ee991d590f05fe74cc2da47a1558a9", null ],
    [ "portShare", "classVeriCongestion.html#a7427e3ee4ff0fd2b7bda6757196f9db8", null ],
    [ "total_find_affected_time", "classVeriCongestion.html#ae12b5adabb58dadbb3e0598879b979c0", null ],
    [ "total_generate_subnetwork_time", "classVeriCongestion.html#abf2d7c0cde4161e4c74d286298b595e1", null ],
    [ "total_simulation_time", "classVeriCongestion.html#a8d07553ff57d943445fcc30a634d8bb9", null ],
    [ "wholeNetwork", "classVeriCongestion.html#a4dd1f464bce867fadb5d089a1e510b32", null ]
];