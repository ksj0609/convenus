#ifndef PORTVOLUMNSTATE_H_
#define PORTVOLUMNSTATE_H_

#include <sys/types.h>
#include <unistd.h>
#include <stdint.h>
#include <string>
#include <unordered_set>
#include <vector>
#include <map>
#include "Flow.h"

using namespace std;

enum PortState {resolved, transparent, unresolved};

/** A data structure that represents the information of a port.
*	The information of a port including the bandwidth of the port,
*	the state of the port, and the set of incoming FlowSegment to
*	this port.
*/
struct PortVolumnState{
public:
	PortState portState;						/**< The state of the port, it can be resolved, transparent, or unresolved*/
	double bandwidth;							/**< The bandwidth of the port*/
	unordered_set< FlowSegment *> in_segment;	/**< The set of incoming flow segments to this port*/

	void printMyself()	/**< Print the information of the port*/
	{
		fprintf(stdout, "bandwidth = %lf, port state = %d\n", this->bandwidth, this->portState);
		fprintf(stdout, "segments\n");
		for(unordered_set<FlowSegment *>::const_iterator it = this->in_segment.begin(); it != this->in_segment.end(); it++){
			FlowSegment *fs = *it;
			fs->printMyself();
		}
	}	
};

#endif /* PORTVOLUMNSTATE_H_ */