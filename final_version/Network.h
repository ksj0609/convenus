#ifndef NETWORK_H_
#define NETWORK_H_

#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <unordered_map>
#include "ForwardingDevice.h"
#include "Flow.h"

using namespace std;

/** Model of the network. The network model is consists of a set of 
*   forwarding devices and a set of network flows in the network.  
*/  

class Network{

friend class Simulation;
private:
	unordered_map< string, ForwardingDevice > deviceMap; /**< A HashMap that maps a switch address to a ForwardingDevice object*/
	unordered_map< uint64_t, string > idToIpAddressMap;  /**< A HashMap that maps a ForwardingDevice's ID to its IP*/
	unordered_map< uint64_t, Flow > flowMap;			 /**< A HashMap that maps a flow ID to a Flow object*/

public:
	unordered_map< string, ForwardingDevice > getDeviceMap() { return this->deviceMap; } 	/**< return deviceMap */
	unordered_map< uint64_t, Flow > getFlowMap() { return this->flowMap; }					/**< return flowMap*/					
	void clearFlowMap() { this->flowMap.clear(); }											/**< empty the flowMap*/
	void setDeviceMap(unordered_map< string, ForwardingDevice > map){ this->deviceMap = map; } /**< set a HashMap to be deviceMap*/
	bool addDevice(ForwardingDevice device);												   /**< add a forwardingDevice object to the Network object*/
	bool addDevice(uint64_t id, const string& ipAddress, bool endDevice, double bandwidth);    /**< add a forwardingDevice object to the Network object*/		
	bool addPort(const string& ipAddress, const string& nextHopIpAddress);					   /**< add a port to a forardingDevice object, and assign a next-hop address to it*/		

	bool isEndDevice(const string& ipAddress);												/**< check whether a forardingDevice is an end device*/
	bool isDevicePresent(const string& ipAddress) const;									/**< check whether a forwardingDevice is already added to the Network object*/					
	bool isPortPresent(const string& ipAddress, unsigned int port);							/**< check whether a port is already added to a forwardingDevice object*/
	string getDeviceIpAddress(uint64_t id);													/**< return the IP of a forwardingDevice, given its the ID*/
	
	bool addFlow(Flow flow);																/**< add a Flow object to the Network object*/			
	bool addFlow(uint64_t id, double ingress, double egress);								/**< add a Flow object to the Network object*/
	bool deleteFlow(uint64_t flowID);														/**< delete a Flow object from the Network object*/
	bool flowExists(uint64_t flowID);														/**< check whether a flow is already added to the Network object*/
	Flow getFlow(uint64_t flowID);															/**< return a Flow object, given the ID of the flow*/
	void printFlows() const;																/**< print the information of the Flow object*/			

	void print() const;																		/**< print the infomation of the Network object*/		
};

#endif /* NETWORK_H_ */
