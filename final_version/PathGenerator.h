#ifndef PATHGENERATOR_H_
#define PATHGENERATOR_H_

#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <map>
#include <unordered_map>
#include <set>
#include <list>
#include <stack>
#include "Network.h"
#include "PortVolumnState.h"
#include "Flow.h"

using namespace std;

/** Generate the flow in the network, called by the Application.
*	
*/
class PathGenerator
{
private:
	Network *myNetwork;					/**< The network model needed to generate the shortest-path*/

public:
	PathGenerator(Network network);		/**< Constructor*/
	//void printMyNetwork();
	//void printMyNetworkFlows();
	bool constructFlow(uint64_t flowID, double ingressRate, string source, string dest);							/**< Construct a network flow*/
	bool runDFS(string source, string dest, unordered_map<string, bool> *states, stack<PortIdentifier> *path);		/**< Trigger the DFS search algorithm*/
};

#endif /* PATHGENERATOR_H_ */