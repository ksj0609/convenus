\section{SYSTEM OVERVIEW}

We develop a verification system, \name, to preserve the congestion-free property of the network. \name is a shim layer that resides between the SDN controller and the network layer as shown in Figure \ref{fig: rua}. \name intercepts the updates issued by the SDN controller, dynamically updates the network model, efficiently computes the new flow rates assume the new update is installed in the network, and performs congestion verification, i.e., whether each flow has the desired throughput and whether the aggregated flow rate at every network device exceeds the link bandwidth. Updates that pass the congestion verification are applied to the data plane, otherwise, \name reports the congestion issues to the network operators with the set of affected flows and estimated new flow rates.

\begin{figure}[!htb]
{
\centering
\includegraphics[width=0.7\textwidth]{system}
\caption{\name sits between the SDN controller and network devices to intercept and verify every flow update to preserve the congestion-free property. \label{fig: rua}}
}
\end{figure}

\name models the network devices as a set of connected output ports. We model each flow as a directed path from an ingress switch to an egress switch. We assume the ingress rates of the flows are known before verifying the updates. The flow input rates can be derived from application specification or acquired from the statistics collected at of the OpenFlow switch flow entries specified in \cite{openflow}. \name consists of three key components to perform the congestion verification upon receiving an update from the SDN controller. 

\begin{itemize}
\item Affected Flow Identification. We determine the smallest set of flows, whose rate will be potentially affected by the update.

\item Minimum Affected Network Graph Generation. We create a network graph consisting of the affected flows and ports identified in the previous module. Congestion may only occur in this subnetwork. The size of the subnetwork is often significantly reduced compared with the entire network, and thus greatly improve the verification speed. This is particularly useful for online verification. Our algorithm to identify the minimum affected network graph works under the assumption the current network is congestion-free before applying the new updates. 

\item Flow Rate Computation and Congestion Verification. We develop a four-phase simulation algorithm to quickly compute the rate of each flow (including the rates at all the intermediate ports along the flow path) in the minimum affected network graph generated by the previous module. The detailed flow rates are then used to determine whether the update will cause congestion and if so, at which portion of the network and by how much.

\end{itemize}

In this paper, we focus on the congestion-free property, but the design of \name intends to provide a generic framework for verifying other invariants. For example, it is straightforward to incorporate the reachability-based invariants\comment{, like loop-freedom and absence of blackhole} described in VeriFlow \cite{VeriFlow} into \name. We plan to explore other security policy and network invariants in the future work.
