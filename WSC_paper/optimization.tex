\section{SPEEDING UP CONGESTION VERIFICATION}
It is critical to perform congestion verification at high speed, because delaying the updates can damage the network state consistency and harm the real-time application requirements such as fast failover. To speed up the process, we investigate ways to reduce the problem space by identifying the minimum set of flows and ports that are affected by the flow update, and performing the verification describe in Section 4 only on the network model consisting of those elements. This approach also reduces the possibility to have circular dependence, which further increases the speed by skipping phase II to IV in the simulation algorithm. Based on the assumption that the existing network is congestion-free, we derive a set of theorems to generate the minimum affected network for different types of flow update, including flow removal, flow addition, and flow modification. An ongoing work is to develop an efficient graph search algorithm to speed up the verification in other scenarios that the congestion-free network assumption does not hold, e.g., one can tolerate short-term network congestion in order to achieve quick update installation.

%In this section, we are dealing with the verification efficiency of our tool. Since ConVenus is an online verification tool, which intercepts each flow update issued by the controller at real-time and determine if it is going to cause congestion in the underline network, there is timing constraints for our process of verification. 
%Note that this assumption is valid if the initial state of the network is congestion-free, because once ConVenus starts running, it will reject any updates that is going to cause congestion. 

\subsection{Flow Removal}
%We first deal with the situation that the incoming update is to remove an existing flow from the network. 
We claim that removing a flow from a congestion-free network neither causes any congestion nor changes the flow rate of any other flows in the network. 
\begin{theorem}\label{thm:first}
Given a congestion-free port, removing an input flow (or reducing its rate) neither makes the port congested nor changes the output rates of other flows sharing the same port.
\end{theorem}
\begin{proof}
Equation \ref{eq:FCFS} indicates $\lambda^{out}_{f,q} = \lambda^{in}_{f,q}$ for every flow $f$ at a congestion-free port $q$ (i.e., $\Lambda^{in}_{q} \leq \mu_q$). A flow removal or a flow rate reduction decreases $\Lambda^{in}_{q}$, and thus cannot cause congestion, and the rate of every other flow remains unchanged as $\lambda^{in}_{f,q}$ .
\end{proof}

A congestion-free network contains no congested ports according to Definition \ref{def:congestion}. Therefore, it is safe to forward any flow removal updates to the data plane and doing that will not change the rates of any existing flows in the network.
%Based on Theorem \ref{thm:first}, we can conclude that our claim is true, 

\subsection{Flow Insertion}
We first introduce the concepts of \textbf{affected flow set} and \textbf{minimum affected network}, and then describe the algorithm to construct them, and finally present a set of theorems to prove the correctness of the algorithm.

\begin{definition}
Given a newly inserted flow $f^{*}$, the affected flow set $A_{f^{*}}$ is the set of all the flows (including $f^{*}$) in the network whose rates may be changed due to the insertion.

\end{definition}\label{def:affected flow set}
\begin{definition}
%Given a newly inserted flow $f^{*}$, the minimum affected network $N_{f^{*}}$ is the network captures all the possible port congestion and the flow rate changes due to the insertion.
Given a newly inserted flow $f^{*}$, the minimum affected network $N_{f^{*}}$ is the network consisting of all the possible congested ports and the affected flow set due to the insertion.
\end{definition}\label{def:minimum affected network}

Algorithm \ref{alg: 1} illustrates how \name generates $A_{f^{*}}$ and $N_{f^{*}}$. Figure \ref{fig: network} presents an example of the input and output used in Algorithm \ref{alg: 1}.

\begin{algorithm}
	\SetKwInOut{Input}{Input}
    	\SetKwInOut{Output}{Output}
	\begin{multicols}{2}
    	\Input{A network $N$, and a flow $f^{*}$ to be inserted}
    	\Output{Affected flow set $A_{f^{*}}$ and minimum affected network $N_{f^{*}}$}
	Add $f^{*}$ into $A_{f^{*}}$ \\  	
   	\For{each flow $f$ in $N$}{ 
   		\If{$Q_{f} \cap Q_{f^*} \neq \varnothing$}
   			{add $f$ into $A_{f^{*}}$} 
   		}	 
   	Add all ports $q \in Q_{f^{*}}$ into $N_{f^{*}}$'s port set \\
	\For{each flow $f$ in $A_{f^{*}}$}{
		\For{each port $q \in Q_{f}$}{
			\If{$q \notin Q_{f^{*}}$}{
				/* include the ingress and egress ports of an affected flow */ \\
				\eIf{$q = q_1$ or $q = q_{|Q_{f}|}$}{
					add $q$ into $N_{f^{*}}$'s port set				
				}{
					/* Removing the unaffected ports in flow $f$ */ \\
					remove $q$ from $Q_{f}$\\
					remove $\lambda_{f,q}$ from $R_{f}$			
				}			
			}
		}	
	}
	Add all flow $f \in A_{f^{*}}$ into $N_{f^{*}}$'s flow set \\
	\Return $A_{f^{*}}, N_{f^{*}}$
	\end{multicols}
    	\caption{Generation of the affected flow set and the minimum affected network}
\label{alg: 1}
\end{algorithm}

\begin{figure}[!h]
{
\centering
\includegraphics[height=0.16\textheight]{generate_network}
\caption{A simple example for Algorithm \ref{alg: 1}: Consider a network $N$ with one existing flow $f_{1} = <(1,6,7,10,11,8,4), R_{f_{1}}>$ and one new flow $f_{2} = <(3,9,7,8,5), R_{f_{2}}>$. The minimum affected network $N_{f_{2}}$ contains $f_{2}$ and the modified $f_{1} = <(1,7,8,4), R_{f_{1}}>$, and the port set \{3, 9, 7, 8, 5, 1, 4\}. 
 \label{fig: network}}
}
\end{figure} 
%The following theorems are going to help prove the correctness of our algorithm, which is to show that the flows in $A_{f^{*}}$ contains all the flows that may have their flow rate changed, and the ports in $N_{f^{*}}$ contains all the ports may be congested and all the ports needed to capture any flow rate change.   

\begin{theorem}\label{thm:insert first}
Inserting a flow into a port $q$ makes the output rates of all the existing flows passing through $q$ either decrease or remain the same.
\end{theorem}
\begin{proof}
There are three possible situations based on equation \eqref{eq:FCFS}. Note that a new flow insertion to $q$ increases $\Lambda^{in}_{q}$. 
\begin{itemize}
	\item If $q$ is congestion-free before and after the flow insertion, then all the existing flows' output rates remain unchanged, i.e., $\lambda^{out}_{f,q} = \lambda^{in}_{f,q}$.
	\item If $q$ is congestion-free before the flow insertion and congested after the flow insertion, then all the existing flows' output rates decrease,  because $\lambda^{in}_{f,q} \times \frac{\mu_{q}}{\Lambda^{in}_{q}} < \lambda^{in}_{f,q}$.
	\item If $q$ is congested before and after the flow insertion, then all the existing flows' output rates decrease, because $\Lambda^{in}_{q}$ is increased.
\end{itemize}
\end{proof}

%Having the observation above, we can take one more step and derive the following theorem. 
\begin{theorem}\label{thm:insert second}
When a flow $f^{*}$ is inserted into a congestion-free network, the possibly congested ports are $q \in Q_{f^{*}}$.
\end{theorem}
\begin{proof}
We prove by contradiction. We assume that, after inserting $f^{*}$, there exists a congested port $\hat{q} \notin Q_{f}$, i.e., $ \mu_{\hat{q}} < \Lambda^{in}_{\hat{q}}$. Therefore, at least one flow $\hat{f}$ ($\hat{f} \neq f^{*}$ because of the definition of $ Q_{f^{*}}$) passing through $\hat{q}$ increases the input rate. If $\hat{f}$ does not share any ports with $f^{*}$, or $\hat{f}$ passes through $\hat{q}$ before sharing any ports with $f^{*}$, then $\hat{f}$'s rate remains the same. If $\hat{f}$ and $f^{*}$ share ports before passing through $\hat{q}$, according to Theorem \ref{thm:insert second}, $\hat{f}$'s rate remains the unchanged or decreases. Either way, we have a contradiction.
\end{proof}

Theorem \ref{thm:insert second} is a key step to prove the correctness of Algorithm \ref{alg: 1}. Theorem \ref{thm:insert second} shows that the generated $N_{f^{*}}$ contains all the congested ports, since we add all $q \in Q_{f^{*}}$. Equation \ref{eq:FCFS} indicates that passing through a congestion-free port does not change the flow rate. This justifies our claim that $A_{f^{*}}$ contains the exact set of the affected flows and those flows can only change the rates at the ports in $N_{f^{*}}$.

%From the analysis above, we know that if there is an update to insert a new flow $f$, we first identify the affected flow set $A_{f}$, then construct the corresponding minimum affected network $N_{f}$, which acts as the input of our simulation algorithm. If the simulation result shows that there is congestion, we reject this flow insertion operation, and feedback the ports that are congested and how the flow rate changes at these ports; otherwise, we issue the update to the data plane and update the results to the network state we maintain in our verification system.

\subsection{Flow Modification}
For a flow modification update, i.e., changing route of an existing flow in the network, \name simply treats the update as a set of flow removal and insertion updates (typically, a removal operation followed by an insertion operation). Since we do not consider the transient network congestion during the updates, the two operations are identical.


